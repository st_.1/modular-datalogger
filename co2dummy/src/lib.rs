#![no_std]

use core::{array::TryFromSliceError, convert::*};
use embedded_hal::{blocking::delay::*, serial::*};
use nb::block;

#[derive(Debug, PartialEq)]
pub struct FirmwareVer {
    major: u8,
    minor: u8
}

#[derive(Debug)]
pub enum Co2DummyError {
    FailedToCheckFirmwareVer,
    InvalidFirmwareVer,
    InvalidArgument,
    TransferError(TransferError),
    ParsingError(TryFromSliceError),
    InvalidReply
}

#[derive(Debug)]
pub enum TransferError {
    FailedToSend,
    FailedToRecv,
    InvalidCrc
}

impl From<TransferError> for Co2DummyError {
    fn from(te: TransferError) -> Self {
        Co2DummyError::TransferError(te)
    }
}

impl From<TryFromSliceError> for Co2DummyError {
    fn from(tfse: TryFromSliceError) -> Self {
        Co2DummyError::ParsingError(tfse)
    }
}

enum Func {
    ReadHoldReg = 3,
    WriteHoldReg = 6
}

pub struct Co2DummyMeasurement {
    pub co2_concentration : f32,
    pub temperature : f32,
    pub humidity : f32,
}

pub struct Co2Dummy<SERIAL>
where SERIAL: Write<u8> + Read<u8> {
    serial: SERIAL
}

impl<SERIAL> Co2Dummy<SERIAL>
where SERIAL: Write<u8> + Read<u8> {
    pub fn new<D>(serial : SERIAL, delay: &mut D) -> Result<Self, Co2DummyError>
    where D: DelayUs<u16> + DelayMs<u16> {
        let mut co2dummy = Co2Dummy { serial };

        delay.delay_ms(100);

        // read firmware version (sanity check)
        let fw_ver_res = co2dummy.firmware_ver();

        if fw_ver_res.is_err() {
            return Err(Co2DummyError::FailedToCheckFirmwareVer);
        } else if fw_ver_res.unwrap() != (FirmwareVer { major: 2, minor: 18 }) {
            return Err(Co2DummyError::InvalidFirmwareVer);
        }

        Ok(co2dummy)
    }

    // Read firmware version
    pub fn firmware_ver(&mut self) -> Result<FirmwareVer, Co2DummyError> {

        // fn_code: 3 (Read Holding Register)
        // addr: 0x0020
        // no. registers: 0x0001
        let req: [u8; 4] = [0x00, 0x20, 0x00, 0x01];
        let mut repl = [0u8; 3];

        self.transfer(Func::ReadHoldReg, &req, &mut repl)?;

        Ok(FirmwareVer { major: repl[1], minor: repl[2] })
    }

    // Trigger continuous measurement
    pub fn trigger_measurement(&mut self) -> Result<(), Co2DummyError> {

        // fn_code: 6 (Write Holding Register)
        // addr: 0x0036
        // content: 0x0000
        let req: [u8; 4] = [0x00, 0x36, 0x00, 0x00];
        let mut repl = [0u8; 4];

        self.transfer(Func::WriteHoldReg, &req, &mut repl)?;

        if req != repl {
            return Err(Co2DummyError::InvalidReply);
        }

        Ok(())
    }

    // Stop continuous measurement
    pub fn stop_measurement(&mut self) -> Result<(), Co2DummyError> {

        // fn_code: 6 (Write Holding Register)
        // addr: 0x0037
        // content: 0x0001
        let req: [u8; 4] = [0x00, 0x37, 0x00, 0x01];
        let mut repl = [0u8; 4];

        self.transfer(Func::WriteHoldReg, &req, &mut repl)?;

        if req != repl {
            return Err(Co2DummyError::InvalidReply);
        }

        Ok(())
    }

    // Set measurement interval
    // Range: [2 .. 1800]
    pub fn set_interval(&mut self, sec_interval : u16) -> Result<(), Co2DummyError> {

        if sec_interval < 2 || sec_interval > 1800 {
            return Err(Co2DummyError::InvalidArgument);
        }

        // fn_code: 6 (Write Holding Register)
        // addr: 0x0025
        // content: sec_interval (LE)
        let mut req: [u8; 4] = [0x00, 0x25, 0x00, 0x00];
        req[2..4].clone_from_slice(&sec_interval.to_be_bytes());
        let mut repl = [0u8; 4];

        self.transfer(Func::WriteHoldReg, &req, &mut repl)?;

        if req != repl {
            return Err(Co2DummyError::InvalidReply);
        }

        Ok(())
    }

    // Read measurement
    // Reads a single measurement of CO2 concentration
    pub fn read_measurement(&mut self) -> Result<Co2DummyMeasurement, Co2DummyError> {

        // fn_code: 3 (Read Holding Register)
        // addr: 0x0028
        // no. registers: 0x0006
        let req: [u8; 4] = [0x00, 0x28, 0x00, 0x06];
        let mut repl = [0u8; 13]; // (the data portion of a modbus frame)

        self.transfer(Func::ReadHoldReg, &req, &mut repl)?;

        Ok(Co2DummyMeasurement {
            co2_concentration: f32::from_be_bytes(repl[1..5].try_into()?),
            temperature: f32::from_be_bytes(repl[5..9].try_into()?),
            humidity: f32::from_be_bytes(repl[9..13].try_into()?),
        })
    }

    // Does the UART + Modbus heavy lifting
    fn transfer<'a>(&mut self, fn_code : Func, request_data : &'a[u8],
                    reply_data : &'a mut[u8]) -> Result<&'a[u8], TransferError> {

        let mut recv_idx = 0;
        let mut buf = [0u8; 256];

        // send a request
        buf[0] = 0x61; // slave address
        buf[1] = fn_code as u8; // function code
        buf[2..request_data.len()+2].clone_from_slice(request_data);
        let crc_req = Self::crc16_modbus(&buf[0..(request_data.len() + 4)]);
        buf[request_data.len() + 2] = (crc_req & 0xFF) as u8;
        buf[request_data.len() + 3] = (crc_req >> 8) as u8;

        let _ = &buf[0..=request_data.len()+3]
            .iter()
            .try_for_each(|b| block!(self.serial.write(*b)))
            .map_err(|_| TransferError::FailedToSend)?;

        // recv reply
        while let Ok(word) = block!(self.serial.read()) {
            buf[recv_idx] = word;
            recv_idx += 1;

            if recv_idx == (reply_data.len() + 4) {
                break;
            }
        }

        if recv_idx < (reply_data.len() + 4) {
            return Err(TransferError::FailedToRecv);
        }

        // check CRC16/MODBUS
        let crc_reply = u16::from_le_bytes([buf[reply_data.len() + 2], buf[reply_data.len() + 3]]);
        let crc_reply_gen = Self::crc16_modbus(&buf[0..(reply_data.len() + 4)]);

        if crc_reply != crc_reply_gen {
            return Err(TransferError::InvalidCrc);
        }

        reply_data.copy_from_slice(&buf[2..(reply_data.len()+2)]);

        Ok(reply_data)
    }

    fn crc16_modbus(frame : &[u8]) -> u16 {
        let mut crc: u16 = 0xFFFF;

        for i in 0..=frame.len()-3 {
            let c : u8 = frame[i as usize];

            crc = crc ^ c as u16;

            let mut n : u8 = 0;
            while n < 8 {
                let lsb  = crc & 1;
                crc >>= 1;
                if lsb == 1 {
                    crc ^= 0xA001;
                }
                n += 1;
            }
        }

        crc
    }
}
