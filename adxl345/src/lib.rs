#![no_std]
#![allow(non_camel_case_types)]
#![allow(dead_code)]

use embedded_hal::{spi::FullDuplex, blocking::spi::Transfer, digital::v2::OutputPin};
pub use accelerometer::{RawAccelerometer, Accelerometer, vector::I16x3, vector::F32x3};

mod enums;
mod config;
mod config_writer;
mod common;
pub use enums::*;
pub use config::Adxl345Config;
pub use common::*;
use config_writer::ConfigWriter;

mod dma;
#[cfg(feature = "stm32f405-dma")]
pub use dma::*;

pub struct Adxl345<SPI, CS>
where SPI: FullDuplex<u8> + Transfer<u8>,
      CS: OutputPin {
    spi: SPI,
    cs: CS,
    config: Adxl345Config
}

impl<SPI, CS> Adxl345<SPI, CS>
where SPI: FullDuplex<u8> + Transfer<u8>,
      CS: OutputPin
{
    pub fn new(spi: SPI, cs: CS, config: Adxl345Config) -> Result<Self, Error> {
        // configure peripheral
        let mut config_writer = ConfigWriter::new(spi, cs)?;
        config_writer.persist_config(&config)?;
        let (spi, cs) = config_writer.release();

        Ok(Self { spi, cs, config })
    }

    fn cs_set_low(&mut self) -> Result<(), Error> {
        if self.cs.set_low().is_err() {
            return Err(Error::UnableToPullChipSelect);
        }

        Ok(())
    }

    fn cs_set_high(&mut self) -> Result<(), Error> {
        if self.cs.set_high().is_err() {
            return Err(Error::UnableToPullChipSelect);
        }

        Ok(())
    }

    fn read(&mut self, mut bytes: &mut [u8]) -> Result<(), Error> {
        if bytes.len() > 2 {
            bytes[0] |= MULTI_BYTE;
        }

        bytes[0] |= SPI_READ;

        self.cs_set_low()?;

        // read bytes at bytes[0] to bytes[1..]
        if self.spi.transfer(&mut bytes).is_err() {
            return Err(Error::SpiError);
        }

        self.cs_set_high()?;

        Ok(())
    }
}


impl<SPI, CS> RawAccelerometer<I16x3> for Adxl345<SPI, CS>
where SPI: FullDuplex<u8> + Transfer<u8>,
      CS: OutputPin
{
    type Error = Error;

    fn accel_raw(&mut self) -> Result<I16x3, accelerometer::Error<Self::Error>> {
        let mut bytes : [u8; BUF_SIZE] = [0; BUF_SIZE];
        bytes[0] = 0x32;

        // read DATAX0..DATAZ1
        if let Err(error) = self.read(&mut bytes) {
            return Err(accelerometer::Error::from(error));
        }

        Ok(buf_to_accel_raw(&bytes))
    }
}

impl<SPI, CS> Accelerometer for Adxl345<SPI, CS>
where SPI: FullDuplex<u8> + Transfer<u8>,
      CS: OutputPin
{
    type Error = Error;

    fn sample_rate(&mut self) -> Result<f32, accelerometer::Error<Self::Error>> {
        Ok(self.config.data_rate.into())
    }

    fn accel_norm(&mut self) -> Result<F32x3, accelerometer::Error<Self::Error>> {
        let raw : I16x3 = self.accel_raw()?;
        Ok(raw_to_accel_norm(raw, self.config.full_resolution, self.config.range))
    }
}
