use crate::{enums::*, config::*};

use embedded_hal::{spi::FullDuplex, blocking::spi::Transfer, digital::v2::OutputPin};

pub struct ConfigWriter<SPI, CS>
where SPI: FullDuplex<u8> + Transfer<u8>,
      CS: OutputPin {
    spi: SPI,
    cs: CS,
}

impl<SPI, CS> ConfigWriter<SPI, CS>
where SPI: FullDuplex<u8> + Transfer<u8>,
      CS: OutputPin
{
    pub(crate) fn new(spi: SPI, cs: CS) -> Result<Self, Error> {
        let mut config_writer = Self { spi, cs };

        config_writer.cs_set_high()?;

        if config_writer.device_id()? != 0b11100101 {
            return Err(Error::InvalidDeviceId)
        }

        Ok(config_writer)
    }

    pub(crate) fn persist_config(&mut self, config: &Adxl345Config) -> Result<(), Error> {
        let Adxl345Config { range, full_resolution, data_rate, power_mode, fifo_mode, fifo_samples } = config;

        self.set_fifo(*fifo_mode, *fifo_samples)?;
        self.set_data_format(*full_resolution, *range)?;
        self.set_data_rate(*data_rate)?;
        self.set_power_mode(*power_mode)?;

        Ok(())
    }

    pub(crate) fn release(self) -> (SPI, CS) {
        (self.spi, self.cs)
    }

    fn device_id(&mut self) -> Result<u8, Error> {
        self.read_register(DEVID)
    }

    fn set_data_format(&mut self, full_resolution: bool, range: Range) -> Result<(), Error> {
        let mut data_format : u8 = 0;

        if full_resolution == true {
            data_format |= 0x8; // D3: FULL_RES
        }

        data_format |= range as u8 & 0xF; // D1, D0: Range

        self.write_register(DATA_FORMAT, data_format)
    }

    fn set_data_rate(&mut self, data_rate: OutputDataRate) -> Result<(), Error> {
        self.write_register(BW_RATE, data_rate as u8)?;
        Ok(())
    }

    fn set_power_mode(&mut self, power_mode: PowerMode) -> Result<(), Error> {
        match power_mode {
            PowerMode::Measure => self.write_register(POWER_CTL, 0b00001000)?, // POWER_CTL[D3] ... Measure
            PowerMode::Standby => self.write_register(POWER_CTL, 0)?
        }
        Ok(())
    }

    fn set_fifo(&mut self, fifo_mode: FifoMode, fifo_samples: u8) -> Result<(), Error> {
        // FIFO_CTL[7:6] <- FIFO_MODE
        // FIFO_CTL[4:0] <- Samples
        self.write_register(FIFO_CTL, (fifo_mode as u8 & 0b11) << 6 | (fifo_samples & 0b11111))?;

        // Enable Watermark interrupt if needed
        if fifo_mode != FifoMode::Bypass {
            self.write_register(INT_ENABLE, 0b10)?; // INT_ENABLE[Watermark] <- 1
            self.write_register(INT_MAP, 0)?;       // INT_MAP[Watermark] <- 0
        }

        Ok(())
    }

    pub fn read_register(&mut self, addr: u8) -> Result<u8, Error> {
        let mut bytes : [u8; 2] = [addr | SPI_READ, 0];

        self.cs_set_low()?;

        // read register
        if self.spi.transfer(&mut bytes).is_err() {
            return Err(Error::SpiError);
        }

        self.cs_set_high()?;

        Ok(bytes[1])
    }

    fn write_register(&mut self, addr: u8, value: u8) -> Result<(), Error> {
        let mut bytes : [u8; 2] = [addr, value];

        self.cs_set_low()?;

        // write register
        if self.spi.transfer(&mut bytes).is_err() {
            return Err(Error::SpiError);
        }

        self.cs_set_high()?;

        Ok(())
    }

    fn cs_set_low(&mut self) -> Result<(), Error> {
        if self.cs.set_low().is_err() {
            return Err(Error::UnableToPullChipSelect);
        }

        Ok(())
    }

    fn cs_set_high(&mut self) -> Result<(), Error> {
        if self.cs.set_high().is_err() {
            return Err(Error::UnableToPullChipSelect);
        }

        Ok(())
    }
}
