#![cfg(feature = "stm32f405-dma")]

use crate::{enums::*, config::*, config_writer::ConfigWriter, common::*};
use embedded_hal::{spi::FullDuplex, blocking::spi::Transfer, digital::v2::{InputPin, OutputPin}};
use stm32f4xx_hal::{
    dma, dma::{config::DmaConfig, traits::{DMASet, StreamISR, Channel}, PeripheralToMemory, MemoryToPeripheral, StreamX, ChannelX},
    spi, spi::{Tx, Rx, Spi}
};
pub use accelerometer::{RawAccelerometer, Accelerometer, vector::I16x3, vector::F32x3};

type SpiDmaTx<DMA, SPI, const S: u8, const CH: u8> = dma::Transfer<
    StreamX<DMA, S>,
    CH,
    Tx<SPI>,
    MemoryToPeripheral,
    &'static mut [u8; BUF_SIZE]
>;

type SpiDmaRx<DMA, SPI, const S: u8, const CH: u8> = dma::Transfer<
    StreamX<DMA, S>,
    CH,
    Rx<SPI>,
    PeripheralToMemory,
    &'static mut [u8; BUF_SIZE]
>;

type Adxl345DmaBuffers = (&'static mut [u8; BUF_SIZE], &'static mut [u8; BUF_SIZE], &'static mut [u8; BUF_SIZE]);

pub struct Adxl345Dma<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, const S_TX: u8, const S_RX: u8, const CH_TX: u8, const CH_RX: u8>
where DMA: dma::traits::Instance,
      SPI: spi::Instance,
      CS: OutputPin,
      INT0: InputPin,
      Spi<SPI, SPI_PINS, SPI_TM>: FullDuplex<u8> + Transfer<u8>,
      StreamX<DMA, S_TX>: StreamISR,
      StreamX<DMA, S_RX>: StreamISR,
      ChannelX<CH_TX>: Channel,
      ChannelX<CH_RX>: Channel {

    transfer_tx: SpiDmaTx<DMA, SPI, S_TX, CH_TX>,
    transfer_rx: SpiDmaRx<DMA, SPI, S_RX, CH_RX>,
    buffer_rx: Option<&'static mut [u8; BUF_SIZE]>,
    count: u8, // 1..16 = reading fifo, 0 = inactive
    spi_raw: Option<Spi<SPI, SPI_PINS, SPI_TM>>, // retained only for the purposes of release()
    cs: CS,
    int0: INT0,
    config: Adxl345Config
}

impl<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, const S_TX: u8, const S_RX: u8, const CH_TX: u8, const CH_RX: u8>
Adxl345Dma<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, S_TX, S_RX, CH_TX, CH_RX>
where DMA: dma::traits::Instance,
      SPI: spi::Instance,
      CS: OutputPin,
      INT0: InputPin,
      Spi<SPI, SPI_PINS, SPI_TM>: FullDuplex<u8> + Transfer<u8>,
      Tx<SPI>: DMASet<StreamX<DMA, S_TX>, CH_TX, MemoryToPeripheral>,
      Rx<SPI>: DMASet<StreamX<DMA, S_RX>, CH_RX, PeripheralToMemory>,
      StreamX<DMA, S_TX>: StreamISR,
      StreamX<DMA, S_RX>: StreamISR,
      ChannelX<CH_TX>: Channel,
      ChannelX<CH_RX>: Channel {

    pub fn new(spi: Spi<SPI, SPI_PINS, SPI_TM>,
               cs: CS,
               int0: INT0,
               stream_tx: StreamX<DMA, S_TX>,
               stream_rx: StreamX<DMA, S_RX>,
               config: Adxl345Config,
               buffers: Adxl345DmaBuffers) -> Result<Self, Error> {

        // configure peripheral
        let mut config_writer = ConfigWriter::new(spi, cs)?;
        config_writer.persist_config(&config)?;
        let (spi, mut cs) = config_writer.release();

        // configure dma
        let (buffer_tx, buffer_rx_0, buffer_rx_1) = buffers;
        let (transfer_tx, transfer_rx) = Self::init_dma(spi, stream_tx, stream_rx, buffer_tx, buffer_rx_0);

        cs.set_low().ok();

        Ok(Self { transfer_tx, transfer_rx, buffer_rx: Some(buffer_rx_1), count: 0, spi_raw: None, cs, int0, config })
    }

    // Polling function
    // Starts a new transfer if the INT0 interrupt is set high,
    // Returns true if a transfer was started
    pub fn poll(&mut self) -> bool {
        let result_start_transfer = self.count == 0 && self.int0.is_high().unwrap_or(false);

        if result_start_transfer {
            self.start_transfer();
        }

        result_start_transfer
    }

    // Starts transfer
    // TX is the clock driver & transfers the read command
    // RX passively collects data into a prepared `identical` buffer
    fn start_transfer(&mut self) {
        self.count = self.config.fifo_samples;
        self.cs.set_low().ok();
        self.transfer_tx.start(|_tx| {});
        self.transfer_rx.start(|_rx| {});
    }

    // Handles DMA interrupt
    // restarts transfer, deasserts CS and decrements transfer count
    // until transfer count reaches 0
    pub fn transfer_rx(&mut self) -> Result<F32x3, ()> {
        // https://ez.analog.com/mems/f/q-a/103002/adxl345-fifo-with-dma
        self.cs.set_high().ok();


        if self.count > 1 {
            // restart RX transfer, exchange buffers
            let (old_buffer, _) = self.transfer_rx
                .next_transfer(self.buffer_rx.take().unwrap())
                .unwrap();

            // restart TX transfer
            unsafe { self.transfer_tx.next_transfer_with(|buffer, _active_buffer| {
                self.cs.set_low().ok();
                (buffer, ())
            }).ok() };

            self.buffer_rx = Some(old_buffer);
        } else {
            self.transfer_tx.clear_transfer_complete_interrupt();
            self.transfer_rx.clear_transfer_complete_interrupt();

            // if watermark is still set run next dma transfer right away
            if self.int0.is_high().unwrap_or(false) {
                self.start_transfer();
            }
        }

        self.count -= 1;

        if let Some(&mut buffer_rx) = &mut self.buffer_rx {
            let raw = buf_to_accel_raw(&buffer_rx);
            let accel_norm = raw_to_accel_norm(raw, self.config.full_resolution, self.config.range);
            Ok(accel_norm)
        } else {
            Err(())
        }
    }

    fn init_dma(spi: Spi<SPI, SPI_PINS, SPI_TM>,
                stream_tx: StreamX<DMA, S_TX>,
                stream_rx: StreamX<DMA, S_RX>,
                buffer_tx: &'static mut [u8; BUF_SIZE],
                buffer_rx: &'static mut [u8; BUF_SIZE])
            -> (
                SpiDmaTx<DMA, SPI, S_TX, CH_TX>,
                SpiDmaRx<DMA, SPI, S_RX, CH_RX>
            ) {

        // DATA_{X,Y,Z} read command
        buffer_tx[0] = 0x32;
        buffer_tx[0] |= SPI_READ;
        buffer_tx[0] |= MULTI_BYTE;

        let (spi_tx, spi_rx) = spi.use_dma().txrx();

        let transfer_config_tx = DmaConfig::default()
            .memory_increment(true);

        let transfer_config_rx = DmaConfig::default()
            .memory_increment(true)
            .transfer_complete_interrupt(true);

        let transfer_tx = dma::Transfer::init_memory_to_peripheral(
            stream_tx,
            spi_tx,
            buffer_tx,
            None,
            transfer_config_tx
        );

        let transfer_rx = dma::Transfer::init_peripheral_to_memory(
            stream_rx,
            spi_rx,
            buffer_rx,
            None,
            transfer_config_rx
        );

        (transfer_tx, transfer_rx)
    }

    fn cs_set_low(&mut self) -> Result<(), Error> {
        if self.cs.set_low().is_err() {
            return Err(Error::UnableToPullChipSelect);
        }

        Ok(())
    }

    fn cs_set_high(&mut self) -> Result<(), Error> {
        if self.cs.set_high().is_err() {
            return Err(Error::UnableToPullChipSelect);
        }

        Ok(())
    }

    pub fn release(self) -> (
            // Can't return Spi due to the way DmaBuilder is implemented
            // Spi<SPI, SPI_PINS, SPI_TM>,
            CS,
            INT0,
            StreamX<DMA, S_TX>,
            StreamX<DMA, S_RX>,
            Adxl345Config,
            Adxl345DmaBuffers
        ) {

        let (stream_tx, _spi_tx, buffer_tx, _) = self.transfer_tx.release();
        let (stream_rx, _spi_rx, buffer_rx_0, _) = self.transfer_rx.release();
        let buffer_rx_1 = self.buffer_rx.unwrap();

        let buffers = (buffer_tx, buffer_rx_0, buffer_rx_1);

        (self.cs, self.int0, stream_tx, stream_rx, self.config, buffers)
    }

}
