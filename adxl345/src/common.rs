use crate::enums::*;
use accelerometer::vector::{I16x3, F32x3};

// read command : u8 + XYZ : 3 * u16
pub const BUF_SIZE: usize = 7;

fn resolution(full_resolution: bool, range: Range) -> u8 {
    if full_resolution == false {
        return 10;
    }

    match range {
        Range::_2G => 10,
        Range::_4G => 11,
        Range::_8G => 12,
        Range::_16G => 13,
    }
}

fn accel_max(full_resolution: bool, range: Range) -> u16 {
    match resolution(full_resolution, range) {
        11 => MAX_ACCEL_11,
        12 => MAX_ACCEL_12,
        13 => MAX_ACCEL_13,
        _ => MAX_ACCEL_10
    }
}

// Conversion from raw bytes to I16x3
pub(crate) fn buf_to_accel_raw(bytes: &[u8; BUF_SIZE]) -> I16x3 {
    // DATA{X,Y,Z}1 = MSB, 2s complement
    let x = (bytes[1] as i16) | ((bytes[2] as i16) << 8);
    let y = (bytes[3] as i16) | ((bytes[4] as i16) << 8);
    let z = (bytes[5] as i16) | ((bytes[6] as i16) << 8);

    I16x3::new(x, y, z)
}

// Conversion to normalized g values
pub(crate) fn raw_to_accel_norm(raw: I16x3, full_resolution: bool, range: Range) -> F32x3 {
    let accel_max : f32 = accel_max(full_resolution, range) as f32;
    let range : f32 = range.into();

    let x = (raw.x as f32 / accel_max) * range;
    let y = (raw.y as f32 / accel_max) * range;
    let z = (raw.z as f32 / accel_max) * range;

    F32x3::new(x, y, z)
}
