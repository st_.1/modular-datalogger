#![allow(dead_code)]
use crate::enums::{Range, OutputDataRate, PowerMode, FifoMode};

#[non_exhaustive]
pub struct Adxl345Config {
    pub(crate) range: Range,
    pub(crate) full_resolution: bool,
    pub(crate) data_rate: OutputDataRate,
    pub(crate) power_mode: PowerMode,
    pub(crate) fifo_mode: FifoMode,
    pub(crate) fifo_samples: u8
}

impl Default for Adxl345Config {
    fn default() -> Self {
        Self {
            range: Range::default(),
            full_resolution: false,
            data_rate: OutputDataRate::default(),
            power_mode: PowerMode::default(),
            fifo_mode: FifoMode::default(),
            fifo_samples: 0
        }
    }
}

impl Adxl345Config {
    pub fn new() -> Self { Self::default() }

    pub fn range(&self, range: Range) -> Self {
        Self { range, ..*self }
    }

    pub fn full_resolution(&self, full_resolution: bool) -> Self {
        Self { full_resolution, ..*self }
    }

    pub fn data_rate(&self, data_rate: OutputDataRate) -> Self {
        Self { data_rate, ..*self }
    }

    pub fn power_mode(&self, power_mode: PowerMode) -> Self {
        Self { power_mode, ..*self }
    }

    pub fn fifo_mode(&self, fifo_mode: FifoMode) -> Self {
        Self { fifo_mode, ..*self }
    }

    pub fn fifo_samples(&self, fifo_samples: u8) -> Self {
        Self { fifo_samples, ..*self }
    }
}
