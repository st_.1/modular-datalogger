#![allow(non_camel_case_types)]
#![allow(dead_code)]

use core::fmt;
use serde::{Serialize, Deserialize};
use Range::*;
use OutputDataRate::*;

pub(crate) const SPI_READ : u8 = 0x80;
pub(crate) const MULTI_BYTE : u8 = 0x40;
pub(crate) const DEVID : u8 = 0x0;
pub(crate) const BW_RATE : u8 = 0x2c;
pub(crate) const DATA_FORMAT : u8 = 0x31;
pub(crate) const POWER_CTL : u8 = 0x2d;
pub(crate) const FIFO_CTL : u8 = 0x38;
pub(crate) const INT_ENABLE : u8 = 0x2e;
pub(crate) const INT_MAP : u8 = 0x2f;

pub const MAX_ACCEL_10 : u16 = 0x1FF; // 2**(10-1)-1
pub const MAX_ACCEL_11 : u16 = 0x3FF; // 2**(11-1)-1
pub const MAX_ACCEL_12 : u16 = 0x7FF; // 2**(12-1)-1
pub const MAX_ACCEL_13 : u16 = 0xFFF; // 2**(13-1)-1

#[derive(Debug)]
pub enum Error {
    UnableToPullChipSelect,
    SpiError,
    InvalidDeviceId
}

#[derive(Debug, Copy, Clone)]
pub enum PowerMode {
    Standby,
    Measure
}

impl Default for PowerMode {
    fn default() -> Self { PowerMode::Standby }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum FifoMode {
    Bypass = 0b00,
    FIFO = 0b01,
    Stream = 0b10,
    Trigger = 0b11
}

impl Default for FifoMode {
    fn default() -> Self { FifoMode::Bypass }
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum Range {
    _2G = 0b00,
    _4G = 0b01,
    _8G = 0b10,
    _16G = 0b11
}

impl Default for Range {
    fn default() -> Self { _2G }
}

impl From<Range> for f32 {
    fn from(range: Range) -> f32 {
        match range {
            _2G => 2.0,
            _4G => 4.0,
            _8G => 8.0,
            _16G => 16.0,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum OutputDataRate {
    ODR_3200_Hz = 0b1111,
    ODR_1600_Hz = 0b1110,
    ODR_800_Hz  = 0b1101,
    ODR_400_Hz  = 0b1100,
    ODR_200_Hz  = 0b1011,
    ODR_100_Hz  = 0b1010,
    ODR_50_Hz   = 0b1001,
    ODR_25_Hz   = 0b1000,
    ODR_12_5_Hz = 0b0111,
    ODR_6_25_Hz = 0b0110,
    ODR_3_13_Hz = 0b0101,
    ODR_1_56_Hz = 0b0100,
    ODR_0_78_Hz = 0b0011,
    ODR_0_39_Hz = 0b0010,
    ODR_0_20_Hz = 0b0001,
    ODR_0_10_Hz = 0b0000,
}

impl Default for OutputDataRate {
    fn default() -> Self { ODR_100_Hz }
}

impl From<OutputDataRate> for f32 {
    fn from(data_rate: OutputDataRate) -> f32 {
        match data_rate {
            ODR_3200_Hz => 3200.0,
            ODR_1600_Hz => 1600.0,
            ODR_800_Hz  => 800.0,
            ODR_400_Hz  => 400.0,
            ODR_200_Hz  => 200.0,
            ODR_100_Hz  => 100.0,
            ODR_50_Hz   => 50.0,
            ODR_25_Hz   => 25.0,
            ODR_12_5_Hz => 12.5,
            ODR_6_25_Hz => 6.25,
            ODR_3_13_Hz => 3.13,
            ODR_1_56_Hz => 1.56,
            ODR_0_78_Hz => 0.78,
            ODR_0_39_Hz => 0.39,
            ODR_0_20_Hz => 0.20,
            ODR_0_10_Hz => 0.10,
        }
    }
}

impl fmt::Display for OutputDataRate {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:.2} Hz", f32::from(*self))
    }
}

impl fmt::Display for Range {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let val = match *self {
            Self::_2G => 2,
            Self::_4G => 4,
            Self::_8G => 8,
            Self::_16G => 16,
        };

        write!(f, "{} g", val)
    }
}
