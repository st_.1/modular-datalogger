# Datalog format (*.dl)

## Header
Contains version information and metadata for sensors included in a datalog.

| Name                                  | Data type | Description                                     |
|---------------------------------------|-----------|-------------------------------------------------|
| Version                               | u8        | Datalog format version                          |
| Sensor count                          | u8        | Number of sensors in the datalog                |
| Sensor header entry 1 .. sensor count |           | Entries describing each sensor and their values |

### Sensor header entries

| Name                                | Data type | Description                                |
|-------------------------------------|-----------|--------------------------------------------|
| Name                                | CStr      | Sensor name (eg. Adxl 345)                 |
| Id                                  | u16 (le)  | Sensor id                                  |
| Period                              | u64 (le)  | Period in μs                               |
| Value count                         | u8        | Number of values measured by a&nbsp;sensor |
| Sensor value entry 1 .. value count |           | Entries describing sensor's values         |

### Sensor value entry

| Name         | Data type | Description                      |
|--------------|-----------|----------------------------------|
| Name         | CStr      | Value name (eg. Temperature)     |
| Unit         | CStr      | Value unit (eg. %, ppm, C, ...)  |
| Data type id | u8        | Defined in table "Data type" |

### Data type

| Value | Data type |
|-------|-----------|
| 0     | u8        |
| 1     | i8        |
| 2     | u16       |
| 3     | i16       |
| 4     | u32       |
| 5     | i32       |
| 6     | f32       |
| 7     | f64       |

## Body entries

Contain data entries themselves in a format described by the header.

| Name                                                       | Data type               | Description                            |
|------------------------------------------------------------|-------------------------|----------------------------------------|
| Id                                                         | u16 (le)                | Unique sensor id defined in the header |
| Timestamp                                                  | u64 (le)                | Relative timestamp in μs               |
| Measurement Result                                         | u8                      | Defined in table "Measurement result"  |
| Value (only shown if the measurement result was successful | Defined by data type id | Value defined in sensor header entry   |
| ...                                                        | ...                     | (value count - 1 more values)          |

### Measurement result

| Value | Enumeration | Description                                                            |
|-------|-------------|------------------------------------------------------------------------|
| 0     | Success     | Measurement successful                                                 |
| 1     | Failure     | Measurement not successful                                             |
