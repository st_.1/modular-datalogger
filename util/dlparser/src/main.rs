use std::env;
use std::fs::File;
use std::io;
use std::io::{Read, Seek, BufRead, BufReader};
use std::string::FromUtf8Error;
use std::convert::TryInto;

use num_derive::FromPrimitive;
use num_traits::FromPrimitive;

#[derive(Debug)]
enum FormatError {
    UnsupportedVersion,
    InvalidSensorNameLength(u8),
    InvalidSensorName(FromUtf8Error),
    InvalidSensorValueNameLength((u8,u8)),
    InvalidSensorValueUnitLength((u8,u8)),
    InvalidSensorValueName(FromUtf8Error),
    InvalidSensorValueUnit(FromUtf8Error),
    InvalidValueDataType((u8,u8)),
    InvalidSensorId(u16, u64),
    InvalidSensorIdLength(u64),
    InvalidMeasurementResult(u16),
    IoError(io::Error)
}

impl From<io::Error> for FormatError {
    fn from(error: io::Error) -> FormatError {
        FormatError::IoError(error)
    }
}

#[allow(dead_code)]
#[derive(Debug, FromPrimitive)]
enum ValueDataType {
    U8,
    I8,
    U16,
    I16,
    U32,
    I32,
    F32,
    F64
}

#[derive(Debug)]
struct SensorValue {
    name : String,
    unit : String,
    value_type : ValueDataType
}

#[derive(Debug)]
enum SensorEntryValue {
    U8(u8),
    I8(i8),
    U16(u16),
    I16(i16),
    U32(u32),
    I32(i32),
    F32(f32),
    F64(f64)
}

#[derive(Debug, FromPrimitive, PartialEq)]
enum MeasurementResult {
    Success,
    Failure
}

#[derive(Debug)]
struct SensorEntry {
    time_us: u64,
    result: MeasurementResult,
    values: Option<Vec<SensorEntryValue>>
}

#[derive(Debug)]
struct SensorLog {
    name : String,
    id : u16,
    period_us : u64,
    values : Vec<SensorValue>,
    entries : Vec<SensorEntry>
}

type DataLog = Vec<SensorLog>;

fn parse_sensor_values<R>(reader: &mut BufReader<R>, value_count: u8, sensor_no: u8) -> Result<Vec<SensorValue>, FormatError>
    where R : Read {

    let mut name_bytes : Vec<u8> = Vec::new();
    let mut unit_bytes : Vec<u8> = Vec::new();
    let mut values : Vec<SensorValue> = Vec::new();

    for value_no in 0..value_count {
        name_bytes.clear();
        unit_bytes.clear();
        let name_len = reader.read_until(b'\0', &mut name_bytes)?;
        let unit_len = reader.read_until(b'\0', &mut unit_bytes)?;

        if name_len == 0 {
            return Err(FormatError::InvalidSensorValueNameLength((sensor_no, value_no)))
        }

        if unit_len == 0 {
            return Err(FormatError::InvalidSensorValueUnitLength((sensor_no, value_no)))
        }

        let name =
            match String::from_utf8(name_bytes.clone()) {
                Ok(name) => name,
                Err(error) =>
                    return Err(FormatError::InvalidSensorValueName(error))
            };

        let unit =
            match String::from_utf8(unit_bytes.clone()) {
                Ok(unit) => unit,
                Err(error) =>
                    return Err(FormatError::InvalidSensorValueUnit(error))
            };

        let mut value_type_byte : [u8; 1] = [0];

        reader.read_exact(&mut value_type_byte)?;

        let value_type : ValueDataType =
            match FromPrimitive::from_u8(value_type_byte[0]) {
                Some(value_type) => value_type,
                None => return Err(FormatError::InvalidValueDataType((sensor_no, value_no)))
            };

        values.push(SensorValue { name, unit, value_type });
    }

    return Ok(values);
}

fn parse_header<R>(reader: &mut BufReader<R>) -> Result<DataLog, FormatError>
    where R : Read {

    let mut data_log : DataLog = Vec::new();
    let mut head_buf : [u8; 2] = [0; 2];

    reader.read_exact(&mut head_buf)?;

    let format_ver = head_buf[0];
    let sensor_count = head_buf[1];

    if format_ver != 0 {
        return Err(FormatError::UnsupportedVersion);
    }

    let mut sensor_name_bytes : Vec<u8> = Vec::new();
    let mut sensor_values_buf = [0u8; 11];

    for sensor_no in 0..sensor_count {

        sensor_name_bytes.clear();
        let name_len = reader.read_until(b'\0', &mut sensor_name_bytes)?;
        reader.read_exact(&mut sensor_values_buf)?;

        if name_len == 0 {
            return Err(FormatError::InvalidSensorNameLength(sensor_no))
        }

        let name =
            match String::from_utf8(sensor_name_bytes.clone()) {
                Ok(name) => name,
                Err(error) =>
                    return Err(FormatError::InvalidSensorName(error))
            };

        let id = u16::from_le_bytes(sensor_values_buf[0..2].try_into().unwrap());
        let period_us = u64::from_le_bytes(sensor_values_buf[2..10].try_into().unwrap());
        let value_count = sensor_values_buf[10];
        let values = parse_sensor_values(reader, value_count, sensor_no)?;

        data_log.push(SensorLog { name, id, period_us, values, entries: Vec::new() });
    }

    Ok(data_log)
}

fn parse_entry_values<R>(reader: &mut BufReader<R>, sensor_log: &SensorLog) -> Result<Vec<SensorEntryValue>, FormatError>
    where R : Read + Seek {

    let mut val_1 : [u8; 1] = [0; 1];
    let mut val_2 : [u8; 2] = [0; 2];
    let mut val_4 : [u8; 4] = [0; 4];
    let mut val_8 : [u8; 8] = [0; 8];

    let mut values : Vec<SensorEntryValue> = Vec::new();

    // read entries based on definitions in SensorLog::values
    for value_def in &sensor_log.values {
        match &value_def.value_type {
            ValueDataType::U8 => {
                reader.read_exact(&mut val_1)?;
                values.push(SensorEntryValue::U8(val_1[0]));
            },
            ValueDataType::I8 => {
                reader.read_exact(&mut val_1)?;
                values.push(SensorEntryValue::I8(val_1[0] as i8));
            },
            ValueDataType::U16 => {
                reader.read_exact(&mut val_2)?;
                values.push(SensorEntryValue::U16(u16::from_le_bytes(val_2)));
            },
            ValueDataType::I16 => {
                reader.read_exact(&mut val_2)?;
                values.push(SensorEntryValue::I16(i16::from_le_bytes(val_2)));
            },
            ValueDataType::U32 => {
                reader.read_exact(&mut val_4)?;
                values.push(SensorEntryValue::U32(u32::from_le_bytes(val_4)));
            },
            ValueDataType::I32 => {
                reader.read_exact(&mut val_4)?;
                values.push(SensorEntryValue::I32(i32::from_le_bytes(val_4)));
            },
            ValueDataType::F32 => {
                reader.read_exact(&mut val_4)?;
                values.push(SensorEntryValue::F32(f32::from_le_bytes(val_4)));
            },
            ValueDataType::F64 => {
                reader.read_exact(&mut val_8)?;
                values.push(SensorEntryValue::F64(f64::from_le_bytes(val_8)));
            }
        };
    }

    Ok(values)
}

fn parse_entries<R>(reader: &mut BufReader<R>, data_log: &mut DataLog) -> Result<(), FormatError>
    where R : Read + Seek {

    let mut sensor_id_bytes = [0u8; 2];
    let mut time_us_bytes = [0u8; 8];
    let mut result_bytes = [0u8; 1];

    loop {
        // try reading another entry
        let size = reader.read(&mut sensor_id_bytes)?;
        if size == 0
            { return Ok(()) } // quit on EOF
        if size == 1
            { return Err(FormatError::InvalidSensorIdLength(reader.stream_position()? - 1)); }

        let sensor_id = u16::from_le_bytes(sensor_id_bytes.clone());

        // find SensorLog
        let sensor_log : &mut SensorLog;

        match data_log.iter_mut().find(|s| s.id == sensor_id) {
            Some(sl) => sensor_log = sl,
            None => return Err(FormatError::InvalidSensorId(sensor_id, reader.stream_position()? - 2))
        }

        // relative timestamp
        reader.read(&mut time_us_bytes)?;
        let time_us = u64::from_le_bytes(time_us_bytes.clone());

        // measurement result
        reader.read(&mut result_bytes)?;

        let result: MeasurementResult =
            match FromPrimitive::from_u8(result_bytes[0]) {
                Some(value_type) => value_type,
                None => return Err(FormatError::InvalidMeasurementResult(sensor_log.id))
            };

        // measurement values
        let values =
            if result == MeasurementResult::Success {
                Some(parse_entry_values(reader, &sensor_log)?)
            } else {
                None
            };

        sensor_log.entries.push(SensorEntry { time_us, result, values });
    }
}

fn parse<R>(reader: &mut BufReader<R>) -> Result<DataLog, FormatError>
    where R : Read + Seek {

    let mut data_log = parse_header(reader)?;
    parse_entries(reader, &mut data_log)?;
    Ok(data_log)
}

fn print(data_log : &DataLog) {
    println!("Sensors:");

    for sensor in data_log.iter() {
        println!("    {}", sensor.name);
        println!("        id: {} period: {} ms", sensor.id, sensor.period_us as f64 / 1_000f64);
        println!("        values:");
        for value in sensor.values.iter() {
            println!("            {} : {:?} {}", value.name, value.value_type, value.unit);
        }
    }

    println!("Entries:");

    for sensor in data_log.iter() {
        println!("    {}", sensor.name);
        let mut entry_no = 0;
        for entry in sensor.entries.iter() {
            print!("        #{} t: {} ms : ", entry_no, entry.time_us as f64 / 1000f64);

            if entry.result == MeasurementResult::Success {
                for value in entry.values.iter() {
                    print!("{:?} ", value);
                }
            } else {
                print!("{:?}", entry.result);
            }

            println!();
            entry_no += 1;
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("Usage: {} <log.dl>", args[0]);
        return;
    }

    let filename = &args[1];

    // open file

    let mut reader =
        match File::open(filename) {
            Err(err) => {
                println!("Unable to read the file {}: {}", filename, err);
                return;
            }
            Ok(file) => BufReader::new(file)
        };

    // parse datalog

    let data_log =
        match parse(&mut reader) {
            Ok(dl) => dl,
            Err(err) => {
                println!("Failed to parse \"{}\": {:?}", filename, err);
                return;
            }
        };

    // print datalog contents

    print(&data_log);
}

