/*
   A mock CO2 sensor modelled after the SCD30
   https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/9.5_CO2/Sensirion_CO2_Sensors_SCD30_Interface_Description.pdf
   https://modbus.org/docs/Modbus_over_serial_line_V1.pdf

   Target:
     ATtiny25 via ATTinyCore

   Pinout:
     TX .. pin 5 (PB0)
     RX .. pin 6 (PB1)

   Provided functionality:
     modbus communication over UART @ 19200 baud
     RTU mode
     8 data bits, 1 start bit, 1 stop bit, no parity

     modbus address: 0x61

     5 basic sensor commands:
       Trigger continuous measurement
       Stop continuous measurement
       Set measurement interval
       Read measurement
       Read firmware version
*/

/* globals */
word g_interval;
bool g_measuring;

float g_co2_level, g_temperature, g_humidity;

#include <SoftwareSerial.h>

enum Func {
  ReadHoldReg = 3,
  ReadInputReg = 4,
  WriteHoldReg = 6
};

void print_crc(word crc) {
  Serial.print("#");
  for (int i = 3; i >= 0; i--) {
    char c = crc >> (i * 4) & 0b1111;

    if (c > 9) {
      c += 'A' - 10;
    } else {
      c += '0';
    }

    Serial.print(c);
  }
}

int crc16_modbus(byte * data, byte len) {
  // CRC16/MODBUS (6.2.2 CRC Generation)
  word crc = 0xFFFF;

  for (byte i = 0; i < len; i++) {
    byte c = data[i];

    crc ^= c;

    byte n = 0;
    while (n < 8) {
      byte lsb = crc & 1;
      crc >>= 1;
      if (lsb == 1) {
        crc ^= 0xA001;
      }
      n++;
    }
  }

  return crc;
}

void command_fw_ver() {
  // slave addr: 0x61
  Serial.write(0x61);
  // function code: 3
  Serial.write(0x03);
  // no. bytes: 2
  Serial.write(0x02);
  // fw major: 0x02
  Serial.write(0x02);
  // fw minor: 0x12
  Serial.write(0x12);
  // crc: 0x21B9
  Serial.write(0xB9);
  Serial.write(0x21);
}

void command_set_interval(word interval, word crc) {
  // slave addr: 0x61
  Serial.write(0x61);
  // function code: 6
  Serial.write(0x06);
  // addr: 0x0025
  Serial.write((byte)0x00);
  Serial.write(0x25);
  // content: interval
  Serial.write(interval >> 8);
  Serial.write(interval & 0xFF);
  // crc
  Serial.write(crc & 0xFF);
  Serial.write(crc >> 8);

  g_interval = interval;
}

void command_start_measurement() {
  // slave addr: 0x61
  Serial.write(0x61);
  // function code: 6
  Serial.write(0x06);
  // addr: 0x0036
  Serial.write((byte)0x00);
  Serial.write(0x36);
  // content: 0x0000
  Serial.write((byte)0x00);
  Serial.write((byte)0x00);
  // crc: 0x6064
  Serial.write(0x60);
  Serial.write(0x64);

  g_measuring = true;
}

void command_stop_measurement() {
  // slave addr: 0x61
  Serial.write(0x61);
  // function code: 6
  Serial.write(0x06);
  // addr: 0x0037
  Serial.write((byte)0x00);
  Serial.write(0x37);
  // content: 0x0001
  Serial.write((byte)0x00);
  Serial.write((byte)0x01);
  // crc: 0xF064
  Serial.write(0xF0);
  Serial.write(0x64);

  g_measuring = false;
}

void command_read_measurement() {
  // 61 03 0C 00 00 00 00 00 00 00 00 00 00 00 00 F3 58
  byte response[15] = { 0x61, 0x03, 0x0C };

  // (F32 BE)

  // response[3..=6]   .. g_co2_level
  for (byte i = 0; i < 4; i++)
    response[3+i] = ((uint8_t*)&g_co2_level)[3-i];

  // response[7..=10]  .. g_temperature
  for (byte i = 0; i < 4; i++)
    response[7+i] = ((uint8_t*)&g_temperature)[3-i];

  // response[11..=14] .. g_humidity
  for (byte i = 0; i < 4; i++)
    response[11+i] = ((uint8_t*)&g_humidity)[3-i];

  word crc = crc16_modbus(response, 15);

  // slave addr: 0x61
  Serial.write(0x61);
  // function code: 3
  Serial.write(0x03);
  // no.bytes: 0x0C
  Serial.write(0x0C);

  if (g_measuring == true) {
    for (byte i = 3; i < 15; i++)
      Serial.write((byte)response[i]);
    // crc
    Serial.write(crc & 0xFF);
    Serial.write(crc >> 8);
  } else {
    for (byte i = 0; i < 12; i++)
      Serial.write((byte)0x00);
    Serial.write(0xF3);
    Serial.write(0x58);
  }
}

void process_request(byte * frame, byte frame_offset) {

  // Parse the incoming RTU frame

  if (frame_offset < 4) {
    // too short of a frame, discart the request
    return;
  }

  byte address = frame[0];
  byte fncode = frame[1];
  word crc = frame[frame_offset - 1] << 8 | frame[frame_offset - 2];
  byte * data = frame + 2;
  byte data_len = frame_offset - 4;

  if (data_len != 4) return;

  word addr = data[0] << 8 | data[1];

  if (address != 0x61) {
    return;
  }

  if (crc16_modbus(frame, frame_offset - 2) != crc) {
    // invalid CRC, ignore request
    return;
  }

  switch(fncode) {
    case ReadHoldReg:
      {
        word noregs = data[2] << 8 | data[3];

        // Read firmware version
        if (addr == 0x20 && noregs == 1) {
          command_fw_ver();
        }

        if (addr == 0x28 && noregs == 0x06) {
          command_read_measurement();
        }
      }
      break;
    case ReadInputReg:
      break;
    case WriteHoldReg:
      {
        switch (addr) {
          case 0x25:
            command_set_interval((word(data[2]) << 8) | data[3], crc);
            break;
          case 0x36:
            command_start_measurement();
            break;
          case 0x37:
            command_stop_measurement();
            break;
        }
      }
      break;
    default:
      // invalid function code
      return;
  }
}

void setup() {
  Serial.begin(19200);
  Serial.setTimeout(10);

  g_interval = 5; // [2..1000]
  g_measuring = false;

  g_co2_level = 621.678212; // ppm
  g_temperature = 25.35;    // C
  g_humidity = 60.5568;     // %
}

void loop() {
  byte frame[256];
  byte i = 0;

  i = Serial.readBytes(frame, 256);

  if (i > 0) {
    process_request(frame, i);
  }
}
