use embedded_graphics::{
    mono_font::{ascii::*, MonoTextStyleBuilder},
    prelude::*,
    text::Text,
    image::Image,
    pixelcolor::BinaryColor,
    draw_target::DrawTarget,
    primitives::*
};
use tinybmp::Bmp;
use heapless::String;
use core::fmt::Write;
use unicode_truncate::UnicodeTruncateStr;

#[derive(Copy,Clone)]
pub enum MenuLink<'ml> {
    Some(&'ml Menu<'ml>),
    None,
    Pop
}

#[derive(Copy,Clone)]
pub enum Icon {
    List,
    New,
    File,
    Files,
    Storage,
    Partition,
    Sensor,
    Sensors,
    Info,
    About,
    Opt,
    Back
}

static ICONS: &'static [&[u8]] = &[
    include_bytes!("../assets/list.bmp"),
    include_bytes!("../assets/new.bmp"),
    include_bytes!("../assets/file.bmp"),
    include_bytes!("../assets/files.bmp"),
    include_bytes!("../assets/storage.bmp"),
    include_bytes!("../assets/partition.bmp"),
    include_bytes!("../assets/sensor.bmp"),
    include_bytes!("../assets/sensors.bmp"),
    include_bytes!("../assets/info.bmp"),
    include_bytes!("../assets/about.bmp"),
    include_bytes!("../assets/opt.bmp"),
    include_bytes!("../assets/back.bmp")
];

#[derive(Copy,Clone)]
pub enum Filler {
    About
}

static FILLER: &'static [&[u8]] = &[
    include_bytes!("../assets/about_filler.bmp")
];

pub struct MenuOption<'mo> {
    pub label: &'mo str,
    pub icon: Icon,
    pub link: MenuLink<'mo>
}

pub enum Menu<'m> {
    List {
        name: &'m str,
        icon: Icon,
        options: &'m [MenuOption<'m>]
    },
    Info {
        name: &'m str,
        icon: Icon,
        text: &'m str,
        filler: Option<Filler>
    },
    Log {
        name: &'m str,
        inprog: bool,
        sensor_count: u32,
        sample_count: u32,
        log_size: u32
    }
}

// try to convert this into a macro crate
impl Menu<'_> {
    pub fn len<'m>(&'m self) -> usize {
        match self {
            Menu::List{name, icon, options} => options.len(),
            Menu::Info{name, icon, text, filler} => 0,
            Menu::Log{name, inprog, sensor_count, sample_count, log_size} => if *inprog { 1 } else { 2 }
        }
    }
}

fn format_amount(value: &u32) -> String<5> {
    let mut value_str : String<20> = String::new();
    let mut amount_str : String<5> = String::new();
    let div_val : f32;
    let mult_char : Option<char>;

    match value {
        0..=999 => {
            div_val = *value as f32;
            mult_char = None;
        },
        1_000..=999_999 => {
            div_val = *value as f32 / 1_000f32;
            mult_char = Some('K');
        },
        1_000_000..=999_999_999 => {
            div_val = *value as f32 / 1_000_000f32;
            mult_char = Some('M');
        },
        _ => {
            div_val = *value as f32 / 1_000_000_000f32;
            mult_char = Some('G');
        }
    }

    // write division result to string
    write!(&mut value_str, "{}", div_val).ok();

    // truncate value to 3..5 unicode graphemes, add optional multiplier character
    if let Some(chr) = mult_char {
        let (trunc_value_str, _) = value_str.as_str().unicode_truncate(
                if value_str.chars().nth(3) == Some('.') {3} else {4});
        write!(&mut amount_str, "{}{}", trunc_value_str, chr).ok();
    } else {
        let (trunc_value_str, _) = value_str.as_str().unicode_truncate(
            if value_str.chars().nth(4) == Some('.') {4} else {5});
        write!(&mut amount_str, "{}", trunc_value_str).ok();
    }

    amount_str
}

fn draw_list<D>(display: &mut D, menu_idx: &usize, name: &str, icon: &Icon, options: &[MenuOption])
    where D: DrawTarget<Color = BinaryColor> {

    let style = MonoTextStyleBuilder::new()
        .font(&FONT_5X7)
        .text_color(BinaryColor::On)
        .background_color(BinaryColor::Off)
        .build();

    let style_inverse = MonoTextStyleBuilder::new()
        .font(&FONT_5X7)
        .text_color(BinaryColor::Off)
        .background_color(BinaryColor::On)
        .build();

    let style_smaller = MonoTextStyleBuilder::new()
        .font(&FONT_4X6)
        .text_color(BinaryColor::On)
        .background_color(BinaryColor::Off)
        .build();

    // determine menu offset
    let offset: usize;
    let offset_max = options.len() - 3;
    let no_options = options.len() as i32;

    if *menu_idx <= 1 {
        offset = 0;
    } else if *menu_idx >= options.len() - 2 {
        offset = offset_max;
    } else {
        offset = *menu_idx as usize - 1;
    }

    // determine scrollbar sizing
    let rect_height = 22 / no_options as u32 * 3;
    let rect_pos =
        if offset == 0 { 10 }
        else if offset < offset_max { 10 + (22.0 / no_options as f32 * offset as f32) as i32 }
        else { (32 - rect_height) as i32 };

    let rect = Rectangle::new(Point::new(115, rect_pos), Size::new(6, rect_height))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1));

    let title_text = Text::new(
        name,
        Point::new(14, 6),
        style
    );

    let title_num_str = format!("{}/{}", *menu_idx + 1, options.len());

    let title_num = Text::new(
        title_num_str.as_str(),
        Point::new(122 - title_num_str.len() as i32 * 4, 6),
        style_smaller
    );

    let icon_bmp = Bmp::<BinaryColor>::from_slice(ICONS[*icon as usize]).unwrap();
    let title_image = Image::new(&icon_bmp, Point::new(6, 2));

    let title_line = Line::new(Point::new(0, 9), Point::new(127, 9))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1));

    rect.draw(display).ok();
    title_line.draw(display).ok();
    title_image.draw(display).ok();
    title_text.draw(display).ok();
    title_num.draw(display).ok();

    for i in 0..=2 {
        let option_text = Text::new(
            options[i + offset].label,
            Point::new(14, 9 + 7 * (i as i32 + 1)),
            if i + offset == *menu_idx { style_inverse } else { style }
        );

        let option_bmp = Bmp::<BinaryColor>::from_slice(ICONS[options[i + offset].icon as usize]).unwrap();
        let option_image = Image::new(&option_bmp, Point::new(6, 5 + 7 * (i as i32 + 1)));

        option_text.draw(display).ok();
        option_image.draw(display).ok();
    }
}

fn draw_info<D>(display: &mut D, menu_idx: &usize, name: &str, icon: &Icon, text: &str, filler: &Option<Filler>)
    where D: DrawTarget<Color = BinaryColor> {

    let style = MonoTextStyleBuilder::new()
        .font(&FONT_5X7)
        .text_color(BinaryColor::On)
        .background_color(BinaryColor::Off)
        .build();

    let title_text = Text::new(
        name,
        Point::new(14, 6),
        style
    );

    let icon_bmp = Bmp::<BinaryColor>::from_slice(ICONS[*icon as usize]).unwrap();
    let title_image = Image::new(&icon_bmp, Point::new(6, 2));

    let title_line = Line::new(Point::new(0, 9), Point::new(127, 9))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1));

    let body_text = Text::new(text, Point::new(8, 19), style);

    title_line.draw(display).ok();
    title_image.draw(display).ok();
    title_text.draw(display).ok();
    body_text.draw(display).ok();

    if let Some(filler) = filler {
        let about_filler_bmp = Bmp::<BinaryColor>::from_slice(FILLER[*filler as usize]).unwrap();
        let filler_image = Image::new(&about_filler_bmp, Point::new(60, 12));
        filler_image.draw(display).ok();
    }
}

fn draw_log<D>(display: &mut D, menu_idx: &usize, name: &str, inprog: &bool, sensor_count: &u32, sample_count: &u32, log_size: &u32)
    where D: DrawTarget<Color = BinaryColor> {

    let style = MonoTextStyleBuilder::new()
        .font(&FONT_5X7)
        .text_color(BinaryColor::On)
        .background_color(BinaryColor::Off)
        .build();

    let style_inverse = MonoTextStyleBuilder::new()
        .font(&FONT_5X7)
        .text_color(BinaryColor::Off)
        .background_color(BinaryColor::On)
        .build();

    let title_text = Text::new(
        name,
        Point::new(14, 6),
        style
    );

    let status_text = Text::new(
        if *inprog { "inprog" } else { "halted" },
        Point::new(84, 6),
        style
    );

    let icon_bmp = Bmp::<BinaryColor>::from_slice(ICONS[Icon::File as usize]).unwrap();
    let title_image = Image::new(&icon_bmp, Point::new(6, 2));

    let title_line = Line::new(Point::new(0, 9), Point::new(127, 9))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1));

    title_line.draw(display).ok();
    title_image.draw(display).ok();
    title_text.draw(display).ok();
    status_text.draw(display).ok();

    // no samples, log size, no sensors
    // sensor_count, sample_count, log_size

    let mut stats_text_str : String<50> = String::new();
    let sample_count_str = format_amount(sample_count);
    let log_size_str = format_amount(log_size);
    // "sensors:     4\nsamples: 1034K\n   size:   10M",
    write!(&mut stats_text_str, "sensors: {:>5}\nsamples: {:>5}\n   size: {:>5}", sensor_count, sample_count_str, log_size_str).ok();

    let stats_text = Text::new(
        stats_text_str.as_str(),
        Point::new(6, 15),
        style
    );
    stats_text.draw(display).ok();

    // stop/resume, quit
    if *inprog {
        let stop_button_text = Text::new(
            " stop ",
            Point::new(88, 22),
            style_inverse
        );
        stop_button_text.draw(display).ok();
    } else {
        let resume_button_text = Text::new(
            "resume",
            Point::new(88, 19),
            if *menu_idx % 2 == 0 { style_inverse } else { style }
        );
        let quit_button_text = Text::new(
            " quit ",
            Point::new(88, 26),
            if *menu_idx % 2 == 1 { style_inverse } else { style }
        );
        resume_button_text.draw(display).ok();
        quit_button_text.draw(display).ok();
    }
}

pub fn draw_menu<D>(display: &mut D, menu: &Menu, menu_idx: &usize)
    where D: DrawTarget<Color = BinaryColor> {

    match menu {
        Menu::List{name, icon, options} => draw_list(display, menu_idx, name, icon, options),
        Menu::Info{name, icon, text, filler} => draw_info(display, menu_idx, name, icon, text, filler),
        Menu::Log{name, inprog, sensor_count, sample_count, log_size} => draw_log(display, menu_idx, name, inprog, sensor_count, sample_count, log_size)
    }
}
