#![allow(dead_code)]
#![allow(unused_variables)]

use heapless::Deque;
use std::{thread::sleep, time::Duration};
use embedded_graphics::{
    prelude::*,
    pixelcolor::BinaryColor,
    draw_target::DrawTarget,
};
use embedded_graphics_simulator::{
    OutputSettingsBuilder, SimulatorDisplay, SimulatorEvent, Window, sdl2::Keycode
};

mod menu;
use crate::menu::{Menu, MenuOption, MenuLink, Icon, Filler, draw_menu};

fn main() -> Result<(), core::convert::Infallible> {
    let output_settings = OutputSettingsBuilder::new().scale(3).build();
    let mut display: SimulatorDisplay<BinaryColor> = SimulatorDisplay::new(Size::new(128, 32));
    let mut window = Window::new("", &output_settings);

    let log_menu = Menu::Log {
        name: "Log 5.dl",
        inprog: false,
        sensor_count: 4,
        sample_count: 1_345_123,
        log_size: 3_121_001_234
    };

    let file_menu = Menu::List {
        name: "File list",
        icon: Icon::Files,
        options: &[
            MenuOption {label: "Log folder 1", icon: Icon::Files, link: MenuLink::None},
            MenuOption {label: "Log folder 2", icon: Icon::Files, link: MenuLink::None},
            MenuOption {label: "Log 1.dl", icon: Icon::File, link: MenuLink::None},
            MenuOption {label: "Log 2.dl", icon: Icon::File, link: MenuLink::None},
            MenuOption {label: "Log 3.dl", icon: Icon::File, link: MenuLink::None},
            MenuOption {label: "Log 4.dl", icon: Icon::File, link: MenuLink::None},
            MenuOption {label: "Back to menu", icon: Icon::Back, link: MenuLink::Pop}
        ]
    };

    let sensor_menu = Menu::List {
        name: "Sensors",
        icon: Icon::Sensors,
        options: &[
            MenuOption {label: "Dht11", icon: Icon::Sensor, link: MenuLink::None},
            MenuOption {label: "Mcp9808", icon: Icon::Sensor, link: MenuLink::None},
            MenuOption {label: "Adxl345", icon: Icon::Sensor, link: MenuLink::None},
            MenuOption {label: "Mock CO2", icon: Icon::Sensor, link: MenuLink::None},
            MenuOption {label: "Back to menu", icon: Icon::Back, link: MenuLink::Pop}
        ]
    };

    let storage_menu = Menu::List {
        name: "Storage",
        icon: Icon::Storage,
        options: &[
            MenuOption {label: "Out. dir", icon: Icon::Files, link: MenuLink::None},
            MenuOption {label: "Partition", icon: Icon::Partition, link: MenuLink::None},
            MenuOption {label: "Back to menu", icon: Icon::Back, link: MenuLink::Pop}
        ]
    };

    let sysinfo_menu = Menu::Info {
        name: "System info",
        icon: Icon::Info,
        filler: None,
        text: "Free space: 324MB/64GB\nLogs created: 24"
    };

    let about_menu = Menu::Info {
        name: "About",
        icon: Icon::About,
        filler: Some(Filler::About),
        text: "Ver. 0.1\nros0061"
    };

    let main_menu = Menu::List {
        name: "Menu",
        icon: Icon::List,
        options: &[
            MenuOption {label: "New datalog", icon: Icon::New, link: MenuLink::Some(&log_menu)},
            MenuOption {label: "List files", icon: Icon::Files, link: MenuLink::Some(&file_menu)},
            MenuOption {label: "Sensor setup", icon: Icon::Sensors, link: MenuLink::Some(&sensor_menu)},
            MenuOption {label: "Storage setup", icon: Icon::Storage, link: MenuLink::Some(&storage_menu)},
            MenuOption {label: "System info", icon: Icon::Info, link: MenuLink::Some(&sysinfo_menu)},
            MenuOption {label: "About", icon: Icon::About, link: MenuLink::Some(&about_menu)}
        ]
    };

    let mut menu: &Menu = &main_menu;
    let mut menu_idx: usize = 0;
    let mut menu_history = Deque::<&Menu, 8>::new();

    'r: loop {
        display.clear(BinaryColor::Off)?;

        draw_menu(&mut display, menu, &menu_idx);

        window.update(&display);

        for e in window.events() {
            match e {
                SimulatorEvent::Quit => break 'r,
                SimulatorEvent::KeyDown{keycode, keymod, repeat} => {
                    if keycode == Keycode::Up {
                        menu_idx = if menu_idx > 0 { menu_idx - 1 } else { menu_idx };
                    }
                    else if keycode == Keycode::Down {
                        menu_idx = if menu.len() > 0 && menu_idx < (menu.len() - 1) { menu_idx + 1 } else { menu_idx };
                    }
                    else if keycode == Keycode::Return {
                        match menu {
                            Menu::List {options, ..} => {
                                if let MenuLink::Some(link) = options[menu_idx].link {
                                    menu_idx = 0;
                                    if menu_history.len() == menu_history.capacity() {
                                        menu_history.pop_front();
                                    }
                                    menu_history.push_back(menu).ok();
                                    menu = &link;
                                }
                                else if let MenuLink::Pop = options[menu_idx].link {
                                    menu_idx = 0;
                                    match menu_history.pop_back() {
                                        Some(parent) => menu = parent,
                                        None => menu = &main_menu
                                    };
                                }
                            },
                            Menu::Log {inprog, ..} => {
                                if menu_idx % 2 == 1 {
                                    menu_idx = 0;
                                    match menu_history.pop_back() {
                                        Some(parent) => menu = parent,
                                        None => menu = &main_menu
                                    };
                                }
                            },
                            _ => {
                                menu_idx = 0;
                                match menu_history.pop_back() {
                                    Some(parent) => menu = parent,
                                    None => menu = &main_menu
                                };
                            }
                        }
                    }
                },
                _ => {}
            }
        }

        sleep(Duration::from_millis(33));
    }

    Ok(())
}
