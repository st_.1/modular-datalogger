/*
 * Menu: drawing routines
 *
 * Reads immutable (Menu) and mutable (MenuState) menu data
 * and renders it on the display
 */

use embedded_graphics::{
    mono_font::{ascii::*, MonoTextStyleBuilder},
    prelude::*,
    text::Text,
    image::Image,
    pixelcolor::BinaryColor,
    draw_target::DrawTarget,
    primitives::*
};
use tinybmp::Bmp;
use heapless::String;
use core::{fmt::Write, cmp::max};
use unicode_truncate::UnicodeTruncateStr;

use crate::menu::{list_drawable::*, bitmaps::*, structure::*, state::*, sensor_submenu::*, text_style::*, num_fmt::*};
use crate::storage::{StorageStats, FileList};
use crate::sensor::{SensorConfig, SensorEntryState, PresetList, DatalogState};

use super::sensor_submenu::SensorMenuConfig;

fn draw_titlebar<D>(display: &mut D, name: &str, icon: &Icon)
    where D: DrawTarget<Color = BinaryColor> {

    let (style_regular, _, _) = build_text_styles();

    let title_text = Text::new(
        name,
        Point::new(14, 6),
        style_regular
    );

    let icon_bmp = Bmp::<BinaryColor>::from_slice(ICONS[*icon as usize]).unwrap();
    let title_image = Image::new(&icon_bmp, Point::new(6, 2));

    let title_line = Line::new(Point::new(0, 9), Point::new(127, 9))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1));

    title_line.draw(display).ok();
    title_image.draw(display).ok();
    title_text.draw(display).ok();
}

fn draw_sensor_state<D>(
        display: &mut D,
        option_display_idx: usize,
        sensor_config: &SensorConfig,
        sensor_menu_config: SensorMenuConfig,
        sensor_menu_var: SensorMenuVar
    )
    where D: DrawTarget<Color = BinaryColor> {

    // Return if there is nothing to draw
    if sensor_menu_var == SensorMenuVar::None {
        return;
    }

    let (style_regular, _, _) = build_text_styles();

    // Get SensorConfig entry state
    let entry_state: SensorEntryState = sensor_config.entry_state(sensor_menu_config, sensor_menu_var);

    let mut entry_state_str: String<8> = String::new();

    let option_state_str = match entry_state {
        SensorEntryState::Boolean(var) => {
            if var { "enabled" } else { "disabled" }
        },
        SensorEntryState::Period(period_us) => {
            entry_state_str = format_period(period_us);
            entry_state_str.as_str()
        },
        SensorEntryState::DataRate(data_rate) => {
            entry_state_str = format_data_rate(data_rate);
            entry_state_str.as_str()
        },
        SensorEntryState::Range(range) => {
            write!(&mut entry_state_str, "{}", range).ok();
            entry_state_str.as_str()
        }
        _ => {""}
    };

    let option_state_text = Text::new(
        option_state_str,
        Point::new(72, 9 + 7 * (option_display_idx as i32 + 1)),
        style_regular
    );

    option_state_text.draw(display).ok();
}

fn draw_list<'ld, D, E: 'ld, L>(
        display: &mut D,
        option_idx: usize,
        options: &'ld L,
        name: &str,
        icon: &Icon,
        sensor_res: Option<(&SensorConfig, SensorMenuConfig)>
    )
    where D: DrawTarget<Color = BinaryColor>,
          E: ListDrawableEntry<'ld>,
          L: ListDrawable<'ld, E> {

    let (style_regular, style_inverse, style_smaller) = build_text_styles();

    // determine menu offset
    let offset: usize;
    let offset_max = max(options.len() as i32 - 3, 0) as usize;
    let no_options = options.len() as i32;

    if option_idx <= 1 {
        offset = 0;
    } else if option_idx >= options.len() - 2 {
        offset = offset_max;
    } else {
        offset = option_idx as usize - 1;
    }

    // determine scrollbar sizing
    let rect_height = 22 / no_options as u32 * 3;
    let rect_pos =
        if offset == 0 { 10 }
        else if offset < offset_max { 10 + (22.0 / no_options as f32 * offset as f32) as i32 }
        else { (32 - rect_height) as i32 };

    let rect = Rectangle::new(Point::new(115, rect_pos), Size::new(6, rect_height))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1));

    let mut title_num_str : String<8> = String::new();
    write!(title_num_str, "{}/{}", option_idx + 1, options.len()).ok();

    let title_num = Text::new(
        title_num_str.as_str(),
        Point::new(122 - title_num_str.len() as i32 * 4, 6),
        style_smaller
    );

    // draw title bar, scrollbar
    draw_titlebar(display, name, icon);
    if options.len() > 3 {
        rect.draw(display).ok();
    }
    title_num.draw(display).ok();

    // draw options
    if options.busy() {
        let busy_text = Text::new(
            "Loading entries...",
            Point::new(14, 25),
            style_regular
        );
        busy_text.draw(display).ok();
        return;
    }

    for i in 0..=2 {
        if let Some(option) = options.option(i + offset) {
            let option_text = Text::new(
                option.label(),
                Point::new(14, 9 + 7 * (i as i32 + 1)),
                if i + offset == option_idx { style_inverse } else { style_regular }
            );

            let option_bmp = Bmp::<BinaryColor>::from_slice(ICONS[option.icon() as usize]).unwrap();
            let option_image = Image::new(&option_bmp, Point::new(6, 5 + 7 * (i as i32 + 1)));

            option_text.draw(display).ok();
            option_image.draw(display).ok();

            // optional sensor state
            if let Some((sensor_config, sensor_menu_config)) = sensor_res {
                draw_sensor_state(display, i, sensor_config, sensor_menu_config, option.sensor_var());
            }
        }
    }
}

fn draw_info<D>(display: &mut D, name: &str, icon: &Icon, text: &str, filler: &Option<Filler>)
    where D: DrawTarget<Color = BinaryColor> {

    let (style_regular, _, _) = build_text_styles();

    let body_text = Text::new(text, Point::new(8, 19), style_regular);

    draw_titlebar(display, name, icon);
    body_text.draw(display).ok();

    if let Some(filler) = filler {
        let about_filler_bmp = Bmp::<BinaryColor>::from_slice(FILLER[*filler as usize]).unwrap();
        let filler_image = Image::new(&about_filler_bmp, Point::new(60, 12));
        filler_image.draw(display).ok();
    }
}

fn draw_sys_info<D>(display: &mut D, storage_stats: &Option<StorageStats>, name: &str, icon: &Icon)
    where D: DrawTarget<Color = BinaryColor> {
    let mut text: String<50> = String::new();

    match storage_stats {
        Some(storage_stats) => {
            write!(text, "Free space: {}/{}\nLogs created: {}",
                   format_stat(storage_stats.free_space),
                   format_stat(storage_stats.total_space),
                   format_stat(storage_stats.no_logs)).ok();
        },
        None => {
            write!(text, "Loading stats...").ok();
        }
    }

    draw_info(display, name, icon, &text, &None);
}

fn draw_preset_list<'ld, D>(
        display: &mut D,
        option_idx: usize,
        preset_list: &Option<PresetList>,
        name: &str,
        icon: &Icon,
    )
    where D: DrawTarget<Color = BinaryColor> {

    if let Some(preset_list) = preset_list {
        draw_list(display, option_idx, preset_list, name, icon, None);
    } else {
        draw_info(display, name, icon, &"Failed to retrieve\npreset list.", &None);
    }
}

fn draw_file_list<D>(
        display: &mut D,
        option_idx: usize,
        file_list: &Option<FileList>,
        name: &str,
        icon: &Icon
    )
    where D: DrawTarget<Color = BinaryColor> {

    match file_list {
        Some(file_list) => draw_list(display, option_idx, file_list, name, icon, None),
        None => draw_info(display, name, icon, &"Loading file list...", &None)
    }
}

fn draw_datalog<'ld, D>(display: &mut D, icon: &Icon, menu_state: &MenuState, datalog_state: &DatalogState)
    where D: DrawTarget<Color = BinaryColor> {

    draw_titlebar(display, datalog_state.filename.as_str(), icon);

    let (style_regular, style_inverse, _) = build_text_styles();

    // status info
    let status_text = Text::new(
        if datalog_state.inprog { "inprog" } else { "halted" },
        Point::new(84, 6),
        style_regular
    );

    status_text.draw(display).ok();

    // no samples, log size, no sensors
    let mut stats_text_str : String<50> = String::new();
    let measured_count_str = format_stat(datalog_state.no_measured as u64);
    let dropped_count_str = format_stat(datalog_state.no_dropped as u64);
    let log_size_str = format_stat(datalog_state.size as u64);
    // number of sensors, no. successful measurement, no. dropped measurements, current size
    write!(&mut stats_text_str, "sensors: {:>5}\nm:{:>5} d:{:>5}\nsize: {:>5}", datalog_state.no_sensors, measured_count_str, dropped_count_str, log_size_str).ok();

    let stats_text = Text::new(
        stats_text_str.as_str(),
        Point::new(6, 15),
        style_regular
    );
    stats_text.draw(display).ok();

    // stop/resume, quit
    if datalog_state.inprog {
        let stop_button_text = Text::new(
            " stop ",
            Point::new(88, 22),
            style_inverse
        );
        stop_button_text.draw(display).ok();
    } else {
        let resume_button_text = Text::new(
            "resume",
            Point::new(88, 19),
            if menu_state.option_idx % 2 == 0 { style_inverse } else { style_regular }
        );
        let quit_button_text = Text::new(
            " quit ",
            Point::new(88, 26),
            if menu_state.option_idx % 2 == 1 { style_inverse } else { style_regular }
        );
        resume_button_text.draw(display).ok();
        quit_button_text.draw(display).ok();
    }
}

pub fn draw_menu<D>(display: &mut D, menu_state: &MenuState, datalog_state: &DatalogState, sensor_config: &SensorConfig)
    where D: DrawTarget<Color = BinaryColor> {

    match (*menu_state).current_menu {
        Menu::List {name, icon, options} => draw_list(display, menu_state.option_idx, options, name, icon, None),
        Menu::Info {name, icon, text, filler} => draw_info(display, name, icon, text, filler),
        Menu::SysInfo {name, icon} => draw_sys_info(display, &menu_state.storage_stats, name, icon),
        Menu::FileList {name, icon, dir_selection: _} => draw_file_list(display, menu_state.option_idx, &menu_state.file_list, name, icon),
        Menu::PresetList {name, icon} => draw_preset_list(display, menu_state.option_idx, &menu_state.preset_list, name, icon),
        Menu::SensorList {name, icon, config, options} => draw_list(display, menu_state.option_idx, options, name, icon, Some((sensor_config, *config))),
        Menu::PartitionList {name, icon} => draw_list(display, menu_state.option_idx, &menu_state.part_table, name, icon, None),
        Menu::Datalog {icon} => draw_datalog(display, icon, menu_state, datalog_state)
    }
}

pub fn draw_error<D>(display: &mut D, title: &str, message: &str)
    where D: DrawTarget<Color = BinaryColor> {
   draw_info(display, title, &Icon::Err, message, &None);
}
