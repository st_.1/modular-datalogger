use ::adxl345::OutputDataRate;
use heapless::String;
use core::fmt::Write;
use unicode_truncate::UnicodeTruncateStr;

pub fn format_data_rate(val: OutputDataRate) -> String<8> {
    let mut result_str : String<8> = String::new();
    let data_rate_ft : f32 = val.into();
    write!(&mut result_str, "{} Hz", data_rate_ft).ok();

    result_str
}

pub fn format_period(val: u64) -> String<8> {
    let mut value_str : String<20> = String::new();
    let mut result_str : String<8> = String::new();
    let suffix: &str;
    let val_flt = val as f32;
    let div_val: f32;

    match val {
        0..=999 => {
            div_val = val_flt;
            suffix = &"us";
        },
        1_000..=999_999 => {
            div_val = val_flt / 1_000f32;
            suffix = &"ms";
        },
        1_000_000..=999_999_999 => {
            div_val = val_flt / 1_000_000f32;
            suffix = &"s";
        },
        _ => {
            div_val = val_flt / 1_000_000_000f32;
            suffix = &"Ks";
        }
    };

    // write division result to string
    write!(&mut value_str, "{}", div_val).ok();

    // truncate value to 4..5 unicode graphemes, add suffix
    let (trunc_value_str, _) = value_str.as_str().unicode_truncate(
        if value_str.chars().nth(4) == Some('.') {4} else {5});

    write!(&mut result_str, "{} {}", trunc_value_str, suffix).ok();

    result_str
}

pub fn format_stat(val: u64) -> String<5> {
    let mut value_str : String<20> = String::new();
    let mut amount_str : String<5> = String::new();
    let val_flt = val as f32;
    let div_val : f32;
    let mult_char : Option<char>;

    match val {
        0..=999 => {
            div_val = val_flt;
            mult_char = None;
        },
        1_000..=999_999 => {
            div_val = val_flt / 1_000f32;
            mult_char = Some('K');
        },
        1_000_000..=999_999_999 => {
            div_val = val_flt / 1_000_000f32;
            mult_char = Some('M');
        },
        _ => {
            div_val = val_flt / 1_000_000_000f32;
            mult_char = Some('G');
        }
    }

    // write division result to string
    write!(&mut value_str, "{}", div_val).ok();

    // truncate value to 3..5 unicode graphemes, add optional multiplier character
    if let Some(chr) = mult_char {
        let (trunc_value_str, _) = value_str.as_str().unicode_truncate(
                if value_str.chars().nth(3) == Some('.') {3} else {4});
        write!(&mut amount_str, "{}{}", trunc_value_str, chr).ok();
    } else {
        let (trunc_value_str, _) = value_str.as_str().unicode_truncate(
            if value_str.chars().nth(4) == Some('.') {4} else {5});
        write!(&mut amount_str, "{}", trunc_value_str).ok();
    }

    amount_str
}
