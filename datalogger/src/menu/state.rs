/*
 * Menu: MenuState
 *
 * Stores all mutable vars of the menu system
 */

#![allow(dead_code)]

use super::{PartTableSubmenu, structure::{Menu, MAIN_MENU}};
use crate::storage::{FileList, StorageStats, GenericMbrTable};
use crate::sensor::{SensorConfig, PresetList};
use heapless::Deque;

struct MenuHistoryEntry<'mhe> {
    pub menu: &'mhe Menu<'mhe>,
    pub option_idx : usize
}

pub struct MenuState<'ms> {
    pub current_menu: &'ms Menu<'ms>,
    pub option_idx: usize,
    menu_history: Deque::<MenuHistoryEntry<'ms>, 8>,
    pub storage_stats: Option<StorageStats>,
    pub file_list: Option<FileList>,
    pub preset_list: Option<PresetList>,
    pub part_table: PartTableSubmenu
}

impl<'ms> MenuState<'ms> {
    pub fn new(part_table: PartTableSubmenu) -> MenuState<'ms> {
        MenuState {
            option_idx: 0,
            current_menu: &MAIN_MENU,
            menu_history: Deque::<MenuHistoryEntry, 8>::new(),
            storage_stats: None,
            file_list: None,
            preset_list: None,
            part_table
        }
    }

    pub fn select_option(&mut self, menu: &'ms Menu) {
        let menu_history = &mut self.menu_history;

        // historize current menu & option index
        if menu_history.len() == menu_history.capacity() {
            menu_history.pop_front();
        }

        let menu_history_entry = MenuHistoryEntry {
            menu: self.current_menu,
            option_idx: self.option_idx
        };

        menu_history.push_back(menu_history_entry).ok();

        // set menu to selected, reset option index
        self.current_menu = &menu;
        self.option_idx = 0;
    }

    pub fn pop_back(&mut self) {
        // go back one entry in menu history
        match self.menu_history.pop_back() {
            Some(parent) => {
                self.current_menu = parent.menu;
                self.option_idx = parent.option_idx;
            },
            None => {
                self.current_menu = &MAIN_MENU;
                self.option_idx = 0;
            }
        }
    }

    pub fn len(&self) -> usize {
        match self.current_menu {
            Menu::FileList {name: _, icon: _, dir_selection: _} => {
                match &self.file_list {
                    Some(file_list) => { file_list.len + file_list.frame_idx_offset() }
                    None => 0
                }
            },
            Menu::SensorList {name: _, icon: _, config: _, options} => options.len(),
            Menu::List {name: _, icon: _, options} => options.len(),
            Menu::PresetList {name: _, icon: _} => {
                if let Some(preset_list) = self.preset_list { preset_list.len() } else { 0 }
            },
            Menu::Datalog {icon: _} => 2,
            Menu::PartitionList { name: _, icon: _ } => { self.part_table.len() }
            _ => 0
        }
    }

    // Adjusts menu state to a partition change
    pub fn set_part(&mut self, idx: usize) {
        // Change selected partition in the partition menu
        self.part_table.mark_selected(idx);
    }
}
