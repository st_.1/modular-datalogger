/*
 * Menu: text styles
 *
 * embedded_graphics text styles for the drawing routines
 */

use embedded_graphics::{
    mono_font::{ascii::*, MonoTextStyleBuilder, MonoTextStyle},
    prelude::*,
    pixelcolor::BinaryColor,
};

pub fn build_text_styles<'bts>()
    -> (MonoTextStyle<'bts, BinaryColor>,
        MonoTextStyle<'bts, BinaryColor>,
        MonoTextStyle<'bts, BinaryColor>)
{
    let regular = MonoTextStyleBuilder::new()
        .font(&FONT_5X7)
        .text_color(BinaryColor::On)
        .background_color(BinaryColor::Off)
        .build();

    let inverse = MonoTextStyleBuilder::new()
        .font(&FONT_5X7)
        .text_color(BinaryColor::Off)
        .background_color(BinaryColor::On)
        .build();

    let smaller = MonoTextStyleBuilder::new()
        .font(&FONT_4X6)
        .text_color(BinaryColor::On)
        .background_color(BinaryColor::Off)
        .build();

    (regular, inverse, smaller)
}
