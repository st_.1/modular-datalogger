/*
 * Menu: list drawable traits
 *
 * Represents a list of drawable menu items
 */

use crate::menu::{structure::{Menu, MenuOption}, bitmaps::Icon, sensor_submenu::SensorOption};
use crate::storage::{FileList, FileListEntry, FileListEntryType, FrameDirection};
use crate::sensor::{PresetList, PresetListEntry};
use super::{SensorMenuVar, PartTableSubmenu, PartTableSubmenuEntry};

pub trait ListDrawable<'ld, E>
    where E: ListDrawableEntry<'ld> {

    fn len(&self) -> usize;
    fn option(&self, idx: usize) -> Option<&E>;
    fn busy(&self) -> bool;
}

pub trait ListDrawableEntry<'ld> {
    fn label(&'ld self) -> &'ld str;
    fn icon(&self) -> Icon;
    fn sensor_var(&self) -> SensorMenuVar;
}

impl<'ld> ListDrawable<'ld, MenuOption<'ld>> for &'ld[MenuOption<'ld>] {
    fn len(&self) -> usize {
        <[MenuOption]>::len(self)
    }

    fn option(&self, idx: usize) -> Option<&MenuOption<'ld>> {
        if idx < self.len() {
            Some(&self[idx])
        } else {
            None
        }
    }

    fn busy(&self) -> bool {
        false
    }
}

impl<'ld> ListDrawable<'ld, FileListEntry> for FileList {
    fn len(&self) -> usize {
        self.len + self.frame_idx_offset()
    }

    fn option(&self, idx: usize) -> Option<&FileListEntry> {
        // back to menu @ pos 0
        if idx == 0 {
            return Some(&self.exit_entry);
        }

        if idx == 1 && self.dir_selection {
            return Some(&self.sel_entry);
        }

        // file list itself, offset by 1
        let frame_idx = idx as i32 - self.offset as i32 - self.frame_idx_offset() as i32;

        if frame_idx >= 0 && frame_idx < self.frame.len() as i32 {
            Some(&self.frame[frame_idx as usize])
        } else {
            None
        }
    }

    fn busy(&self) -> bool {
        uprintln!("self.busy: {}", self.busy);
        self.busy
    }
}

impl<'ld> ListDrawable<'ld, SensorOption<'ld>> for &'ld[SensorOption<'ld>] {
    fn len(&self) -> usize {
        self[..].len()
    }

    fn option(&self, idx: usize) -> Option<&SensorOption<'ld>> {
        if idx < self.len() {
            Some(&self[idx])
        } else {
            None
        }
    }

    fn busy(&self) -> bool {
        false
    }
}

impl<'ld> ListDrawable<'ld, PresetListEntry> for PresetList {
    fn len(&self) -> usize {
        <[PresetListEntry]>::len(self)
    }

    fn option(&self, idx: usize) -> Option<&PresetListEntry> {
        if idx < self.len() {
            Some(&self[idx])
        } else {
            None
        }
    }

    fn busy(&self) -> bool {
        false
    }
}

impl<'ld> ListDrawable<'ld, PartTableSubmenuEntry> for PartTableSubmenu {
    fn len(&self) -> usize {
        self.len()
    }

    fn option(&self, idx: usize) -> Option<&PartTableSubmenuEntry> {
        if idx < self.len() {
            Some(&self.entry(idx))
        } else {
            None
        }
    }

    fn busy(&self) -> bool {
        false
    }
}

impl<'ld> ListDrawableEntry<'ld> for MenuOption<'ld> {
    fn label(&'ld self) -> &'ld str {
        self.label
    }

    fn icon(&self) -> Icon {
        self.icon
    }

    fn sensor_var(&self) -> SensorMenuVar {
        SensorMenuVar::None
    }
}

impl<'ld> ListDrawableEntry<'ld> for FileListEntry {
    fn label(&'ld self) -> &'ld str {
        self.name.as_str()
    }

    fn icon(&self) -> Icon {
        match self.ftype {
            FileListEntryType::Folder => Icon::Files,
            FileListEntryType::Log => Icon::File,
            FileListEntryType::Back => Icon::Back,
            FileListEntryType::Select => Icon::Select,
            _ => Icon::Opt
        }
    }

    fn sensor_var(&self) -> SensorMenuVar {
        SensorMenuVar::None
    }
}

impl<'ld> ListDrawableEntry<'ld> for SensorOption<'ld> {
    fn label(&'ld self) -> &'ld str {
        self.name
    }

    fn icon(&self) -> Icon {
        self.icon
    }

    fn sensor_var(&self) -> SensorMenuVar {
        self.var
    }
}

impl<'ld> ListDrawableEntry<'ld> for PresetListEntry {
    fn label(&'ld self) -> &'ld str {
        self.desc
    }

    fn icon(&self) -> Icon {
        Icon::Opt
    }

    fn sensor_var(&self) -> SensorMenuVar {
        SensorMenuVar::None
    }
}

impl<'ld> ListDrawableEntry<'ld> for PartTableSubmenuEntry {
    fn label(&'ld self) -> &'ld str {
        self.desc.as_str()
    }

    fn icon(&self) -> Icon {
        if self.selected {
            Icon::File
        } else {
            Icon::Opt
        }
    }

    fn sensor_var(&self) -> SensorMenuVar {
        SensorMenuVar::None
    }
}
