/*
 * Menu: structure
 *
 * Stores the (immutable) menu structure
 */

#![allow(dead_code)]

use super::bitmaps::{Icon, Filler};
use super::sensor_submenu::{SensorMenuConfig, SensorMenuVar, SensorOption};

#[derive(Copy,Clone)]
pub enum MenuLink<'ml> {
    Some(&'ml Menu<'ml>),
    None,
    Pop
}

pub struct MenuOption<'mo> {
    pub label: &'mo str,
    pub icon: Icon,
    pub link: MenuLink<'mo>
}

pub enum Menu<'m> {
    Datalog {
        icon: Icon
    },
    List {
        name: &'m str,
        icon: Icon,
        options: &'m [MenuOption<'m>]
    },
    Info {
        name: &'m str,
        icon: Icon,
        text: &'m str,
        filler: Option<Filler>
    },
    SysInfo {
        name: &'m str,
        icon: Icon
    },
    FileList {
        name: &'m str,
        icon: Icon,
        dir_selection: bool
    },
    PresetList {
        name: &'m str,
        icon: Icon
    },
    PartitionList {
        name: &'m str,
        icon: Icon
    },
    SensorList {
        name: &'m str,
        icon: Icon,
        config: SensorMenuConfig,
        options: &'m [SensorOption<'m>]
    }
}

const ABOUT_MENU: Menu = Menu::Info {
    name: "About",
    icon: Icon::About,
    filler: Some(Filler::About),
    text: "Ver. 0.1\nros0061"
};

const SYSINFO_MENU: Menu = Menu::SysInfo {
    name: "System info",
    icon: Icon::Info
};

const FILELIST_MENU: Menu = Menu::FileList {
    name: "File list",
    icon: Icon::Files,
    dir_selection: false
};

pub(super) const PRESETLIST_MENU: Menu = Menu::PresetList {
    name: "Presets",
    icon: Icon::List
};

const DHT11_MENU: Menu = Menu::SensorList {
    name: "Dht 11",
    icon: Icon::Sensor,
    config: SensorMenuConfig::Dht11,
    options: &[
        SensorOption {icon: Icon::SensorVar, name: "Temperature", var: SensorMenuVar::Temperature},
        SensorOption {icon: Icon::SensorVar, name: "Humidity", var: SensorMenuVar::Humidity},
        SensorOption {icon: Icon::Period, name: "Period", var: SensorMenuVar::Period},
        SensorOption {icon: Icon::Back, name: "Back to menu", var: SensorMenuVar::None}
    ]
};

const MCP9808_MENU: Menu = Menu::SensorList {
    name: "Mcp 9808",
    icon: Icon::Sensor,
    config: SensorMenuConfig::Mcp9808,
    options: &[
        SensorOption {icon: Icon::SensorVar, name: "Temperature", var: SensorMenuVar::Temperature},
        SensorOption {icon: Icon::Period, name: "Period", var: SensorMenuVar::Period},
        SensorOption {icon: Icon::Back, name: "Back to menu", var: SensorMenuVar::None}
    ]
};

const ADXL345_MENU: Menu = Menu::SensorList {
    name: "Adxl 345",
    icon: Icon::Sensor,
    config: SensorMenuConfig::Adxl345,
    options: &[
        SensorOption {icon: Icon::SensorVar, name: "Accel.", var: SensorMenuVar::Acceleration},
        SensorOption {icon: Icon::Period, name: "Data Rate", var: SensorMenuVar::DataRate},
        SensorOption {icon: Icon::Opt, name: "Range", var: SensorMenuVar::Range},
        SensorOption {icon: Icon::Opt, name: "Full res.", var: SensorMenuVar::FullResolution},
        SensorOption {icon: Icon::Back, name: "Back to menu", var: SensorMenuVar::None}
    ]
};

const CO2DUMMY_MENU: Menu = Menu::SensorList {
    name: "CO2 Dummy",
    icon: Icon::Sensor,
    config: SensorMenuConfig::Co2Dummy,
    options: &[
        SensorOption {icon: Icon::SensorVar, name: "CO2", var: SensorMenuVar::CO2},
        SensorOption {icon: Icon::SensorVar, name: "Temperature", var: SensorMenuVar::Temperature},
        SensorOption {icon: Icon::SensorVar, name: "Humidity", var: SensorMenuVar::Humidity},
        SensorOption {icon: Icon::Period, name: "Period", var: SensorMenuVar::Period},
        SensorOption {icon: Icon::Back, name: "Back to menu", var: SensorMenuVar::None}
    ]
};

const SENSOR_MENU: Menu = Menu::List {
    name: "Sensors",
    icon: Icon::Sensors,
    options: &[
        MenuOption {label: "Dht11", icon: Icon::Sensor, link: MenuLink::Some(&DHT11_MENU)},
        MenuOption {label: "Mcp9808", icon: Icon::Sensor, link: MenuLink::Some(&MCP9808_MENU)},
        MenuOption {label: "Adxl345", icon: Icon::Sensor, link: MenuLink::Some(&ADXL345_MENU)},
        MenuOption {label: "CO2 Dummy", icon: Icon::Sensor, link: MenuLink::Some(&CO2DUMMY_MENU)},
        MenuOption {label: "Back to menu", icon: Icon::Back, link: MenuLink::Pop}
    ]
};

const STORAGE_MENU: Menu = Menu::List {
    name: "Storage",
    icon: Icon::Storage,
    options: &[
        MenuOption {label: "Output directory", icon: Icon::Files, link: MenuLink::Some(&DIRSELECT_MENU)},
        MenuOption {label: "Partition", icon: Icon::Partition, link: MenuLink::Some(&PARTITION_MENU)},
        MenuOption {label: "Back to menu", icon: Icon::Back, link: MenuLink::Pop}
    ]
};

const DATALOG_MENU: Menu = Menu::Datalog {
    icon: Icon::File
};

const PARTITION_MENU: Menu = Menu::PartitionList {
    icon: Icon::Partition,
    name: "Partitions"
};

const DIRSELECT_MENU: Menu = Menu::FileList {
    name: "Output directory",
    icon: Icon::Files,
    dir_selection: true
};

pub const MAIN_MENU: Menu = Menu::List {
    name: "Menu",
    icon: Icon::List,
    options: &[
        MenuOption {label: "New datalog", icon: Icon::New, link: MenuLink::Some(&DATALOG_MENU)},
        MenuOption {label: "List files", icon: Icon::Files, link: MenuLink::Some(&FILELIST_MENU)},
        MenuOption {label: "Sensor setup", icon: Icon::Sensors, link: MenuLink::Some(&SENSOR_MENU)},
        MenuOption {label: "Storage setup", icon: Icon::Storage, link: MenuLink::Some(&STORAGE_MENU)},
        MenuOption {label: "System info", icon: Icon::Info, link: MenuLink::Some(&SYSINFO_MENU)},
        MenuOption {label: "About", icon: Icon::About, link: MenuLink::Some(&ABOUT_MENU)}
    ]
};
