use crate::menu::{Menu, MenuLink, MenuState, ButtonEvent, SensorMenuVar, structure::PRESETLIST_MENU, sensor_submenu::SensorOption};
use crate::sensor::{SensorConfig, SensorEntryState, get_preset_list, DatalogState, PresetListEntryValue};
use crate::storage::{FrameDirection, FileListEntry, FileListEntryType, StorageEvent, StorageConfig};
use crate::app::{storage, persist_config};
use super::{SensorMenuConfig, PartTableSubmenuEntry};
use crate::panic::{dl_panic, dl_check};

fn process_file_list(menu_state: &mut MenuState, storage_config: &mut StorageConfig, button_event: ButtonEvent) {
    if let Some(ref mut file_list) = menu_state.file_list {
        let offset = file_list.offset as i32;
        let option_idx = menu_state.option_idx as i32;
        let total_len = file_list.frame.len() as i32 + offset + file_list.frame_idx_offset() as i32;

        match button_event {
            // decide whenether to move FileList frame and in which direction
            ButtonEvent::Up => {
                if option_idx - (offset + file_list.frame_idx_offset() as i32) <= 3 {
                    file_list.move_frame(FrameDirection::Up);
                }
            },
            ButtonEvent::Down => {
                if total_len - option_idx <= 3 {
                    file_list.move_frame(FrameDirection::Down);
                }
            }
            ButtonEvent::Select => {
                // back to menu option
                if option_idx == 0 {
                    menu_state.pop_back();
                    return;
                }

                // (optional) output directory selection
                if option_idx == 1 && file_list.dir_selection {
                    // change output directory path, reload storage config
                    let rt = storage_config.set_out_dir(file_list.current_dir.as_str());
                    dl_check!(rt, "Failed to change output dir");
                    storage::spawn(StorageEvent::ReloadConfig).unwrap();
                    // persist config
                    persist_config::spawn().unwrap();

                    menu_state.pop_back();
                    return;
                }

                // directory selection
                let idx = (option_idx - file_list.frame_idx_offset() as i32 - offset) as usize;
                if file_list.frame[idx].ftype == FileListEntryType::Folder {
                    let rt = file_list.change_dir(idx);
                    dl_check!(rt, "Failed to change directory");
                    menu_state.option_idx = 0;
                }
            }
        }
    }
}

fn process_sensor_list(menu_state: &mut MenuState, config: &SensorMenuConfig, options: &[SensorOption], sensor_config: &mut SensorConfig, button_event: ButtonEvent) {
    if button_event == ButtonEvent::Select {
        if SensorMenuVar::None == options[menu_state.option_idx].var {
            // Persist config
            persist_config::spawn().unwrap();

            menu_state.pop_back();
            return;
        }

        // Update state of the selected sensor config entry
        let sensor_entry_state = sensor_config.entry_state(*config, options[menu_state.option_idx].var);

        if sensor_entry_state.is_presetable() {
            menu_state.preset_list = Some(get_preset_list(&sensor_entry_state, config));
            menu_state.select_option(&PRESETLIST_MENU);
        }

        if let SensorEntryState::Boolean(var) = sensor_entry_state {
            sensor_config.update_entry_state(
                *config,
                options[menu_state.option_idx].var,
                SensorEntryState::Boolean(!var)
            );
        }
    }
}

fn process_part_list(menu_state: &mut MenuState, storage_config: &mut StorageConfig, button_event: ButtonEvent) {
    if button_event == ButtonEvent::Select {
        let part_entry = menu_state.part_table.entry(menu_state.option_idx);
        let (part_no, part_valid) = (part_entry.part_no, part_entry.valid);

        if part_valid {
            // Set partition index (config, menu_state)
            storage_config.partition_idx = part_no as i8;
            menu_state.set_part(part_no);
            // Persist config
            persist_config::spawn().unwrap();

            menu_state.pop_back();
        }
    }
}

fn process_preset_list(menu_state: &mut MenuState, sensor_config: &mut SensorConfig, button_event: ButtonEvent) {
    if button_event == ButtonEvent::Select {
        // Set period or data rate on selection according to the config in the parent menu
        if let Some(preset_list) = menu_state.preset_list { // preset list must exist
            let preset_list_value = &preset_list[menu_state.option_idx].value;
            menu_state.pop_back();

            if let Menu::SensorList {name: _, icon: _, config, options: _} = menu_state.current_menu { // parent menu = sensor list
                match preset_list_value {
                    PresetListEntryValue::Period(period) => {
                        sensor_config.update_entry_state(
                            *config,
                            SensorMenuVar::Period,
                            SensorEntryState::Period(*period as u64)
                        );
                    },
                    PresetListEntryValue::DataRate(data_rate) => {
                        sensor_config.update_entry_state(
                            *config,
                            SensorMenuVar::DataRate,
                            SensorEntryState::DataRate(*data_rate)
                        );
                    },
                    PresetListEntryValue::Range(range) => {
                        sensor_config.update_entry_state(
                            *config,
                            SensorMenuVar::Range,
                            SensorEntryState::Range(*range)
                        );
                    }
                }
            }
        } else {
            menu_state.pop_back();
        }
    }
}

fn process_datalog(menu_state: &mut MenuState, datalog_state: &mut DatalogState, sensor_config: &SensorConfig, button_event: ButtonEvent) {
    if button_event == ButtonEvent::Select {
        if datalog_state.inprog {
            // pause
            datalog_state.stop();
            menu_state.option_idx = 0;
        } else if menu_state.option_idx % 2 == 0 {
            // resume
            datalog_state.resume(sensor_config);
        } else {
            // end
            menu_state.pop_back();
            let rt = datalog_state.end_capture();
            dl_check!(rt, "Failed to end capture");

            // invalidate storage stats & file list
            menu_state.storage_stats = None;
            menu_state.file_list = None;
        }
    }
}

fn list_on_select(selected_menu: &Menu, menu_state: &mut MenuState, datalog_state: &mut DatalogState, sensor_config: &SensorConfig) {
    match selected_menu {
        // start datalog capture on new datalog selection
        Menu::Datalog {icon: _} => {
            let rt = datalog_state.start_capture(sensor_config);
            dl_check!(rt, "Failed to start capture");
        },
        // update stats on SysInfo selection
        Menu::SysInfo {name: _, icon: _} => {
            if menu_state.storage_stats.is_none() {
                storage::spawn(StorageEvent::UpdateStorageStats).unwrap();
            }
        },
        // update file list on FileList selection
        Menu::FileList {name: _, icon: _, dir_selection} => {
            if let Some(file_list) = &mut menu_state.file_list {
                file_list.dir_selection = *dir_selection;
            } else {
                storage::spawn(StorageEvent::UpdateFileList(*dir_selection)).unwrap();
            }
        }
        _ => {}
    }
}

pub(super) fn process_menu(menu_state: &mut MenuState, datalog_state: &mut DatalogState, sensor_config: &mut SensorConfig, storage_config: &mut StorageConfig, button_event: ButtonEvent) {
    match button_event {
        ButtonEvent::Up => {
            menu_state.option_idx -= if menu_state.option_idx > 0 { 1 } else { 0 };

            if let Menu::FileList {name: _, icon: _, dir_selection: _} = menu_state.current_menu {
                process_file_list(menu_state, storage_config, button_event);
            }
        },
        ButtonEvent::Down => {
            if menu_state.len() > 0 {
                menu_state.option_idx += if menu_state.option_idx < menu_state.len() - 1 { 1 } else { 0 }
            }

            if let Menu::FileList {name: _, icon: _, dir_selection: _} = menu_state.current_menu {
                process_file_list(menu_state, storage_config, button_event);
            }
        },
        ButtonEvent::Select => {
            match menu_state.current_menu {
                Menu::List {name: _, icon: _, options} => {
                    match options[menu_state.option_idx].link {
                        MenuLink::Some(menu) => {
                            // select highlighted option from the list
                            menu_state.select_option(menu);

                            // handle events triggered by submenu selection
                            list_on_select(menu, menu_state, datalog_state, sensor_config);
                            return;
                        },
                        MenuLink::Pop => {
                            // revert back to a parent menu
                            menu_state.pop_back();
                        },
                        MenuLink::None => {}
                    }
                },
                // submenu specific logic
                Menu::SensorList {name: _, icon: _, config, options} => {
                    process_sensor_list(menu_state, config, options, sensor_config, button_event);
                },
                Menu::FileList {name: _, icon: _, dir_selection: _} => {
                    process_file_list(menu_state, storage_config, button_event);
                },
                Menu::PresetList {name: _, icon: _} => {
                    process_preset_list(menu_state, sensor_config, button_event);
                },
                Menu::Datalog {icon: _} => {
                    process_datalog(menu_state, datalog_state, sensor_config, button_event);
                },
                Menu::PartitionList { name: _, icon: _ } => {
                    process_part_list(menu_state, storage_config, button_event);
                },
                _ => { menu_state.pop_back(); }
            }
        }
    }
}
