/*
 * Menu: part table submenu
 * A list of menu entries generated from GenericMbrTable
 */

use heapless::String;
use core::fmt::Write;

use super::num_fmt::format_stat;
use crate::storage::{GenericMbrTable, PartitionType, StorageConfig};

pub struct PartTableSubmenu {
    entries: [PartTableSubmenuEntry; 4]
}

pub struct PartTableSubmenuEntry {
    // Partition number (0..=3)
    pub part_no: usize,
    // Generated partition description
    pub desc: String<20>,
    // Is entry usable/selectable as a storage partition?
    pub valid: bool,
    // Is entry currently selected as an output partition
    pub selected: bool
}

impl PartTableSubmenuEntry {
    pub fn blank() -> Self {
        Self { part_no: 0, desc: String::new(), valid: false, selected: false }
    }
}

impl PartTableSubmenu {
    pub fn new(mbr_table: &GenericMbrTable, part_entry_idx: usize) -> Self {
        let mut entries: [PartTableSubmenuEntry; 4] =
            [PartTableSubmenuEntry::blank(),
             PartTableSubmenuEntry::blank(),
             PartTableSubmenuEntry::blank(),
             PartTableSubmenuEntry::blank()];

        for (i, mbr_entry) in mbr_table.into_iter().enumerate() {
            entries[i].part_no = i;

            if i == part_entry_idx {
                entries[i].selected = true;
            }

            match mbr_entry.part_type {
                PartitionType::Fat32 | PartitionType::Fat16 => {
                    write!(&mut entries[i].desc, "Part #{} {}", i, format_stat(mbr_entry.no_sectors as u64 * 512)).ok();
                    entries[i].valid = true;
                },
                PartitionType::None => {
                    write!(&mut entries[i].desc, "Part #{} N/A", i).ok();
                },
                PartitionType::Unsupported => {
                    write!(&mut entries[i].desc, "Part #{} Unsupported", i).ok();
                }
            }
        }

        PartTableSubmenu { entries }
    }

    pub fn mark_selected(&mut self, part_entry_idx: usize) {
        for entry in &mut self.entries {
            entry.selected = part_entry_idx == entry.part_no;
        }
    }

    pub fn len(&self) -> usize { self.entries.len() }

    pub fn entry(&self, idx: usize) -> &PartTableSubmenuEntry { &self.entries[idx] }
}
