/*
 * Menu: bitmaps
 *
 * Menu bitmaps & matching enums,
 */

#![allow(dead_code)]

#[derive(Copy,Clone,Debug)]
pub enum Icon {
    List,
    New,
    File,
    Files,
    Storage,
    Partition,
    Sensor,
    Sensors,
    SensorVar,
    Period,
    Info,
    About,
    Opt,
    Back,
    Select,
    Err
}

pub(super) static ICONS: &'static [&[u8]] = &[
    include_bytes!("../../assets/list.bmp"),
    include_bytes!("../../assets/new.bmp"),
    include_bytes!("../../assets/file.bmp"),
    include_bytes!("../../assets/files.bmp"),
    include_bytes!("../../assets/storage.bmp"),
    include_bytes!("../../assets/partition.bmp"),
    include_bytes!("../../assets/sensor.bmp"),
    include_bytes!("../../assets/sensors.bmp"),
    include_bytes!("../../assets/sensorvar.bmp"),
    include_bytes!("../../assets/period.bmp"),
    include_bytes!("../../assets/info.bmp"),
    include_bytes!("../../assets/about.bmp"),
    include_bytes!("../../assets/opt.bmp"),
    include_bytes!("../../assets/back.bmp"),
    include_bytes!("../../assets/select.bmp"),
    include_bytes!("../../assets/err.bmp")
];

#[derive(Copy,Clone)]
pub enum Filler {
    About
}

pub(super) static FILLER: &'static [&[u8]] = &[
    include_bytes!("../../assets/about_filler.bmp")
];
