/*
 * Menu: sensor submenu config enumeration, options
 */

use crate::menu::bitmaps::Icon;
use crate::sensor::SensorConfig;
use heapless::{Vec, String};

#[derive(Debug, Copy, Clone)]
pub enum SensorMenuConfig {
    Dht11,
    Mcp9808,
    Adxl345,
    Co2Dummy
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SensorMenuVar {
    Temperature,
    Humidity,
    CO2,
    Acceleration,
    Period,
    DataRate,
    Range,
    DMA,
    FullResolution,
    None
}

#[derive(Debug)]
pub struct SensorOption<'so> {
    pub icon: Icon,
    pub name: &'so str,
    pub var: SensorMenuVar
}
