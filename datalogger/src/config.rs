/*
 * Configuration options
 */

use serde::{Serialize, Deserialize};
use postcard::{from_bytes, to_vec};
use heapless::Vec;

use crate::sensor::SensorConfig;
use crate::storage::StorageConfig;

pub const PERSIST_SIZE: usize = 200;

#[derive(Debug)]
pub enum DatalogConfigError {
    UnableToSerialize(postcard::Error),
    UnableToDeserialize(postcard::Error)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DatalogConfig {
    pub sensor_config: SensorConfig,
    pub storage_config: StorageConfig
}

impl DatalogConfig {
    pub fn new() -> Self {
        Self {
            sensor_config: SensorConfig::new(),
            storage_config: StorageConfig::new()
        }
    }

    pub fn compose(sensor_config: &SensorConfig,
                   storage_config: &StorageConfig) -> Self {
        Self {
            sensor_config: *sensor_config,
            storage_config: *storage_config
        }
    }

    pub fn as_vec(&self) -> Result<Vec<u8, PERSIST_SIZE>, DatalogConfigError> {
        match to_vec(self) {
            Ok(vec) => Ok(vec),
            Err(err) => Err(DatalogConfigError::UnableToSerialize(err))
        }
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Self, DatalogConfigError> {
        match from_bytes(bytes) {
            Ok(vec) => Ok(vec),
            Err(err) => Err(DatalogConfigError::UnableToDeserialize(err))
        }
    }
}
