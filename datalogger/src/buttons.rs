/*
 * Buttons: task
 *
 * Handling & setup of button interrupts
 */

use rtic::mutex_prelude::*;
use systick_monotonic::*;
use fugit::Duration;
use crate::app::{button_up, button_down, button_select, button_timeout, menu};
use crate::menu::MenuEvent;
use stm32f4xx_hal::{gpio::{ExtiPin, gpioc::{PC1, PC2, PC3}, Input, Edge}, pac::{EXTI, SYSCFG}, prelude::*};

pub struct Buttons {
    up: PC3<Input>,
    down: PC2<Input>,
    select : PC1<Input>,
    exti: EXTI
}

const BUTTON_TIMEOUT: u64 = 100; // ms

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ButtonEvent {
    Up,
    Down,
    Select
}

impl From<ButtonEvent> for MenuEvent {
    fn from(button_event: ButtonEvent) -> Self {
        MenuEvent::ButtonPress(button_event)
    }
}

impl Buttons {
    pub fn new(mut button_up : PC3<Input>,
               mut button_down : PC2<Input>,
               mut button_select : PC1<Input>,
               mut exti_dev : EXTI,
               syscfg_dev : SYSCFG) -> Self {

        // button config (reset, choose, select)
        // 11 PC3     up button -> EXTI3
        // 12 PC2   down button -> EXTI2
        // 13 PC1 select button -> EXTI1

        let syscfg = &mut syscfg_dev.constrain();
        let exti = &mut exti_dev;

        button_up.make_interrupt_source(syscfg);
        button_up.enable_interrupt(exti);
        button_up.trigger_on_edge(exti, Edge::Falling);
        button_down.make_interrupt_source(syscfg);
        button_down.enable_interrupt(exti);
        button_down.trigger_on_edge(exti, Edge::Falling);
        button_select.make_interrupt_source(syscfg);
        button_select.enable_interrupt(exti);
        button_select.trigger_on_edge(exti, Edge::Falling);

        Buttons { up: button_up, down: button_down, select: button_select, exti: exti_dev }
    }
}

/* interrupt handlers */
pub(crate) fn button_up(mut cx: button_up::Context) {
    uprintln!("button_up");
    cx.shared.buttons.lock(|buttons| {
        menu::spawn(ButtonEvent::Up.into()).ok();
        buttons.up.clear_interrupt_pending_bit();
        buttons.up.disable_interrupt(&mut buttons.exti);
        button_timeout::spawn_after(BUTTON_TIMEOUT.millis(), ButtonEvent::Up).ok();
    });
}

pub(crate) fn button_down(mut cx: button_down::Context) {
    uprintln!("button_down");
    cx.shared.buttons.lock(|buttons| {
        menu::spawn(ButtonEvent::Down.into()).ok();
        buttons.down.clear_interrupt_pending_bit();
        buttons.down.disable_interrupt(&mut buttons.exti);
        button_timeout::spawn_after(BUTTON_TIMEOUT.millis(), ButtonEvent::Down).ok();
    });
}

pub(crate) fn button_select(mut cx: button_select::Context) {
    uprintln!("button_select");
    cx.shared.buttons.lock(|buttons| {
        menu::spawn(ButtonEvent::Select.into()).ok();
        buttons.select.clear_interrupt_pending_bit();
        buttons.select.disable_interrupt(&mut buttons.exti);
        button_timeout::spawn_after(BUTTON_TIMEOUT.millis(), ButtonEvent::Select).ok();
    });
}

pub(crate) fn button_timeout(mut cx: button_timeout::Context, button_event: ButtonEvent) {
    cx.shared.buttons.lock(|buttons| {
        match button_event {
            ButtonEvent::Up => { buttons.up.enable_interrupt(&mut buttons.exti); },
            ButtonEvent::Down => { buttons.down.enable_interrupt(&mut buttons.exti); },
            ButtonEvent::Select => { buttons.select.enable_interrupt(&mut buttons.exti); }
        }
    });
}
