/*
 * Storage: configuration options
 */

use heapless::String;
use serde::{Serialize, Deserialize, Serializer};
use serde_big_array::BigArray;
use core::str;

pub const OUT_DIR_PATH_LEN: usize = 100;

#[derive(Debug)]
pub enum StorageConfigError {
    OutputDirPathLengthExceeded
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct StorageConfig {
    // Partition selected in storage config submenu,
    // overriden with favorable partition if empty or unusable
    //
    // NOTE: Due to the way `fatfs` is implemened this config
    // necessitates a restart in order to take effect
    // (No known way to deinitialize/deconstruct `FileSystem` object)
    pub partition_idx: i8, // 0..=3
    // Directory used to store the logs
    // (C string + length)
    #[serde(with = "BigArray")]
    pub out_dir: [u8; OUT_DIR_PATH_LEN],
    pub out_dir_len: u8
}

impl StorageConfig {
    pub fn new() -> Self {
        Self {
            partition_idx: -1,
            out_dir: [0u8; OUT_DIR_PATH_LEN],
            out_dir_len: 0
        }
    }

    pub fn out_dir(&self) -> Option<String<OUT_DIR_PATH_LEN>> {
        if self.out_dir_len == 0 || self.out_dir_len as usize > OUT_DIR_PATH_LEN { return None; }

        let dir = str::from_utf8(&self.out_dir[0..self.out_dir_len as usize]);

        if let Ok(dir_str) = dir {
            Some(String::from(dir_str))
        } else {
            None
        }
    }

    pub fn set_out_dir(&mut self, out_dir: &str) -> Result<(), StorageConfigError> {
        if out_dir.len() > OUT_DIR_PATH_LEN {
            return Err(StorageConfigError::OutputDirPathLengthExceeded);
        }

        uprintln!("len: {}/{}, bytes: {:?}", out_dir.len(), OUT_DIR_PATH_LEN, out_dir.as_bytes());

        self.out_dir_len = out_dir.len() as u8;
        self.out_dir[0..out_dir.len()].clone_from_slice(out_dir.as_bytes());
        Ok(())
    }
}
