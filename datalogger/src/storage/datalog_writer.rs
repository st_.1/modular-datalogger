#![allow(dead_code)]

use rtic::Mutex;
use core::cell::RefCell;
use heapless::String;
use embedded_graphics::pixelcolor::raw::ToBytes;
use fatfs::{File, FileSystem, LossyOemCpConverter, NullTimeProvider, Error, FileSystemStats, Read, Write as WriteOther};
use crate::sensor::MeasurementResult;

use super::{FsCache, SdPartition, sdpartition};

#[derive(Debug)]
pub enum EntryValueType {
    U8,
    I8,
    U16,
    I16,
    U32,
    I32,
    F32,
    F64
}

#[derive(Debug)]
pub enum EntryValue {
    U8(u8),
    I8(i8),
    U16(u16),
    I16(i16),
    U32(u32),
    I32(i32),
    F32(f32),
    F64(f64)
}

#[derive(Debug)]
pub enum DatalogWriterError {
    IOError(Error<sdpartition::Error>)
}

impl From<Error<sdpartition::Error>> for DatalogWriterError {
    fn from(err: Error<sdpartition::Error>) -> Self { DatalogWriterError::IOError(err) }
}

pub struct DatalogWriter<'w> {
    file: File<'w, FsCache<SdPartition>, NullTimeProvider, LossyOemCpConverter>
}

impl<'w> DatalogWriter<'w> {
    pub fn new(file: File<'w, FsCache<SdPartition>, NullTimeProvider, LossyOemCpConverter>) -> Self {
        Self { file }
    }

    pub fn begin_new_log(&mut self, sensor_count: u8) -> Result<(), DatalogWriterError> {
        uprintln!("[Storage] begin_new_log ({})", sensor_count);

        self.file.write_all(&[0])?; // format version: 0
        self.file.write_all(&[sensor_count])?;
        Ok(())
    }

    pub fn begin_header(&mut self, sensor_name: &str, sensor_id: u16, period_us: u64, value_count: u8) -> Result<(), DatalogWriterError> {
        uprintln!("[Storage] begin_header ({}, {}, {:?})", sensor_name, sensor_id, period_us);

        self.file.write_all(sensor_name.as_bytes())?;
        self.file.write_all(b"\0")?;
        self.file.write_all(&sensor_id.to_le_bytes())?;
        self.file.write_all(&period_us.to_le_bytes())?;
        self.file.write_all(&[value_count])?;
        Ok(())
    }

    pub fn add_var(&mut self, name: &str, unit: &str, data_type: EntryValueType) -> Result<(), DatalogWriterError> {
        uprintln!("[Storage] add_var ({}, {}, {:?})", name, unit, data_type);

        self.file.write_all(name.as_bytes())?;
        self.file.write_all(b"\0")?;
        self.file.write_all(unit.as_bytes())?;
        self.file.write_all(b"\0")?;
        self.file.write_all(&[data_type as u8])?;
        Ok(())
    }

    pub fn flush(&mut self) -> Result<(), DatalogWriterError> {
        self.file.flush()?;
        Ok(())
    }

    pub fn begin_entry(&mut self, sensor_id: u16, timestamp: u64, result: MeasurementResult) -> Result<(), DatalogWriterError> {
        self.file.write_all(&sensor_id.to_le_bytes())?;
        self.file.write_all(&timestamp.to_le_bytes())?;
        self.file.write_all(&[result as u8])?;
        Ok(())
    }

    pub fn log_var(&mut self, value: EntryValue) -> Result<(), DatalogWriterError> {
        match value {
            EntryValue::U8(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            },
            EntryValue::I8(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            },
            EntryValue::U16(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            },
            EntryValue::I16(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            },
            EntryValue::U32(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            },
            EntryValue::I32(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            },
            EntryValue::F32(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            },
            EntryValue::F64(val) => {
                self.file.write_all(&val.to_le_bytes())?;
            }
        }

        Ok(())
    }
}
