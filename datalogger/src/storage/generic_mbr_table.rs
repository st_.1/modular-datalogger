/*
 * Storage: Generic MBR table
 * Enumerates partitions present in the MBR table on the SD card
 */

use stm32f4xx_hal::sdio::{Sdio, SdioPeripheral};
use super::StorageError;

#[derive(Copy, Clone)]
pub struct GenericMbrTable {
    pub entries: [PartitionEntry; 4]
}

#[derive(Copy, Clone, PartialEq)]
pub struct PartitionEntry {
    // 0x04 Partition type
    pub part_type: PartitionType,
    // 0x08 LBA of the first sector
    pub block_addr: u32,
    // 0x0C Number of sectors
    pub no_sectors: u32
}

#[derive(Copy, Clone, PartialEq)]
pub enum PartitionType {
    None,
    Fat32,
    Fat16,
    Unsupported
}

impl From<u8> for PartitionType {
    fn from(part_type_byte: u8) -> Self {
        match part_type_byte {
            0x0 => PartitionType::None,
            0x4 | 0x6 => PartitionType::Fat16,
            0xB | 0xC | 0xE | 0xF => PartitionType::Fat32,
            _ => PartitionType::Unsupported
        }
    }
}

impl PartitionEntry {
    fn blank() -> Self {
        Self {
            part_type: PartitionType::None,
            block_addr: 0,
            no_sectors: 0
        }
    }
}

impl PartitionEntry {
    pub fn usable(&self) -> bool {
        (self.part_type == PartitionType::Fat16 || self.part_type == PartitionType::Fat32) &&
        self.block_addr > 0 &&
        self.no_sectors > 0
    }
}

impl GenericMbrTable {
    pub fn new<P: SdioPeripheral>(sdio: &mut Sdio<P>) -> Result<Self, StorageError>{
        let mut mbr_block: [u8; 512] = [0; 512];
        match sdio.read_block(0, &mut mbr_block) {
            Ok(_) => uprintln!("Got data!"),
            Err(e) => {
                uprintln!("Failed to read block: {:?}", e);
                return Err(StorageError::SdioError(e));
            },
        };

        // check MBR signature
        uprintln!("MBR signature: {:02X?}", &mbr_block[510..]);
        if mbr_block[510..512] != [0x55, 0xAA] {
            return Err(StorageError::InvalidMbrSignature);
        }

        // parse primary partitions in the MBR
        let mut entries = [PartitionEntry::blank(); 4];
        let mut part_type_byte;
        let mut block_addr_bytes = [0u8; 4];
        let mut no_sectors_bytes = [0u8; 4];

        for (i, entry_addr) in (0x01BE..=0x01EE).step_by(16).enumerate() {
            part_type_byte = mbr_block[entry_addr + 0x04];
            block_addr_bytes.copy_from_slice(&mbr_block[entry_addr + 0x08..entry_addr + 0x0C]);
            no_sectors_bytes.copy_from_slice(&mbr_block[entry_addr + 0x0C..entry_addr + 0x10]);

            entries[i] = PartitionEntry {
                part_type: PartitionType::from(part_type_byte),
                block_addr: u32::from_le_bytes(block_addr_bytes),
                no_sectors: u32::from_le_bytes(no_sectors_bytes)
            };
        }

        Ok(Self { entries })
    }

    pub fn find_favorable_partition(&self) -> Result<usize, StorageError>{
        let mut fav_entry: Option<PartitionEntry> = None;
        let mut fav_entry_idx = 0;

        // find the largest Fat16 or Fat32 partition entry
        let mut idx = 0;
        for entry in self.entries {
            if !entry.usable() {
                idx += 1;
                continue;
            }

            if let Some(current_entry) = fav_entry {
                if current_entry.no_sectors < entry.no_sectors {
                    fav_entry = Some(entry);
                    fav_entry_idx = idx;
                }
            } else {
                fav_entry = Some(entry);
                fav_entry_idx = idx;
            }

            idx += 1;
        }

        match fav_entry {
            Some(_) => Ok(fav_entry_idx),
            None => Err(StorageError::NoSupportedPartition)
        }
    }

    pub fn entry(&self, idx: usize) -> &PartitionEntry { &self.entries[idx] }
}

// GenericMbrTable iterator

pub struct GenericMbrTableIterator<'gmt> {
    idx: usize,
    mbr_table: &'gmt GenericMbrTable
}

impl<'gmt> Iterator for GenericMbrTableIterator<'gmt> {
    type Item = PartitionEntry;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx < self.mbr_table.entries.len() {
            let item = self.mbr_table.entries[self.idx];
            self.idx += 1;
            Some(item)
        } else {
            None
        }
    }
}

impl<'gmt> IntoIterator for &'gmt GenericMbrTable {
    type Item = PartitionEntry;
    type IntoIter = GenericMbrTableIterator<'gmt>;

    fn into_iter(self) -> Self::IntoIter {
        GenericMbrTableIterator { idx: 0, mbr_table: &self }
    }
}
