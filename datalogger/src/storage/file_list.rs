#![allow(dead_code)]

use heapless::{Vec, String};
use core::cmp::{min, max};
use core::fmt::{Write, write};

use crate::storage::StorageEvent;
use crate::menu::menu;
use crate::buttons::ButtonEvent;
use crate::app::{menu, storage};

const FRAME_SIZE: usize = 20;
const FRAME_INC: usize = FRAME_SIZE / 2;
const CURRENT_DIR_PATH_SIZE: usize = 100;

#[derive(Debug)]
pub enum FileListError {
    ChangeDirPathTooLongError
}

#[derive(Debug)]
pub enum FrameDirection {
    Up, Down
}

#[derive(Debug, PartialEq)]
pub enum FileListEntryType {
    Folder,
    Log,
    File,
    Back,
    Select,
    None
}

#[derive(Debug)]
pub struct FileListEntry {
    pub ftype: FileListEntryType,
    pub name: String<16>
}

#[derive(Debug)]
pub struct FileList {
    pub frame: Vec<FileListEntry, FRAME_SIZE>,
    pub exit_entry: FileListEntry,
    pub sel_entry: FileListEntry,
    pub dir_selection: bool,
    pub current_dir: String<CURRENT_DIR_PATH_SIZE>,
    pub busy: bool,
    pub offset: usize,
    pub len: usize
}

impl FileListEntry {
    pub fn new() -> Self {
        Self {
            ftype: FileListEntryType::None,
            name: String::new()
        }
    }

    pub fn sel_entry() -> Self {
        Self {
            ftype: FileListEntryType::Select,
            name: String::from("Select directory")
        }
    }

    pub fn exit_entry() -> Self {
        Self {
            ftype: FileListEntryType::Back,
            name: String::from("Back to menu")
        }
    }
}

impl FileList {
    pub fn new(dir_selection: bool) -> Self {
        Self {
            frame: Vec::new(),
            exit_entry: FileListEntry::exit_entry(),
            sel_entry: FileListEntry::sel_entry(),
            dir_selection,
            current_dir: String::new(),
            busy: false,
            offset: 0,
            len: 0
        }
    }

    pub fn clear(&mut self) {
        self.frame.clear();
        self.len = 0;
    }

    pub fn move_frame(&mut self, direction: FrameDirection) {
        let new_offset = match direction {
            FrameDirection::Up => max(self.offset as i32 - FRAME_INC as i32, 0) as usize,
            FrameDirection::Down => max(min(self.offset as i32 + FRAME_INC as i32, self.len as i32 - FRAME_SIZE as i32), 0) as usize
        };

        // avoid unnecessary refills
        if self.offset == new_offset {
            return
        } else {
            self.offset = new_offset;
        }

        self.frame_update();
    }

    pub fn change_dir(&mut self, frame_idx: usize) -> Result<(), FileListError> {
        if self.current_dir.len() + self.frame[frame_idx].name.len() > 100 {
            return Err(FileListError::ChangeDirPathTooLongError);
        }

        if self.busy { return Ok(()); }

        let subdir = &self.frame[frame_idx].name;

        // change current directory path
        if subdir == ".." {
            if let Some(slash_pos) = self.current_dir.rfind("/") {
                self.current_dir.truncate(slash_pos);
            } else {
                self.current_dir.truncate(0);
            }
        } else {
            write!(&mut self.current_dir, "/{}", subdir).ok();
        }

        uprintln!("cd: \"{}\"", self.current_dir);

        self.frame_update();

        Ok(())
    }

    fn frame_update(&mut self) {
        self.busy = true;

        // Call storage task to update frame
        storage::spawn(StorageEvent::UpdateFileList(self.dir_selection)).unwrap();
    }

    pub fn frame_idx_offset(&self) -> usize {
        if self.dir_selection { 2 } else { 1 }
    }
}
