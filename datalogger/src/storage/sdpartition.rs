use core::{cell::RefCell, cmp::min, fmt};
use fatfs::{Read, Write, Seek, IoBase, SeekFrom, IoError};
use stm32f4xx_hal::{sdio, sdio::{Sdio, SdCard}};

#[derive(Debug)]
pub enum Error {
    OutOfBounds,
    ReadError,
    WriteError,
    Timeout,
    UnexpectedEof,
    WriteZero
}

impl IoError for Error {
    fn is_interrupted(&self) -> bool {
        match self {
            Error::Timeout => true,
            _ => false
        }
    }

    fn new_unexpected_eof_error() -> Self {
        return Error::UnexpectedEof;
    }

    fn new_write_zero_error() -> Self {
        return Error::WriteZero;
    }
}

pub struct SdPartition {
    sdio: RefCell<Sdio<SdCard>>,
    begin: u64,
    size: u64, // 8 TB < 16 EiB
    pos: u64
}

impl fmt::Debug for SdPartition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Partition")
         .field("begin", &self.begin)
         .field("size", &self.size)
         .field("pos", &self.pos)
         .finish()
    }
}

impl SdPartition {
    const BLOCK_SIZE : u64 = 512;

    pub fn new(sdio: Sdio<SdCard>, lba_begin: u32, lba_size: u32) -> SdPartition {
        SdPartition {
            sdio: RefCell::new(sdio),
            begin: lba_begin as u64 * Self::BLOCK_SIZE, // first byte of the partition
            size: lba_size as u64 * Self::BLOCK_SIZE,   // size in bytes OVERFLOW
            pos: 0                               // relative position
        }
    }
}

impl IoBase for SdPartition {
    type Error = Error;
}

// Read n bytes into the provided buffer, return number of bytes read
impl Read for SdPartition {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {

        // sdio.read_block reads data in 512B wide blocks,
        // in order to make this work we'll go through all the necessary blocks
        // and copy the relevant bytes

        let mut dst_start : u64 = 0;
        let mut src_start : u64 = self.pos as u64 % Self::BLOCK_SIZE;
        let mut dst_end : u64;
        let mut src_end : u64;

        let mut block_idx : u64 = 0;
        let mut bytes_left = buf.len() as u64;

        while bytes_left > 0 {
            let len = min(Self::BLOCK_SIZE - src_start, bytes_left);
            src_end = src_start + len;
            dst_end = dst_start + len;
            bytes_left -= len;

            // r/w ops
            {
                // read block
                let block_global_idx = ((self.begin + self.pos) / Self::BLOCK_SIZE + block_idx) as u32;
                let mut block : [u8; Self::BLOCK_SIZE as usize] = [0; Self::BLOCK_SIZE as usize];

                let read_result = self.sdio.borrow_mut().read_block(block_global_idx, &mut block);

                match read_result {
                    Ok(_) => {},
                    Err(sdio::Error::Timeout) => return Err(Error::Timeout),
                    Err(_) => return Err(Error::ReadError)
                }

                // clone relevant bytes
                buf[dst_start as usize..dst_end as usize].clone_from_slice(&block[src_start as usize..src_end as usize]);
            }

            src_start = 0;
            dst_start += len;
            block_idx += 1;
        }

        // seek
        self.pos += buf.len() as u64;

        /*if let Ok(s) = str::from_utf8(buf) {
            uprintln!("read: {} | {:?}", s, buf);
        } else {
            uprintln!("read: {:?}", buf);
        }*/

        Ok(buf.len())
    }
}

impl Write for SdPartition {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {

        /*if let Ok(s) = str::from_utf8(buf) {
            uprintln!("write: {} | {:?}", s, buf);
        } else {
            uprintln!("write: {:?}", buf);
        }*/

        let mut src_start : u64 = 0;
        let mut dst_start : u64 = self.pos % Self::BLOCK_SIZE;
        let mut src_end : u64;
        let mut dst_end : u64;

        let mut block_idx : u64 = 0;
        let mut bytes_left = buf.len() as u64;

        while bytes_left > 0 {
            let len = min(Self::BLOCK_SIZE - dst_start, bytes_left);
            dst_end = dst_start + len;
            src_end = src_start + len;
            bytes_left -= len;

            // r/w ops
            {
                // current block
                let block_global_idx = ((self.begin + self.pos) / Self::BLOCK_SIZE + block_idx) as u32;
                let mut block : [u8; Self::BLOCK_SIZE as usize] = [0; Self::BLOCK_SIZE as usize];

                // read block if the write is only partial
                if dst_start % Self::BLOCK_SIZE != 0 || dst_end % Self::BLOCK_SIZE != 0 {
                    let read_result = self.sdio.borrow_mut().read_block(block_global_idx, &mut block);

                    match read_result {
                        Ok(_) => {},
                        Err(sdio::Error::Timeout) => return Err(Error::Timeout),
                        Err(_) => return Err(Error::ReadError)
                    }
                }

                // clone relevant bytes to current block
                block[dst_start as usize..dst_end as usize].clone_from_slice(&buf[src_start as usize..src_end as usize]);

                // write block
                let write_result = self.sdio.borrow_mut().write_block(block_global_idx, &block);

                match write_result {
                    Ok(_) => {},
                    Err(sdio::Error::Timeout) => return Err(Error::Timeout),
                    Err(_) => return Err(Error::WriteError)
                }
            }

            dst_start = 0;
            src_start += len;
            block_idx += 1;
        }

        // seek
        self.pos += buf.len() as u64;

        Ok(buf.len())
    }

    fn flush(&mut self) -> Result<(), Error> {
        // caching is done in FsCache
        Ok(())
    }
}

impl Seek for SdPartition {
    fn seek(&mut self, seek_from: SeekFrom) -> Result<u64, Error> {

        let pos : u64;

        match seek_from {
            SeekFrom::Start(offset) => {
                pos = offset as u64;
            },
            SeekFrom::End(offset) => {
                pos =
                    if offset >= 0 {
                        self.size - offset as u64
                    } else {
                        self.size + (-offset as u64)
                    };
            },
            SeekFrom::Current(offset) => {
                pos =
                    if offset >= 0 {
                        self.pos + offset as u64
                    } else {
                        self.pos - (-offset as u64)
                    };
            }
        }

        // check bounds

        if pos > self.size {
            Err(Error::OutOfBounds)
        } else {
            self.pos = pos;
            Ok(self.pos as u64)
        }
    }
}
