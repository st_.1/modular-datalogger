#[derive(Debug)]
pub struct StorageStats {
    pub free_space: u64,
    pub total_space: u64,
    pub no_logs: u64
}

impl StorageStats {
    pub fn blank() -> Self {
        Self { free_space: 0, total_space: 0, no_logs: 0 }
    }
}
