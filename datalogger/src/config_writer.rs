use crate::app::persist_config;
use crate::config::{DatalogConfig, DatalogConfigError, PERSIST_SIZE};

use embedded_hal::{blocking::spi::Transfer, digital::v2::OutputPin};
use heapless::Vec;
use core::{fmt, fmt::Write, str};
use spi_memory::{Error, series25::Flash, prelude::*};
use rtic::mutex_prelude::*;

#[derive(Debug)]
pub enum ConfigWriterError {
    FlashInitError,
    UnableToPersistConfig,
    UnableToReadConfig,
    UnableToEraseSector,
    UnableToParseConfig(DatalogConfigError),
    UnableToSerializeConfig(DatalogConfigError)
}

pub struct ConfigWriter<SPI, CS>
where SPI: Transfer<u8>,
      CS: OutputPin
{
    ext_flash: Flash<SPI, CS>
}

impl<SPI, CS> ConfigWriter<SPI, CS>
where SPI: Transfer<u8>,
      CS: OutputPin
{
    pub fn new(spi: SPI, cs: CS) -> Result<Self, ConfigWriterError> {
        match Flash::init(spi, cs) {
            Ok(ext_flash) => Ok(Self { ext_flash }),
            Err(_) => Err(ConfigWriterError::FlashInitError)
        }
    }

    pub fn config(&mut self) -> Result<DatalogConfig, ConfigWriterError> {
        let mut bytes = [0u8; PERSIST_SIZE];

        // Get config file bytes
        match self.ext_flash.read(0, &mut bytes) {
            Ok(_) => {
                uprintln!("sensor config loaded");
            },
            Err(_) => {
                uprintln!("sensor config not loaded");
                return Err(ConfigWriterError::UnableToReadConfig);
            }
        }

        // Deserialize
        match DatalogConfig::from_bytes(&bytes) {
            Ok(config) => {
                uprintln!("sensor config parsed: {:?}", config);
                Ok(config)
            },
            Err(err) => {
                uprintln!("failed to parse sensor config: {:?}", err);
                Err(ConfigWriterError::UnableToParseConfig(err))
            }
        }
    }

    pub fn persist_config(&mut self, config: &DatalogConfig) -> Result<(), ConfigWriterError> {
        let mut bytes_vec: Vec<u8, PERSIST_SIZE>;

        // Serialize
        match config.as_vec() {
            Ok(vec) => { bytes_vec = vec; },
            Err(err) => {
                uprintln!("unable to serialize sensor config: {:?}", err);
                return Err(ConfigWriterError::UnableToSerializeConfig(err));
            }
        }

        // Erase 4K sector @ addr 0
        if self.ext_flash.erase_sectors(0, 1).is_err() {
            return Err(ConfigWriterError::UnableToEraseSector);
        }

        // Persist config file bytes
        if let Err(_) = self.ext_flash.write_bytes(0, &mut bytes_vec) {
            uprintln!("unable to persist datalog config");
            Err(ConfigWriterError::UnableToPersistConfig)
        } else {
            uprintln!("config persist: {:?}", config);
            Ok(())
        }
    }
}

pub fn persist_config(cx: persist_config::Context) {
    let sensor_config = cx.shared.sensor_config;
    let storage_config = cx.shared.storage_config;
    let config_writer = cx.local.config_writer;

    (sensor_config, storage_config).lock(|sensor_config, storage_config| {
        let config = DatalogConfig::compose(sensor_config, storage_config);
        config_writer.persist_config(&config).unwrap();
    });
}
