/*
 * Sensor: period presets
 *
 * Predefined list of period/data rate values allowed for each sensor (μs, Hz)
 */

use ::adxl345::{OutputDataRate, OutputDataRate::*, Range};
use PresetListEntryValue::*;
use crate::menu::SensorMenuConfig;
use crate::sensor::SensorEntryState;

pub type PresetList = &'static [PresetListEntry];

pub enum PresetListEntryValue {
    Period(u32),
    DataRate(OutputDataRate),
    Range(Range)
}

pub struct PresetListEntry {
    pub value: PresetListEntryValue,
    pub desc: &'static str
}

const DHT11_PRESETS: PresetList = &[
    PresetListEntry { value: Period(1_000_000), desc: "1 second"},
    PresetListEntry { value: Period(2_000_000), desc: "2 seconds"},
    PresetListEntry { value: Period(3_000_000), desc: "3 seconds"},
    PresetListEntry { value: Period(5_000_000), desc: "5 seconds"},
    PresetListEntry { value: Period(10_000_000), desc: "10 seconds"},
];

const MCP9808_PRESETS: PresetList = &[
    PresetListEntry { value: Period(250_000), desc: "250 ms"},
    PresetListEntry { value: Period(400_000), desc: "400 ms"},
    PresetListEntry { value: Period(500_000), desc: "500 ms"},
    PresetListEntry { value: Period(1_000_000), desc: "1 second"},
    PresetListEntry { value: Period(2_000_000), desc: "2 seconds"},
    PresetListEntry { value: Period(3_000_000), desc: "3 seconds"},
    PresetListEntry { value: Period(5_000_000), desc: "5 seconds"},
    PresetListEntry { value: Period(10_000_000), desc: "10 seconds"},
];

const CO2DUMMY_PRESETS: PresetList = &[
    PresetListEntry { value: Period(1_000_000), desc: "1 second"},
    PresetListEntry { value: Period(2_000_000), desc: "2 seconds"},
    PresetListEntry { value: Period(3_000_000), desc: "3 seconds"},
    PresetListEntry { value: Period(5_000_000), desc: "5 seconds"},
    PresetListEntry { value: Period(10_000_000), desc: "10 seconds"},
    PresetListEntry { value: Period(20_000_000), desc: "20 seconds"},
];

const ADXL345_DATA_RATE_PRESETS: PresetList = &[
    PresetListEntry { value: DataRate(ODR_3200_Hz), desc: "3200 Hz"},
    PresetListEntry { value: DataRate(ODR_1600_Hz), desc: "1600 Hz"},
    PresetListEntry { value: DataRate(ODR_800_Hz), desc: "800 Hz"},
    PresetListEntry { value: DataRate(ODR_400_Hz), desc: "400 Hz"},
    PresetListEntry { value: DataRate(ODR_200_Hz), desc: "200 Hz"},
    PresetListEntry { value: DataRate(ODR_100_Hz), desc: "100 Hz"},
    PresetListEntry { value: DataRate(ODR_50_Hz), desc: "50 Hz"},
    PresetListEntry { value: DataRate(ODR_25_Hz), desc: "25 Hz"},
    PresetListEntry { value: DataRate(ODR_12_5_Hz), desc: "12.5 Hz"},
    PresetListEntry { value: DataRate(ODR_6_25_Hz), desc: "6.25 Hz"},
    PresetListEntry { value: DataRate(ODR_3_13_Hz), desc: "3.13 Hz"},
    PresetListEntry { value: DataRate(ODR_1_56_Hz), desc: "1.56 Hz"},
    PresetListEntry { value: DataRate(ODR_0_78_Hz), desc: "0.78 Hz"},
    PresetListEntry { value: DataRate(ODR_0_39_Hz), desc: "0.39 Hz"},
    PresetListEntry { value: DataRate(ODR_0_20_Hz), desc: "0.2 Hz"},
    PresetListEntry { value: DataRate(ODR_0_10_Hz), desc: "0.1 Hz"},
];

const ADXL345_RANGE_PRESETS: PresetList = &[
    PresetListEntry { value: Range(Range::_2G), desc: "2 g"},
    PresetListEntry { value: Range(Range::_4G), desc: "4 g"},
    PresetListEntry { value: Range(Range::_8G), desc: "8 g"},
    PresetListEntry { value: Range(Range::_16G), desc: "16 g"},
];

pub fn get_preset_list(sensor_entry_state: &SensorEntryState, config: &SensorMenuConfig) -> PresetList {
    match config {
        SensorMenuConfig::Dht11 => DHT11_PRESETS,
        SensorMenuConfig::Mcp9808 => MCP9808_PRESETS,
        SensorMenuConfig::Adxl345 => {
            if let SensorEntryState::DataRate(_) = sensor_entry_state {
                ADXL345_DATA_RATE_PRESETS
            } else {
                ADXL345_RANGE_PRESETS
            }
        },
        SensorMenuConfig::Co2Dummy => CO2DUMMY_PRESETS
    }
}
