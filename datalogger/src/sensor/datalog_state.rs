use heapless::String;
use rtic_monotonic::Monotonic;
use systick_monotonic::Systick;
use core::mem::size_of;

use super::SENSOR_TASK_TABLE;
use super::config::SensorConfig;
use crate::storage::{Storage, StorageError};
use crate::storage::StorageEvent;
use crate::app::{storage, monotonics, sensor_dht11, menu, GRAN};
use crate::menu::{menu, MenuEvent};
use crate::panic::dl_check;

// size of an entry header (optionally followed by measured values)
const BEGIN_ENTRY_SIZE: u32 = (size_of::<u16>() + size_of::<u64>() + size_of::<u8>()) as u32;

pub type SystInstant = <Systick<GRAN> as Monotonic>::Instant;

#[derive(Debug)]
pub enum DatalogError {
    UnableToInitDatalog,
    UnableToEndDatalog
}

pub struct DatalogState {
    pub inprog: bool,
    pub filename: String<11>,
    pub no_sensors: u8,
    pub no_measured: u32,
    pub no_dropped: u32,
    pub size: u32,
    start_time: SystInstant
}

impl DatalogState {
    pub fn new(sensor_config: &SensorConfig) -> Self {
        Self {
            inprog: false,
            filename: String::new(),
            no_sensors: sensor_config.active_sensors(),
            no_measured: 0,
            no_dropped: 0,
            size: 0,
            start_time: monotonics::now()
        }
    }

    // start_capture()
    // * inprog <- true
    // * filename <- "Log1.dl" <- storage.init_datalog()
    // * no_sensors <- sensor_config.active_sensors()
    // * no_measured, size .. incremented on each "tick"
    // # storage.init_datalog()
    pub fn start_capture(&mut self, sensor_config: &SensorConfig) -> Result<(), DatalogError> {
        self.inprog = true;
        self.no_sensors = sensor_config.active_sensors();
        self.filename = String::from("...");

        // calls trigger_tasks on success
        match storage::spawn(StorageEvent::DatalogInit) {
            Err(_) => Err(DatalogError::UnableToInitDatalog),
            Ok(_) => Ok(())
        }
    }

    // end_capture()
    // * inprog <- false
    // * filename <- ""
    // * no_measured, size <- 0
    // # storage EndDatalog
    pub fn end_capture(&mut self) -> Result<(), DatalogError> {
        self.inprog = false;
        self.no_measured = 0;
        self.no_dropped = 0;
        self.size = 0;
        match storage::spawn(StorageEvent::DatalogEnd) {
            Err(_) => Err(DatalogError::UnableToEndDatalog),
            Ok(_) => Ok(())
        }
    }

    // Starts periodic tasks which handle the actual measurements
    pub fn trigger_tasks(&mut self, sensor_config: &SensorConfig) {
        // trigger each enabled sensor task
        for task_entry in SENSOR_TASK_TABLE {
            if (task_entry.enabled_fn)(sensor_config) {
                let rt = (task_entry.spawn_fn)();
                dl_check!(rt, "Failed to start sensor tasks");
            }
        }

        // trigger periodic menu redraw
        menu::spawn(MenuEvent::SensorRedraw).ok();
    }

    // Sets a point in time on which all further
    // relative timestamps are based on
    pub fn set_timestamp(&mut self) {
        self.start_time = monotonics::now();
    }

    pub fn stop(&mut self) {
        self.inprog = false;
    }

    pub fn resume(&mut self, sensor_config: &SensorConfig) {
        self.inprog = true;
        self.trigger_tasks(sensor_config);
    }

    pub fn inc_measured(&mut self, inc: u32) {
        self.no_measured += inc;
    }

    pub fn inc_dropped(&mut self, inc: u32) {
        self.no_dropped += inc;
    }

    // Increases the datalog estimated size by entry header size + param
    pub fn inc_size(&mut self, inc: u32) {
        self.size += inc + BEGIN_ENTRY_SIZE;
    }

    pub fn rel_time_us(&self) -> u64 {
        let now = monotonics::now();
        let now_us = now.ticks() * GRAN as u64;
        let start_time_us = self.start_time.ticks() * GRAN as u64;

        now_us - start_time_us
    }
}
