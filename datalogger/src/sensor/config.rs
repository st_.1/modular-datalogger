/*
 * Sensor: configuration options
 */

use serde::{Serialize, Deserialize};
use crate::menu::{SensorMenuConfig, SensorMenuVar};
use ::adxl345::{OutputDataRate, OutputDataRate::*, Range};

const DEFAULT_BOOLEAN: bool = false;
const DEFAULT_FULL_RES: bool = true;
const DEFAULT_PERIOD: u64 = 1_000_000;
const DEFAULT_DATA_RATE_ADXL345: OutputDataRate = ODR_3_13_Hz;
const DEFAULT_RANGE_ADXL345: Range = Range::_2G;

#[derive(PartialEq)]
pub enum SensorEntryState {
    Boolean(bool),
    Period(u64),
    DataRate(OutputDataRate),
    Range(Range),
    None
}

impl SensorEntryState {
    pub fn is_presetable(&self) -> bool {
        if let Self::Period(_) = self { return true; }
        if let Self::DataRate(_) = self { return true; }
        if let Self::Range(_) = self { return true; }

        false
    }
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct SensorConfig {
    pub dht11: Dht11Config,
    pub mcp9808: Mcp9808Config,
    pub adxl345: Adxl345Config,
    pub co2dummy: Co2DummyConfig
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct Dht11Config {
    pub temp_enabled: bool,
    pub hum_enabled: bool,
    pub period_us: u64
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct Mcp9808Config {
    pub temp_enabled: bool,
    pub period_us: u64
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct Adxl345Config {
    pub acc_enabled: bool,
    pub data_rate: OutputDataRate,
    pub range: Range,
    pub full_resolution: bool
}

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct Co2DummyConfig {
    pub co2_enabled: bool,
    pub temp_enabled: bool,
    pub hum_enabled: bool,
    pub period_us: u64
}

impl SensorConfig {
    pub fn new() -> Self {
        Self {
            dht11: Dht11Config::new(),
            mcp9808: Mcp9808Config::new(),
            adxl345: Adxl345Config::new(),
            co2dummy: Co2DummyConfig::new()
        }
    }

    // Get entry state by subconfig and variable type (eg. Dht11, Temperature)
    pub fn entry_state(&self, config: SensorMenuConfig, var: SensorMenuVar) -> SensorEntryState {
        match config {
            SensorMenuConfig::Dht11 => self.dht11.entry_state(var),
            SensorMenuConfig::Mcp9808 => self.mcp9808.entry_state(var),
            SensorMenuConfig::Adxl345 => self.adxl345.entry_state(var),
            SensorMenuConfig::Co2Dummy => self.co2dummy.entry_state(var)
        }
    }

    // Update entry state by subconfig and variable type (eg. Dht11, Temperature)
    pub fn update_entry_state(&mut self, config: SensorMenuConfig, var: SensorMenuVar, entry_state: SensorEntryState) {
        match config {
            SensorMenuConfig::Dht11 => self.dht11.update_entry_state(var, entry_state),
            SensorMenuConfig::Mcp9808 => self.mcp9808.update_entry_state(var, entry_state),
            SensorMenuConfig::Adxl345 => self.adxl345.update_entry_state(var, entry_state),
            SensorMenuConfig::Co2Dummy => self.co2dummy.update_entry_state(var, entry_state)
        }
    }

    pub fn active_sensors(&self) -> u8 {
        let mut count = 0;

        if self.dht11.enabled() { count += 1; }
        if self.mcp9808.enabled() { count += 1; }
        if self.adxl345.enabled() { count += 1; }
        if self.co2dummy.enabled() { count += 1; }

        count
    }
}

impl Dht11Config {
    pub fn new() -> Self {
        Self {
            temp_enabled: DEFAULT_BOOLEAN,
            hum_enabled: DEFAULT_BOOLEAN,
            period_us: DEFAULT_PERIOD
        }
    }

    pub fn entry_state(&self, var: SensorMenuVar) -> SensorEntryState {
        match var {
            SensorMenuVar::Temperature => SensorEntryState::Boolean(self.temp_enabled),
            SensorMenuVar::Humidity => SensorEntryState::Boolean(self.hum_enabled),
            SensorMenuVar::Period => SensorEntryState::Period(self.period_us),
            _ => SensorEntryState::None
        }
    }

    pub fn update_entry_state(&mut self, var: SensorMenuVar, entry_state: SensorEntryState) {
        match entry_state {
            SensorEntryState::Boolean(entry_state) => {
                match var {
                    SensorMenuVar::Temperature => self.temp_enabled = entry_state,
                    SensorMenuVar::Humidity => self.hum_enabled = entry_state,
                    _ => {}
                }
            },
            SensorEntryState::Period(period_us) => {
                if var != SensorMenuVar::Period { return; }
                self.period_us = period_us;
            },
            _ => {}
        }
    }

    pub fn enabled(&self) -> bool {
        self.temp_enabled || self.hum_enabled
    }
}

impl Mcp9808Config {
    pub fn new() -> Self {
        Self {
            temp_enabled: DEFAULT_BOOLEAN,
            period_us: DEFAULT_PERIOD
        }
    }

    pub fn entry_state(&self, var: SensorMenuVar) -> SensorEntryState {
        match var {
            SensorMenuVar::Temperature => SensorEntryState::Boolean(self.temp_enabled),
            SensorMenuVar::Period => SensorEntryState::Period(self.period_us),
            _ => SensorEntryState::None
        }
    }

    pub fn update_entry_state(&mut self, var: SensorMenuVar, entry_state: SensorEntryState) {
        match entry_state {
            SensorEntryState::Boolean(entry_state) => {
                if var != SensorMenuVar::Temperature { return; }
                self.temp_enabled = entry_state;
            },
            SensorEntryState::Period(period_us) => {
                if var != SensorMenuVar::Period { return; }
                self.period_us = period_us;
            },
            _ => {}
        }
    }

    pub fn enabled(&self) -> bool {
        self.temp_enabled
    }
}

impl Adxl345Config {
    pub fn new() -> Self {
        Self {
            acc_enabled: DEFAULT_BOOLEAN,
            data_rate: DEFAULT_DATA_RATE_ADXL345,
            range: DEFAULT_RANGE_ADXL345,
            full_resolution: DEFAULT_FULL_RES
        }
    }

    pub fn entry_state(&self, var: SensorMenuVar) -> SensorEntryState {
        match var {
            SensorMenuVar::Acceleration => SensorEntryState::Boolean(self.acc_enabled),
            SensorMenuVar::DataRate => SensorEntryState::DataRate(self.data_rate),
            SensorMenuVar::Range => SensorEntryState::Range(self.range),
            SensorMenuVar::FullResolution => SensorEntryState::Boolean(self.full_resolution),
            _ => SensorEntryState::None
        }
    }

    pub fn update_entry_state(&mut self, var: SensorMenuVar, entry_state: SensorEntryState) {
        match entry_state {
            SensorEntryState::Boolean(entry_state) => {
                match var {
                    SensorMenuVar::Acceleration => self.acc_enabled = entry_state,
                    SensorMenuVar::FullResolution => self.full_resolution = entry_state,
                    _ => {}
                }
            },
            SensorEntryState::DataRate(data_rate) => {
                if var != SensorMenuVar::DataRate { return; }
                self.data_rate = data_rate;
            },
            SensorEntryState::Range(range) => {
                if var != SensorMenuVar::Range { return; }
                self.range = range;
            },
            _ => {}
        }
    }

    // converts data rate (Hz) to period (us)
    pub fn period_us(&self) -> u64 {
        let data_rate_ft: f32 = self.data_rate.into();

        (1f32 / data_rate_ft * 1_000_000f32) as u64
    }

    pub fn enabled(&self) -> bool {
        self.acc_enabled
    }
}

impl Co2DummyConfig {
    pub fn new() -> Self {
        Self {
            co2_enabled: DEFAULT_BOOLEAN,
            temp_enabled: DEFAULT_BOOLEAN,
            hum_enabled: DEFAULT_BOOLEAN,
            period_us: DEFAULT_PERIOD
        }
    }

    pub fn entry_state(&self, var: SensorMenuVar) -> SensorEntryState {
        match var {
            SensorMenuVar::CO2 => SensorEntryState::Boolean(self.co2_enabled),
            SensorMenuVar::Temperature => SensorEntryState::Boolean(self.temp_enabled),
            SensorMenuVar::Humidity => SensorEntryState::Boolean(self.hum_enabled),
            SensorMenuVar::Period => SensorEntryState::Period(self.period_us),
            _ => SensorEntryState::None
        }
    }

    pub fn update_entry_state(&mut self, var: SensorMenuVar, entry_state: SensorEntryState) {
        match entry_state {
            SensorEntryState::Boolean(entry_state) => {
                match var {
                    SensorMenuVar::CO2 => self.co2_enabled = entry_state,
                    SensorMenuVar::Temperature => self.temp_enabled = entry_state,
                    SensorMenuVar::Humidity => self.hum_enabled = entry_state,
                    _ => {}
                }
            },
            SensorEntryState::Period(period_us) => {
                if var != SensorMenuVar::Period { return; }
                self.period_us = period_us;
            },
            _ => {}
        }
    }

    pub fn enabled(&self) -> bool {
        self.co2_enabled || self.temp_enabled || self.hum_enabled
    }
}
