use embedded_hal::{serial::{Read, Write}, blocking::delay::{DelayMs, DelayUs}};
use systick_monotonic::*;
use rtic::mutex_prelude::*;
use core::mem::size_of;
use ::co2dummy::{Co2Dummy, Co2DummyError};

use crate::app::{sensor_co2dummy, monotonics, GRAN, SensorDelay};
use crate::storage::{DatalogWriter, EntryValue, EntryValueType, DatalogWriterError, StorageEvent, StorageQueueEntry};
use super::{MeasurementResult, Sensor, DatalogState, SensorConfig, SystInstant};

pub struct Co2DummySensor<SERIAL>
where SERIAL: Write<u8> + Read<u8> {
    sensor: Co2Dummy<SERIAL>,
    id: u16,
    last_meas: Option<SystInstant>
}

impl<SERIAL> Co2DummySensor<SERIAL>
where SERIAL: Write<u8> + Read<u8> {
    pub fn new<D>(serial: SERIAL, delay: &mut D, id: u16) -> Result<Self, Co2DummyError>
    where D: DelayUs<u16> + DelayMs<u16> {
        let mut sensor = Co2Dummy::new(serial, delay)?;
        sensor.set_interval(2)?;
        sensor.trigger_measurement()?;

        Ok(Self { sensor, id, last_meas: None })
    }
}

impl<SERIAL> Sensor for Co2DummySensor<SERIAL>
where SERIAL: Write<u8> + Read<u8> {

    fn measure(&mut self, _: &mut SensorDelay, sensor_config: &SensorConfig, rel_time_us: u64) -> MeasurementResult {
        // measure
        let meas = self.sensor.read_measurement();

        // append to storage queue
        self.storage_begin_entry(
            self.id,
            rel_time_us,
            if meas.is_ok() { MeasurementResult::Success } else { MeasurementResult::Failure }
        ).unwrap();

        if let Ok(meas) = &meas {
            if sensor_config.co2dummy.co2_enabled {
                self.storage_log_var(EntryValue::F32(meas.co2_concentration)).unwrap();
            }
            if sensor_config.co2dummy.temp_enabled {
                self.storage_log_var(EntryValue::F32(meas.temperature)).unwrap();
            }
            if sensor_config.co2dummy.hum_enabled {
                self.storage_log_var(EntryValue::F32(meas.humidity)).unwrap();
            }
        }

        // return status info
        match meas {
            Ok(meas) => {
                uprintln!("[co2dummy] co2: {:.2} temp: {:.2} hum: {:.2}", meas.co2_concentration, meas.temperature, meas.humidity);
                MeasurementResult::Success
            },
            Err(_) => {
                uprintln!("[co2dummy] unable to retrieve");
                MeasurementResult::Failure
            }
        }
    }

    fn write_header(&self, writer: &mut DatalogWriter, sensor_config: &SensorConfig) -> Result<(), DatalogWriterError> {
        let config = &sensor_config.co2dummy;
        let mut no_vars = 0;

        no_vars += if config.co2_enabled { 1 } else { 0 };
        no_vars += if config.temp_enabled { 1 } else { 0 };
        no_vars += if config.hum_enabled { 1 } else { 0 };

        writer.begin_header("CO2DUMMY", self.id, config.period_us, no_vars)?;

        if config.co2_enabled {
            writer.add_var("CO2 Concentration", "ppm", EntryValueType::F32)?;
        }

        if config.temp_enabled {
            writer.add_var("Temperature", "°C", EntryValueType::F32)?;
        }

        if config.hum_enabled {
            writer.add_var("Humidity", "%", EntryValueType::F32)?;
        }

        Ok(())
    }

    fn set_last(&mut self, last: Option<SystInstant>) {
        self.last_meas = last;
    }

    fn get_last(&self) -> Option<SystInstant> {
        self.last_meas
    }

    fn id(&self) -> u16 {
        self.id
    }

    fn size_inc(&self, sensor_config: &SensorConfig) -> u32 {
        let config = &sensor_config.co2dummy;
        let mut size_inc = 0u32;

        if config.temp_enabled { size_inc += size_of::<f32>() as u32; }
        if config.hum_enabled { size_inc += size_of::<f32>() as u32; }
        if config.co2_enabled { size_inc += size_of::<f32>() as u32; }

        size_inc
    }

    fn enabled(&self, sensor_config: &SensorConfig) -> bool {
        sensor_config.co2dummy.enabled()
    }
}

pub fn sensor_co2dummy(cx: sensor_co2dummy::Context) {
    let delay = cx.shared.delay;
    let datalog_state = cx.shared.datalog_state;
    let sensors = cx.shared.sensors;
    let sensor_config = cx.shared.sensor_config;

    (delay, datalog_state, sensors, sensor_config).lock(|delay, datalog_state, sensors, sensor_config| {
        let co2dummy = &mut sensors.co2dummy;

        // measure & store
        let meas_result = co2dummy.measure(delay, sensor_config, datalog_state.rel_time_us());

        // schedule next measurement in the closest available time slot
        if let Some(sched_time) = co2dummy.schedule(datalog_state, meas_result,
                                                    sensor_config.co2dummy.enabled(),
                                                    sensor_config.co2dummy.period_us,
                                                    sensor_config) {
            sensor_co2dummy::spawn_at(sched_time).unwrap();
        }
    });
}
