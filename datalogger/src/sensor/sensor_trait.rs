use super::{SystInstant, datalog_state::*, config::SensorConfig};
use crate::app::{sensor_dht11, monotonics, GRAN, SensorDelay, storage};
use crate::storage::{DatalogWriter, DatalogWriterError, StorageQueueEntry, StorageEvent, EntryValue};
use embedded_hal::blocking::delay::{DelayMs, DelayUs};
use systick_monotonic::*;
use core::mem::size_of;

#[derive(Debug)]
pub enum MeasurementResult {
    Success,
    Failure
}

pub trait Sensor {
    fn write_header(&self, writer: &mut DatalogWriter, sensor_config: &SensorConfig) -> Result<(), DatalogWriterError>;
    fn set_last(&mut self, _last: Option<SystInstant>) {}
    fn get_last(&self) -> Option<SystInstant> { None }
    fn size_inc(&self, sensor_config: &SensorConfig) -> u32;
    fn enabled(&self, sensor_config: &SensorConfig) -> bool;
    fn id(&self) -> u16;

    // periodic requests
    fn measure(&mut self, delay: &mut SensorDelay, sensor_config: &SensorConfig, rel_time_us: u64) -> MeasurementResult;
    fn schedule(&mut self,
                datalog_state: &mut DatalogState,
                meas_result: MeasurementResult,
                sensor_enabled: bool,
                period_us: u64,
                sensor_config: &SensorConfig) -> Option<SystInstant> {

        // add measurement result to datalog stats
        match meas_result {
            MeasurementResult::Success => {
                datalog_state.inc_measured(1);
                datalog_state.inc_size(self.size_inc(&sensor_config));
            },
            MeasurementResult::Failure => {
                datalog_state.inc_dropped(1);
                datalog_state.inc_size(0);
            }
        }

        if datalog_state.inprog && sensor_enabled {
            // time offset = period - (relative time % period)
            let now = monotonics::now();
            let now_us = now.ticks() * GRAN as u64;
            let rel_time_us = datalog_state.rel_time_us();
            let offset = (period_us - (rel_time_us % period_us)).micros();

            // compute dropped measurements, update datalog stats
            if let Some(last) = self.get_last() {
                let last_us = last.ticks() * GRAN as u64;

                if now_us - last_us > period_us {
                    uprintln!("can't keep up: {} > {}, inc: {}", (now_us - last_us) as f64 / 1_000_000f64, period_us as f64 / 1_000_000f64, (now_us - last_us - period_us) / period_us);
                    let no_dropped = ((now_us - last_us - period_us) / period_us) as u32;
                    datalog_state.inc_dropped(no_dropped);
                }
            }

            //uprintln!("rel_time_us: {} offset: {}", rel_time_us as f64 / 1_000_000f64, (period_us - (rel_time_us % period_us)) as f64 / 1_000_000f64);
            //uprintln!("measured: {} dropped: {}", datalog_state.no_measured, datalog_state.no_dropped);

            // set last measurement timestamp and return
            self.set_last(Some(now + offset));
            Some(now + offset)
        } else {
            self.set_last(None);
            None
        }
    }

    // optional DMA support
    fn poll(&mut self,
            _datalog_state: &mut DatalogState,
            _sensor_config: &SensorConfig) -> Option<SystInstant> { None }
    fn transfer_rx(&mut self,
                   _datalog_state: &mut DatalogState,
                   _sensor_config: &SensorConfig) {}

    // StorageEvent::DatalogAppend shorthands
    fn storage_begin_entry(&self, sensor_id: u16, timestamp: u64, result: MeasurementResult) -> Result<(), StorageEvent> {
        storage::spawn(StorageEvent::DatalogAppend(
            StorageQueueEntry::BeginEntry { sensor_id, timestamp, result }
        ))
    }

    fn storage_log_var(&self, value: EntryValue) -> Result<(), StorageEvent> {
        storage::spawn(StorageEvent::DatalogAppend(
            StorageQueueEntry::LogVar { value }
        ))
    }
}
