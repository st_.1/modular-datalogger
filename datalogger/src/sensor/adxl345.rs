#![allow(dead_code)]
#![allow(non_camel_case_types)]

use embedded_hal::{spi::FullDuplex, blocking::spi::Transfer, digital::v2::{InputPin, OutputPin}};
use systick_monotonic::*;
use rtic::mutex_prelude::*;
use core::{mem::size_of, marker::PhantomData};
use ::adxl345::{Adxl345Dma, Adxl345Config, Error, F32x3, Accelerometer, PowerMode, FifoMode, Range, OutputDataRate::*, BUF_SIZE};

use super::{MeasurementResult, Sensor, DatalogState, SensorConfig, SystInstant};
use crate::storage::{DatalogWriter, EntryValue, EntryValueType, DatalogWriterError, StorageEvent, StorageQueueEntry};
use crate::app::{sensor_adxl345_polling, sensor_adxl345_dma_rx, monotonics, GRAN, SensorDelay};

use stm32f4xx_hal::{
    dma, dma::{StreamX, ChannelX, MemoryToPeripheral, PeripheralToMemory, StreamsTuple, traits::{StreamISR, DMASet, Channel}},
    spi, spi::{Tx, Rx, Spi},
    rcc
};

// SPI2_TX dma 1 channel 0 stream 4
// SPI2_RX dma 1 channel 0 stream 3

pub struct Adxl345Sensor<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, const S_TX: u8, const S_RX: u8, const CH_TX: u8, const CH_RX: u8>
where DMA: dma::traits::Instance,
      SPI: spi::Instance,
      CS: OutputPin,
      INT0: InputPin,
      Spi<SPI, SPI_PINS, SPI_TM>: FullDuplex<u8> + Transfer<u8>,
      StreamX<DMA, S_TX>: StreamISR,
      StreamX<DMA, S_RX>: StreamISR,
      ChannelX<CH_TX>: Channel,
      ChannelX<CH_RX>: Channel {

    sensor: Adxl345Dma<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, S_TX, S_RX, CH_TX, CH_RX>,
    // unique sensor id
    id: u16,
    // start time of the last transfer
    last_transfer_us: u64,
    // start time of the transfer currently in progress
    current_transfer_us: Option<u64>,
    // number of processed entries in the current transfer
    entry_count: u32
}

impl<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, const S_TX: u8, const S_RX: u8, const CH_TX: u8, const CH_RX: u8>
Adxl345Sensor<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, S_TX, S_RX, CH_TX, CH_RX>
where DMA: dma::traits::Instance,
      SPI: spi::Instance,
      CS: OutputPin,
      INT0: InputPin,
      Spi<SPI, SPI_PINS, SPI_TM>: FullDuplex<u8> + Transfer<u8>,
      Tx<SPI>: DMASet<StreamX<DMA, S_TX>, CH_TX, MemoryToPeripheral>,
      Rx<SPI>: DMASet<StreamX<DMA, S_RX>, CH_RX, PeripheralToMemory>,
      StreamX<DMA, S_TX>: StreamISR,
      StreamX<DMA, S_RX>: StreamISR,
      ChannelX<CH_TX>: Channel,
      ChannelX<CH_RX>: Channel {

    pub fn new(spi: Spi<SPI, SPI_PINS, SPI_TM>,
               cs: CS,
               int0: INT0,
               stream_tx: StreamX<DMA, S_TX>,
               stream_rx: StreamX<DMA, S_RX>,
               sensor_config: &SensorConfig,
               id: u16) -> Result<Self, Error> {

        let sensor_config = &sensor_config.adxl345;

        // configure peripheral according to sensor config
        let config = Adxl345Config::new()
            .power_mode(PowerMode::Measure)
            .full_resolution(sensor_config.full_resolution)
            .data_rate(sensor_config.data_rate)
            .range(sensor_config.range)
            .fifo_samples(16)
            .fifo_mode(FifoMode::FIFO);

        // alloc static buffers
        let buffers = (
            cortex_m::singleton!(: [u8; BUF_SIZE] = [0; BUF_SIZE]).unwrap(),
            cortex_m::singleton!(: [u8; BUF_SIZE] = [0; BUF_SIZE]).unwrap(),
            cortex_m::singleton!(: [u8; BUF_SIZE] = [0; BUF_SIZE]).unwrap()
        );

        let sensor = Adxl345Dma::new(spi, cs, int0, stream_tx, stream_rx, config, buffers)?;

        Ok(Self { sensor, id, last_transfer_us: 0, current_transfer_us: None, entry_count: 0 })
    }
}

impl<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, const S_TX: u8, const S_RX: u8, const CH_TX: u8, const CH_RX: u8> Sensor
for Adxl345Sensor<SPI, SPI_PINS, SPI_TM, DMA, CS, INT0, S_TX, S_RX, CH_TX, CH_RX>
where DMA: dma::traits::Instance,
      SPI: spi::Instance,
      CS: OutputPin,
      INT0: InputPin,
      Spi<SPI, SPI_PINS, SPI_TM>: FullDuplex<u8> + Transfer<u8>,
      Tx<SPI>: DMASet<StreamX<DMA, S_TX>, CH_TX, MemoryToPeripheral>,
      Rx<SPI>: DMASet<StreamX<DMA, S_RX>, CH_RX, PeripheralToMemory>,
      StreamX<DMA, S_TX>: StreamISR,
      StreamX<DMA, S_RX>: StreamISR,
      ChannelX<CH_TX>: Channel,
      ChannelX<CH_RX>: Channel {

    fn measure(&mut self, _: &mut SensorDelay, _sensor_config: &SensorConfig, _rel_time_us: u64) -> MeasurementResult {
        // NOTE: Periodic requests not implemented,
        // instead an external FIFO approach was used via the poll & transfer_rx methods.

        MeasurementResult::Failure
    }

    fn write_header(&self, writer: &mut DatalogWriter, sensor_config: &SensorConfig) -> Result<(), DatalogWriterError> {
        let config = &sensor_config.adxl345;

        writer.begin_header("ADXL345", self.id, config.period_us(), 3)?;

        if config.acc_enabled {
            writer.add_var("Acceleration x axis", "g", EntryValueType::F32)?;
            writer.add_var("Acceleration y axis", "g", EntryValueType::F32)?;
            writer.add_var("Acceleration z axis", "g", EntryValueType::F32)?;
        }

        Ok(())
    }

    fn id(&self) -> u16 {
        self.id
    }

    fn size_inc(&self, sensor_config: &SensorConfig) -> u32 {
        if sensor_config.adxl345.acc_enabled {
            size_of::<F32x3>() as u32
        } else {
            0
        }
    }

    fn enabled(&self, sensor_config: &SensorConfig) -> bool {
        sensor_config.adxl345.enabled()
    }

    fn poll(&mut self,
            datalog_state: &mut DatalogState,
            sensor_config: &SensorConfig) -> Option<SystInstant> {

        let config = &sensor_config.adxl345;

        // stop polling if datalog is no longer in progress
        if !datalog_state.inprog {
            return None;
        }

        // call driver polling function
        if self.sensor.poll() {
            let last_entry_count = self.entry_count;

            // save current relative timestamp on a successful poll,
            // use the previous value as a point of reference
            if let Some(new_last_transfer_us) = self.current_transfer_us {
                self.last_transfer_us = new_last_transfer_us;
                self.entry_count = 0;
            }
            let current_us = datalog_state.rel_time_us();
            self.current_transfer_us = Some(current_us);

            // update dropped measurement stats
            let period_us = sensor_config.adxl345.period_us() as i32;
            let leftover_us = current_us as i32 - self.last_transfer_us as i32 - (last_entry_count as i32 * period_us);

            if leftover_us > period_us {
                uprintln!("can't keep up: {} > {}, inc: {}", leftover_us as f64 / 1_000_000f64, period_us as f64 / 1_000_000f64, leftover_us / period_us);
                datalog_state.inc_dropped((leftover_us / period_us) as u32);
            }
        }

        // return next polling time
        Some(monotonics::now() + (config.period_us() / 4).micros::<1, GRAN>())
    }

    fn transfer_rx(&mut self,
                   datalog_state: &mut DatalogState,
                   sensor_config: &SensorConfig) {

        if ! datalog_state.inprog { return; }

        let meas = self.sensor.transfer_rx();

        // append to storage queue
        self.storage_begin_entry(
            self.id,
            self.last_transfer_us + (sensor_config.adxl345.period_us() * self.entry_count as u64),
            if meas.is_ok() { MeasurementResult::Success } else { MeasurementResult::Failure }
        ).unwrap();

        //uprintln!("storage timepoint: {:.2}", (self.last_transfer_us + (sensor_config.adxl345.period_us() * self.entry_count as u64)) as f32 / 1000f32);

        if let Ok(accel) = meas {
            self.storage_log_var(EntryValue::F32(accel.x)).unwrap();
            self.storage_log_var(EntryValue::F32(accel.y)).unwrap();
            self.storage_log_var(EntryValue::F32(accel.z)).unwrap();
        }

        self.entry_count += 1;

        // add measurement result to datalog stats
        if meas.is_ok() {
            datalog_state.inc_measured(1);
            datalog_state.inc_size(self.size_inc(sensor_config));
        } else {
            datalog_state.inc_dropped(1);
            datalog_state.inc_size(0);
        }

        // print status info
        match meas {
            Ok(accel) => {
                uprintln!("[adxl345] accel: {:?}", accel);
            },
            Err(_) => {
                uprintln!("[adxl345] unable to retrieve");
            }
        }
    }
}

pub fn sensor_adxl345_polling(cx: sensor_adxl345_polling::Context) {
    let (sensors, datalog_state, sensor_config) = (cx.shared.sensors, cx.shared.datalog_state, cx.shared.sensor_config);

    (sensors, datalog_state, sensor_config).lock(|sensors, datalog_state, sensor_config| {
        let next_poll = sensors.adxl345.poll(datalog_state, sensor_config);

        if let Some(next_poll) = next_poll {
            sensor_adxl345_polling::spawn_at(next_poll).unwrap();
        }
    });
}

pub fn sensor_adxl345_dma_rx(cx: sensor_adxl345_dma_rx::Context) {
    let (sensors, datalog_state, sensor_config) = (cx.shared.sensors, cx.shared.datalog_state, cx.shared.sensor_config);

    (sensors, datalog_state, sensor_config).lock(|sensors, datalog_state, sensor_config| {
        sensors.adxl345.transfer_rx(datalog_state, sensor_config);
    });
}
