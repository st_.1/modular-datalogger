use embedded_hal::blocking::i2c::{WriteRead, Write};
use rtic::mutex_prelude::*;
use core::mem::size_of;
use ::mcp9808::{
    MCP9808,
    reg_conf::ShutdownMode,
    reg_res::ResolutionVal,
    reg_temp_generic::ReadableTempRegister,
    reg_conf::Configuration,
};

use crate::app::{sensor_mcp9808, monotonics, GRAN, SensorDelay};
use crate::storage::{DatalogWriter, EntryValue, EntryValueType, DatalogWriterError, StorageEvent, StorageQueueEntry};
use super::{MeasurementResult, Sensor, DatalogState, SensorConfig, SystInstant};

pub struct Mcp9808Sensor<I2C>
where I2C: WriteRead {
    sensor: MCP9808<I2C>,
    id: u16,
    last_meas: Option<SystInstant>
}

impl<I2C, E> Mcp9808Sensor<I2C>
where I2C: Write<Error = E> + WriteRead<Error = E> {
    pub fn new(i2c: I2C, id: u16) -> Result<Self, mcp9808::Error<E>> {
        let mut mcp9808 = MCP9808::new(i2c);

        let mut conf = mcp9808.read_configuration()?;
        conf.set_shutdown_mode(ShutdownMode::Continuous);
        mcp9808.write_register(conf)?;

        Ok(Self { sensor: mcp9808, id, last_meas: None })
    }
}

impl<I2C, E> Sensor for Mcp9808Sensor<I2C>
where I2C: Write<Error = E> + WriteRead<Error = E> {
    fn measure(&mut self, _: &mut SensorDelay, sensor_config: &SensorConfig, rel_time_us: u64) -> MeasurementResult {
        // measure
        let meas = self.sensor.read_temperature();
        let mut temp_c: f32 = 0f32;

        if let Ok(temp) = meas {
            temp_c = temp.get_celcius(ResolutionVal::Deg_0_0625C);
        }

        // append to storage queue
        self.storage_begin_entry(
            self.id,
            rel_time_us,
            if meas.is_ok() { MeasurementResult::Success } else { MeasurementResult::Failure }
        ).unwrap();

        if meas.is_ok() && sensor_config.mcp9808.temp_enabled {
            self.storage_log_var(EntryValue::F32(temp_c)).unwrap();
        }

        // return status info
        match meas {
            Ok(_) => {
                uprintln!("[mcp9808] temp: {:.2}", temp_c);
                MeasurementResult::Success
            },
            Err(_) => {
                uprintln!("[mcp9808] unable to retrieve");
                MeasurementResult::Failure
            }
        }
    }

    fn write_header(&self, writer: &mut DatalogWriter, sensor_config: &SensorConfig) -> Result<(), DatalogWriterError> {
        let config = &sensor_config.mcp9808;

        writer.begin_header("MCP9808", self.id, config.period_us, 1)?;

        if config.temp_enabled {
            writer.add_var("Temperature", "°C", EntryValueType::F32)?;
        }

        Ok(())
    }

    fn set_last(&mut self, last: Option<SystInstant>) {
        self.last_meas = last;
    }

    fn get_last(&self) -> Option<SystInstant> {
        self.last_meas
    }

    fn id(&self) -> u16 {
        self.id
    }

    fn size_inc(&self, sensor_config: &SensorConfig) -> u32 {
        if sensor_config.mcp9808.temp_enabled {
            size_of::<f32>() as u32
        } else {
            0
        }
    }

    fn enabled(&self, sensor_config: &SensorConfig) -> bool {
        sensor_config.mcp9808.enabled()
    }
}

pub fn sensor_mcp9808(cx: sensor_mcp9808::Context) {
    let delay = cx.shared.delay;
    let datalog_state = cx.shared.datalog_state;
    let sensors = cx.shared.sensors;
    let sensor_config = cx.shared.sensor_config;

    (delay, datalog_state, sensors, sensor_config).lock(|delay, datalog_state, sensors, sensor_config| {
        let mcp9808 = &mut sensors.mcp9808;

        // measure & store
        let meas_result = mcp9808.measure(delay, sensor_config, datalog_state.rel_time_us());

        // schedule next measurement in the closest available time slot
        if let Some(sched_time) = mcp9808.schedule(datalog_state, meas_result,
                                                   sensor_config.mcp9808.enabled(),
                                                   sensor_config.mcp9808.period_us,
                                                   sensor_config) {
            sensor_mcp9808::spawn_at(sched_time).unwrap();
        }
    });
}
