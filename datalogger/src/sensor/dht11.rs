use embedded_hal::digital::v2::{InputPin, OutputPin};
use embedded_hal::blocking::delay::{DelayUs, DelayMs};
use systick_monotonic::*;
use rtic::mutex_prelude::*;
use core::mem::size_of;
use ::dht11::Dht11;

use crate::app::{sensor_dht11, monotonics, GRAN, SensorDelay, storage};
use crate::storage::{DatalogWriter, EntryValue, EntryValueType, DatalogWriterError, StorageEvent, StorageQueueEntry};
use super::{MeasurementResult, Sensor, DatalogState, SensorConfig, SystInstant};

pub struct Dht11Sensor<GPIO>
where GPIO : InputPin + OutputPin {
    sensor: Dht11<GPIO>,
    id: u16,
    last_meas: Option<SystInstant>,
}

impl<GPIO, E> Dht11Sensor<GPIO>
where GPIO: InputPin<Error = E> + OutputPin<Error = E> {
    pub fn new(gpio: GPIO, id: u16) -> Self {
        Dht11Sensor {
            sensor: Dht11::new(gpio),
            id,
            last_meas: None
        }
    }
}

impl<GPIO, E> Sensor for Dht11Sensor<GPIO>
where GPIO: InputPin<Error = E> + OutputPin<Error = E> {
    fn measure(&mut self, delay: &mut SensorDelay, sensor_config: &SensorConfig, rel_time_us: u64) -> MeasurementResult {
        // measure
        let meas = self.sensor.perform_measurement(delay);

        // append to storage queue
        self.storage_begin_entry(
            self.id,
            rel_time_us,
            if meas.is_ok() { MeasurementResult::Success } else { MeasurementResult::Failure }
        ).unwrap();

        if let Ok(meas) = meas {
            let config = &sensor_config.dht11;

            if config.temp_enabled {
                self.storage_log_var(EntryValue::I16(meas.temperature)).unwrap();
            }

            if config.hum_enabled {
                self.storage_log_var(EntryValue::U16(meas.humidity)).unwrap();
            }
        }

        // return status info
        match meas {
            Ok(meas) => {
                uprintln!("[dht11] temp: {:.1} humidity: {:.1}%", meas.temperature as f32 / 10.0, meas.humidity as f32 / 10.0);
                MeasurementResult::Success
            },
            Err(_err) => {
                uprintln!("[dht11] unable to retrieve");
                MeasurementResult::Failure
            }
        }
    }

    fn write_header(&self, writer: &mut DatalogWriter, sensor_config: &SensorConfig) -> Result<(), DatalogWriterError> {
        let config = &sensor_config.dht11;
        let mut no_vars = 0;

        no_vars += if config.temp_enabled { 1 } else { 0 };
        no_vars += if config.hum_enabled { 1 } else { 0 };

        writer.begin_header("DHT11", self.id, config.period_us, no_vars)?;

        if config.temp_enabled { writer.add_var("Temperature", "°C", EntryValueType::I16)?; }
        if config.hum_enabled { writer.add_var("Humidity", "%", EntryValueType::U16)?; }

        Ok(())
    }

    fn set_last(&mut self, last: Option<SystInstant>) {
        self.last_meas = last;
    }

    fn get_last(&self) -> Option<SystInstant> {
        self.last_meas
    }

    fn id(&self) -> u16 {
        self.id
    }

    fn size_inc(&self, sensor_config: &SensorConfig) -> u32 {
        let config = &sensor_config.dht11;
        let mut size_inc = 0u32;

        if config.temp_enabled { size_inc += size_of::<i16>() as u32; }
        if config.hum_enabled { size_inc += size_of::<u16>() as u32; }

        size_inc
    }

    fn enabled(&self, sensor_config: &SensorConfig) -> bool {
        sensor_config.dht11.enabled()
    }
}

pub fn sensor_dht11(cx: sensor_dht11::Context) {
    let delay = cx.shared.delay;
    let datalog_state = cx.shared.datalog_state;
    let sensors = cx.shared.sensors;
    let sensor_config = cx.shared.sensor_config;

    (delay, datalog_state, sensors, sensor_config).lock(|delay, datalog_state, sensors, sensor_config| {
        let dht11 = &mut sensors.dht11;

        // measure & store
        let meas_result = dht11.measure(delay, sensor_config, datalog_state.rel_time_us());

        // schedule next measurement in the closest available time slot
        if let Some(sched_time) = dht11.schedule(datalog_state, meas_result,
                                                 sensor_config.dht11.enabled(),
                                                 sensor_config.dht11.period_us,
                                                 sensor_config) {
            sensor_dht11::spawn_at(sched_time).unwrap();
        }
    });
}
