use embedded_hal::blocking::delay::{DelayMs, DelayUs};
use heapless::Vec;
use super::{Sensor, Sensors, SENSOR_COUNT};

pub struct SensorsIter<'s> {
    idx: usize,
    ref_vec: Vec<&'s dyn Sensor, SENSOR_COUNT>
}

impl<'s> IntoIterator for &'s Sensors {
    type Item = &'s dyn Sensor;
    type IntoIter = SensorsIter<'s>;

    fn into_iter(self) -> <Self as IntoIterator>::IntoIter {
        Self::IntoIter { idx: 0, ref_vec: self.as_vec() }
    }
}

impl<'s> Iterator for SensorsIter<'s> {
    type Item = &'s dyn Sensor;

    fn next(&mut self) -> Option<Self::Item> {
        self.idx += 1;

        if self.ref_vec.len() >= self.idx {
            Some(self.ref_vec[self.idx - 1])
        } else {
            None
        }
    }
}
