use crate::app::{sensor_dht11, sensor_mcp9808, sensor_adxl345_polling, sensor_co2dummy};
use super::config::*;

pub struct SensorTaskEntry {
    pub enabled_fn: fn(sensor_config: &SensorConfig) -> bool,
    pub spawn_fn: fn() -> Result<(),()>
}

// Sensor Task Table: A complete list of sensor tasks and their respetive enable
// functions in SensorConfig
pub const SENSOR_TASK_TABLE: &[SensorTaskEntry] = &[
    SensorTaskEntry {
        enabled_fn: |sensor_config| -> bool { sensor_config.dht11.enabled() },
        spawn_fn: sensor_dht11::spawn
    },
    SensorTaskEntry {
        enabled_fn: |sensor_config| -> bool { sensor_config.mcp9808.enabled() },
        spawn_fn: sensor_mcp9808::spawn
    },
    SensorTaskEntry {
        enabled_fn: |sensor_config| -> bool { sensor_config.adxl345.enabled() },
        spawn_fn: sensor_adxl345_polling::spawn
    },
    SensorTaskEntry {
        enabled_fn: |sensor_config| -> bool { sensor_config.co2dummy.enabled() },
        spawn_fn: sensor_co2dummy::spawn
    }
];
