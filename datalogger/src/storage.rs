use crate::buttons::ButtonEvent;
use crate::app::{menu, storage};
use crate::menu::MenuEvent;
use crate::sensor::{Sensors, MeasurementResult, SensorConfig};
use crate::panic::dl_check;

mod sdpartition;
mod generic_mbr_table;
mod file_list;
mod storage_stats;
mod datalog_writer;
mod config;

use sdpartition::SdPartition;
use fsutils::FsCache;
use generic_mbr_table::*;
use heapless::{Vec, String, spsc::Queue};
use embedded_hal::blocking::delay::{DelayMs, DelayUs};
use rtic::mutex_prelude::*;

pub use datalog_writer::*;
pub use file_list::*;
pub use storage_stats::StorageStats;
pub use config::{StorageConfig, OUT_DIR_PATH_LEN};
pub use generic_mbr_table::*;

use stm32f4xx_hal::{rcc::Clocks, sdio, sdio::{ClockFreq, Sdio, SdCard}, pac::SDIO, gpio::{gpioc::*, gpiod::PD2, PinMode}};
use core::{result::Result, str::from_utf8, fmt::Write, cell::RefCell};
use fatfs::{File, FileSystem, LossyOemCpConverter, NullTimeProvider, Error, FileSystemStats, Read, Write as WriteOther, Seek, SeekFrom, Dir};

extern crate fatfs;

const STORAGE_QUEUE_LEN: usize = 128;

#[derive(Debug)]
pub enum StorageQueueEntry {
    BeginEntry { sensor_id: u16, timestamp: u64, result: MeasurementResult },
    LogVar { value: EntryValue }
}

#[derive(Debug)]
pub enum StorageEvent {
    UpdateStorageStats,
    UpdateFileList(bool),
    ReloadConfig,
    DatalogInit,
    DatalogAppend(StorageQueueEntry),
    DatalogEnd,
    None
}

#[derive(Debug)]
pub enum StorageError {
    SdioError(sdio::Error),
    FatFsError(Error<sdpartition::Error>),
    DirStackOverflow,
    InvalidMbrSignature,
    NoSupportedPartition,
    DatalogNotInitialized,
    UnableToWriteToDatalog(DatalogWriterError),
    UnableToOpenLogFile(Error<sdpartition::Error>),
    UnableToInitDatalog,
    UnableToEndDatalog
}

impl From<Error<sdpartition::Error>> for StorageError {
    fn from(fatfs_error: Error<sdpartition::Error>) -> Self {
        Self::FatFsError(fatfs_error)
    }
}

impl From<DatalogWriterError> for StorageError {
    fn from(err: DatalogWriterError) -> Self { Self::UnableToWriteToDatalog(err) }
}

pub struct Storage {
    fs: FileSystem<FsCache<SdPartition>, NullTimeProvider, LossyOemCpConverter>,
    mbr_table: GenericMbrTable,
    part_entry_idx: usize,
    filename: Option<String<11>>,
    out_dir: Option<String<OUT_DIR_PATH_LEN>>,
    queue: Queue<StorageQueueEntry, STORAGE_QUEUE_LEN>
}

impl Storage {
    pub fn new<M0, M1, M2, M3, M4, M5>(
        sdio: SDIO,
        clocks: &Clocks,
        clk: PC12<M0>,
        cmd: PD2<M1>,
        d0: PC8<M2>,
        d1: PC9<M3>,
        d2: PC10<M4>,
        d3: PC11<M5>,
        storage_config: &StorageConfig) -> Result<Self, StorageError>
    where M0: PinMode,
          M1: PinMode,
          M2: PinMode,
          M3: PinMode,
          M4: PinMode,
          M5: PinMode {

        // SDIO init
        // 4pin bus @ 12Mhz
        // NOTE: the f405 may be capable of up to 48Mhz, might be a hw issue
        let d0 = d0.into_alternate().internal_pull_up(true);
        let d1 = d1.into_alternate().internal_pull_up(true);
        let d2 = d2.into_alternate().internal_pull_up(true);
        let d3 = d3.into_alternate().internal_pull_up(true);
        let clk = clk.into_alternate().internal_pull_up(false);
        let cmd = cmd.into_alternate().internal_pull_up(true);
        let mut sdio = Sdio::<SdCard>::new(sdio, (clk, cmd, d0, d1, d2, d3), clocks);

        let sdio_init_result : Result<(), sdio::Error> = sdio.init(ClockFreq::F12Mhz);

        if let Err(sdio_error) = sdio_init_result {
            return Err(StorageError::SdioError(sdio_error));
        }

        Self::print_size_info(&sdio);

        // Read MBR
        let mbr_table = GenericMbrTable::new(&mut sdio)?;

        // Init partition
        let (fs, part_entry_idx) = Self::partition_init(sdio, storage_config.partition_idx, &mbr_table)?;

        Ok(Self { fs, mbr_table, part_entry_idx, filename: None, out_dir: None, queue: Queue::new() })
    }

    fn partition_init(sdio: Sdio<SdCard>, part_entry_idx: i8, mbr_table: &GenericMbrTable)
        -> Result<(FileSystem<FsCache<SdPartition>, NullTimeProvider, LossyOemCpConverter>, usize), StorageError>  {

        let part_entry_idx =
            if part_entry_idx >= 0 &&
               part_entry_idx <= 3 &&
               mbr_table.entry(part_entry_idx as usize).usable() {
                part_entry_idx as usize
            } else {
                mbr_table.find_favorable_partition()?
            };
        let part_entry = mbr_table.entry(part_entry_idx);

        Self::print_partition_info(&part_entry);

        let partition = SdPartition::new(sdio, part_entry.block_addr, part_entry.no_sectors);
        let cache = FsCache::new(partition);
        let rt = FileSystem::new(cache, fatfs::FsOptions::new());

        match rt {
            Ok(fs) => {
                uprintln!("Volume init successful");
                uprintln!("type: {:?}", fs.fat_type());
                if let Ok(volume_label) = from_utf8(fs.volume_label_as_bytes()) {
                    uprintln!("label: {}", volume_label);
                }

                Ok((fs, part_entry_idx))
            }
            Err(fatfs_error) => {
                uprintln!("Failed to init volume: {:?}", fatfs_error);

                Err(StorageError::FatFsError(fatfs_error))
            }
        }
    }

    fn print_size_info(sdio: &Sdio<SdCard>) {
        let card = sdio.card().unwrap();
        let sd_size_blocks : u32 = card.block_count();
        let sd_size_gig : f64 = card.block_count() as f64 * 512.0 / 1073741824f64;
        uprintln!("SD card size: {:.2} GiB ({} blocks)", sd_size_gig, sd_size_blocks);
    }

    fn print_partition_info(part_entry: &PartitionEntry)  {
        uprintln!("Partition address: {:#X} ({:#X})", part_entry.block_addr * 512, part_entry.block_addr);
        uprintln!("Number of sectors: {} ({}MiB)", part_entry.no_sectors, part_entry.no_sectors as f32 * 512f32 / 1048576f32);
    }

    pub fn stats(&self) -> Result<StorageStats, StorageError> {
        let mut used_space: u64 = 0;
        let mut no_logs: u64 = 0;
        let mut filename: String<11> = String::new();
        let mut dir_stack = Vec::<_, 20>::new();

        // start with the root directory
        dir_stack.push(self.fs.root_dir()).ok();

        while !dir_stack.is_empty() {
            let dir = match dir_stack.pop() {
                Some(dir) => dir,
                None => { continue }
            };

            for r in dir.iter() {
                let entry = r?;

                filename.clear();
                write!(&mut filename, "{}", from_utf8(entry.short_file_name_as_bytes()).unwrap_or_default()).ok();
                filename.make_ascii_lowercase();

                // dir entry, add to dir_stack
                if entry.is_dir() {
                    if filename.starts_with(".") { continue }

                    if dir_stack.push(entry.to_dir()).is_err() {
                        return Err(StorageError::DirStackOverflow)
                    }
                    continue;
                }

                // file entry ends with *.dl, increment number of logs
                if filename.ends_with(".dl") {
                    no_logs += 1;
                }

                // increment used space
                used_space += entry.len();
            }
        }

        let total_space = self.mbr_table.entry(self.part_entry_idx).no_sectors as u64 * 512;

        Ok(StorageStats {
            free_space: total_space - used_space,
            total_space,
            no_logs
        })
    }

    // Returns MBR table and the currently selected partition
    pub fn mbr_table(&self) -> (&GenericMbrTable, usize) {
        (&self.mbr_table, self.part_entry_idx)
    }

    // fills frame & file count in the FileList structure
    pub fn fill_file_list(&self, file_list: &mut FileList) -> Result<(), StorageError> {
        let dir = if file_list.current_dir.len() == 0 {
                self.fs.root_dir()
            } else {
                self.fs.root_dir().open_dir(file_list.current_dir.as_str())?
            };

        let mut filename: String<16> = String::new();
        let mut file_list_entry;

        file_list.clear();

        for (i, r) in dir.iter().enumerate() {
            let entry = r?;

            filename.clear();
            write!(&mut filename, "{}", from_utf8(entry.short_file_name_as_bytes()).unwrap_or_default()).ok();
            filename.make_ascii_lowercase();

            if entry.is_dir() {
                if filename == "." { continue }

                // directories
                file_list_entry = FileListEntry { ftype: FileListEntryType::Folder, name: filename.clone() };
            } else {
                // logs, regular files
                if filename.ends_with(".dl") {
                    file_list_entry = FileListEntry { ftype: FileListEntryType::Log, name: filename.clone() };
                } else {
                    file_list_entry = FileListEntry { ftype: FileListEntryType::File, name: filename.clone() };
                }
            }

            // add entry to frame
            if file_list.offset <= i && !file_list.frame.is_full() {
                file_list.frame.push(file_list_entry).ok();
            }

            // increment number of files in the selected directory
            file_list.len += 1;
        }

        file_list.busy = false;

        Ok(())
    }

    fn base_dir<'w>(&'w self) -> Dir<'w, FsCache<SdPartition>, NullTimeProvider, LossyOemCpConverter> {
        if let Some(out_dir) = &self.out_dir {
            self.fs.root_dir().open_dir(out_dir.as_str())
                .unwrap_or(self.fs.root_dir())
        } else {
            self.fs.root_dir()
        }
    }

    fn gen_logname(&self, root_name : &str) -> String<11> {
        let mut logname : String<11> = String::new();
        let mut i = 0u32;

        loop {
            let mut exists = false;

            logname.clear();
            write!(logname, "{}{}.dl", root_name, i).ok();
            logname.make_ascii_uppercase();

            for r in self.base_dir().iter() {
                if let Ok(dir_entry) = r {
                    let entry_name = from_utf8(dir_entry.short_file_name_as_bytes()).unwrap();

                    if entry_name == logname.as_str() {
                        exists = true;
                        break;
                    }
                }
            }

            if exists == false {
                break;
            } else {
                i += 1;
            }
        }

        logname
    }

    fn datalog_writer<'w>(filename: &'w Option<String<11>>, base_dir: Dir<'w, FsCache<SdPartition>, NullTimeProvider, LossyOemCpConverter>) -> Result<DatalogWriter<'w>, StorageError> {
        if let Some(filename) = filename {
            match base_dir.create_file(filename.as_str()) {
                Ok(mut file) => {
                    file.seek(SeekFrom::End(0))?;
                    Ok(DatalogWriter::new(file))
                },
                Err(err) => Err(StorageError::UnableToOpenLogFile(err))
            }
        } else {
            Err(StorageError::DatalogNotInitialized)
        }
    }

    pub fn init_datalog(&mut self,
                        sensors: &Sensors,
                        sensor_config: &SensorConfig,
                        storage_config: &StorageConfig) -> Result<String<11>, StorageError> {

        // set output directory, filename
        self.out_dir = storage_config.out_dir();
        let filename = self.gen_logname("Log_");
        self.filename = Some(filename.clone());
        let mut writer = Self::datalog_writer(&self.filename, self.base_dir())?;

        // log header (begin_new_log)
        writer.begin_new_log(sensor_config.active_sensors())?;

        // header entries (begin_header, add_var)
        for sensor in sensors {
            if sensor.enabled(sensor_config) {
                sensor.write_header(&mut writer, sensor_config)?;
            }
        }

        Ok(filename)
    }

    fn flush_queue(&mut self) -> Result<(), StorageError> {
        uprintln!("[Storage] flush_queue");

        let queue = &mut self.queue;
        let filename = &mut self.filename;
        let base_dir =
            if let Some(out_dir) = &self.out_dir {
                self.fs.root_dir().open_dir(out_dir.as_str())
                    .unwrap_or(self.fs.root_dir())
            } else {
                self.fs.root_dir()
            };

        let mut writer = Self::datalog_writer(filename, base_dir)?;

        while let Some(entry) = queue.dequeue() {
            match entry {
                StorageQueueEntry::BeginEntry { sensor_id, timestamp, result } => {
                    writer.begin_entry(sensor_id, timestamp, result)?;
                },
                StorageQueueEntry::LogVar { value } => {
                    writer.log_var(value)?;
                }
            }
        }

        writer.flush()?;

        Ok(())
    }

    pub fn append_datalog(&mut self, queue_entry: StorageQueueEntry) -> Result<(), StorageError> {
        if self.queue.is_full() {
            self.flush_queue()?;
        }

        //uprintln!("[Storage] append_datalog: {:?}", queue_entry);
        self.queue.enqueue(queue_entry).ok();

        Ok(())
    }

    pub fn end_datalog(&mut self) -> Result<(), StorageError> {
        self.flush_queue()?;
        self.filename = None;
        Ok(())
    }
}

pub(crate) fn storage(cx: storage::Context, storage_events: StorageEvent) {
    let storage = cx.local.storage;
    let menu_state = cx.shared.menu_state;
    let datalog_state = cx.shared.datalog_state;
    let sensors = cx.shared.sensors;
    let sensor_config = cx.shared.sensor_config;
    let storage_config = cx.shared.storage_config;

    (menu_state, datalog_state, sensors, sensor_config, storage_config).lock(|menu_state, datalog_state, sensors, sensor_config, storage_config| {
        match storage_events {
            StorageEvent::DatalogInit => {
                // init datalog, set timestamp
                datalog_state.filename = storage.init_datalog(sensors, sensor_config, storage_config).unwrap();
                datalog_state.set_timestamp();

                // Redraw menu after the datalog state is updated
                menu::spawn(MenuEvent::Redraw).ok();

                // Also trigger the periodic tasks
                datalog_state.trigger_tasks(sensor_config);
            },
            StorageEvent::DatalogAppend(queue_entry)=> {
                storage.append_datalog(queue_entry).ok();
            },
            StorageEvent::DatalogEnd => {
                storage.end_datalog().ok();
            },
            StorageEvent::UpdateStorageStats => {
                let stats = storage.stats();
                dl_check!(stats, "Failed to computer stats");
                (*menu_state).storage_stats = Some(stats.unwrap());

                // Redraw menu after the storage stats are updated
                menu::spawn(MenuEvent::Redraw).ok();
            },
            StorageEvent::UpdateFileList(dir_selection) => {
                match (*menu_state).file_list {
                    Some(ref mut file_list) => {
                        let rt = storage.fill_file_list(file_list);
                        dl_check!(rt, "Failed to fill file list");
                    },
                    None => {
                        let mut file_list = FileList::new(dir_selection);
                        let rt = storage.fill_file_list(&mut file_list);
                        dl_check!(rt, "Failed to fill file list");
                        (*menu_state).file_list = Some(file_list);
                    }
                }

                // Redraw menu after the filelist frame is updated
                menu::spawn(MenuEvent::Redraw).ok();
            },
            StorageEvent::ReloadConfig => {
                storage.out_dir = storage_config.out_dir();
            },
            _ => {}
        }
    });
}
