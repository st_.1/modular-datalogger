mod period_presets;
mod datalog_state;
mod sensor_trait;
mod sensor_iter;
mod task_table;
mod config;
mod dht11;
mod mcp9808;
mod adxl345;
mod co2dummy;

pub use period_presets::*;
pub use datalog_state::*;
pub use sensor_trait::*;
pub use sensor_iter::*;
pub use task_table::*;
pub use config::*;
pub use self::dht11::*;
pub use self::mcp9808::*;
pub use self::adxl345::*;
pub use self::co2dummy::*;

use heapless::Vec;
use embedded_hal::blocking::delay::{DelayMs, DelayUs};
use stm32f4xx_hal::{
    gpio::{Input, Output, OpenDrain, Alternate, PushPull, gpiob::{PB8, PB13, PB14, PB15}, gpioc::{PC4, PC5, PC6, PC7}},
    spi::{Spi, TransferModeNormal, Pins},
    serial::Serial,
    pac::{SPI2, USART6, DMA1}
};
use crate::app::I2c1Proxy;

const SENSOR_COUNT: usize = 4;

pub struct Sensors {
    pub dht11: Dht11Sensor<PB8<Output<OpenDrain>>>,
    pub mcp9808: Mcp9808Sensor<I2c1Proxy>,
    pub adxl345: Adxl345Sensor<SPI2, (PB13<Alternate<5>>, PB14<Alternate<5>>, PB15<Alternate<5>>), TransferModeNormal, DMA1, PC5<Output<OpenDrain>>, PC4<Input>, 4, 3, 0, 0>,
    pub co2dummy: Co2DummySensor<Serial<USART6, (PC6<Alternate<8>>, PC7<Alternate<8>>)>>
}

impl<'s> Sensors {
    // returns a vector of references to Sensors struct members
    pub fn as_vec(&'s self) -> Vec<&'s dyn Sensor, SENSOR_COUNT> {
        let mut ref_vec = Vec::new();
        ref_vec.push(&self.dht11 as &'s dyn Sensor).ok();
        ref_vec.push(&self.mcp9808 as &'s dyn Sensor).ok();
        ref_vec.push(&self.adxl345 as &'s dyn Sensor).ok();
        ref_vec.push(&self.co2dummy as &'s dyn Sensor).ok();
        ref_vec
    }
}
