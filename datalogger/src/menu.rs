/*
 * Menu: task
 *
 * Manages menu state & calls the drawing routines
 * Called on user input via button interrupts
 */

use rtic::mutex_prelude::*;
use crate::{app::menu, storage::{FrameDirection, GenericMbrTable}};
use ssd1306::{mode::BufferedGraphicsMode, prelude::*, I2CDisplayInterface, Ssd1306};
use embedded_graphics::{
    mono_font::{ascii::FONT_5X7, MonoTextStyleBuilder},
    pixelcolor::BinaryColor,
    prelude::*,
    text::{Baseline, Text},
};
use embedded_hal::blocking::i2c;
use display_interface::DisplayError;
use systick_monotonic::*;

use crate::buttons::ButtonEvent;
use crate::storage::{FileList, StorageStats};

mod process;
mod draw;
mod bitmaps;
mod list_drawable;
mod structure;
mod state;
mod text_style;
mod num_fmt;
mod sensor_submenu;
pub mod part_table_submenu;

use crate::menu::{draw::draw_menu, process::process_menu, structure::{MAIN_MENU, Menu, MenuLink}};
pub use crate::menu::{state::MenuState, sensor_submenu::{SensorMenuConfig, SensorMenuVar}, part_table_submenu::*, draw::draw_error};

pub type Ssd1306Cfg<I> = Ssd1306<I2CInterface<I>, DisplaySize128x32, BufferedGraphicsMode<DisplaySize128x32>>;

pub struct MenuResources<I>(pub Ssd1306Cfg<I>, pub MenuState<'static>);

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MenuEvent {
    ButtonPress(ButtonEvent),
    SensorRedraw,
    Redraw
}

#[derive(Debug)]
pub enum MenuResourcesError {
    DisplayInitError(DisplayError)
}

impl From<DisplayError> for MenuResourcesError {
    fn from(display_error: DisplayError) -> Self { Self::DisplayInitError(display_error) }
}

impl<I> MenuResources<I> {
    pub fn new(i2c: I, mbr_table: &GenericMbrTable, part_entry_idx: usize) -> Result<MenuResources<I>, MenuResourcesError>
        where I: i2c::Write {

        let mut display = Ssd1306::new(
            I2CDisplayInterface::new(i2c),
            DisplaySize128x32,
            DisplayRotation::Rotate0,
        ).into_buffered_graphics_mode();

        display.init()?;

        menu::spawn(MenuEvent::Redraw).ok();

        let part_table = PartTableSubmenu::new(mbr_table, part_entry_idx);

        Ok(MenuResources(display, MenuState::new(part_table)))
    }
}

pub(crate) fn menu(cx: menu::Context, menu_event: MenuEvent) {
    let display = cx.local.display;
    let menu_state = cx.shared.menu_state;
    let datalog_state = cx.shared.datalog_state;
    let sensor_config = cx.shared.sensor_config;
    let storage_config = cx.shared.storage_config;

    (menu_state, datalog_state, sensor_config, storage_config).lock(|menu_state, datalog_state, sensor_config, storage_config| {
        match menu_event {
            MenuEvent::ButtonPress(button_event) => {
                // manage mutable state of the menu system
                process_menu(menu_state, datalog_state, sensor_config, storage_config, button_event);
            },
            MenuEvent::SensorRedraw => {
                // as long as the measurement is in progress schedule another redraw
                if datalog_state.inprog {
                    menu::spawn_after(1.secs(), MenuEvent::SensorRedraw).ok();
                }
            },
            _ => {}
        }

        // display menu
        display.clear();
        draw_menu(display, menu_state, datalog_state, sensor_config);
        display.flush().unwrap();
    });
}
