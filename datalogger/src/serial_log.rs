// As the feather-f405's SWO pin is used up by the flash chip
// and semihosting is excruciatingly slow we define
// a custom uprintln macro for the purpose of logging/debug over serial

#![allow(unused_macros)]
#![allow(dead_code)]

use cortex_m::interrupt::{free, Mutex};
use core::{
    cell::RefCell,
    ops::DerefMut,
    fmt::{self, Write}
};
use stm32f4xx_hal::{
    prelude::*,
    serial::{Serial, Tx, config::{WordLength, Parity, StopBits, DmaConfig, Config, InvalidConfig}},
    time::Bps,
    pac::USART3,
    gpio::{Alternate, gpiob::PB10},
    rcc::Clocks
};

static SERIAL_OUT: Mutex<RefCell<Option<SerialPort>>> = Mutex::new(RefCell::new(None));

struct SerialPort {
    pub usart3: Tx<USART3, u8>
}

impl Write for SerialPort {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
            while self.usart3.write(byte).is_err() {}
        }
        Ok(())
    }
}

fn serial_log_set(serial: Tx<USART3, u8>) {
    free(|cs| {
        if SERIAL_OUT.borrow(cs).borrow().is_none() {
            SERIAL_OUT.borrow(cs).replace(Some( SerialPort { usart3: serial } ));
        }
    });
}

pub fn serial_log_init(usart_dev: USART3,
                       tx: PB10<Alternate<7>>,
                       clocks: &Clocks) -> Result<(), InvalidConfig> {

    let serial_config = Config::default()
        .baudrate(Bps(921600))
        .wordlength_8()
        .parity_none()
        .stopbits(StopBits::STOP1);

    let usart3: Tx<USART3, u8> = Serial::tx(
        usart_dev,
        tx,
        serial_config,
        clocks
    )?;

    serial_log_set(usart3);

    Ok(())
}

pub fn serialout_fmt(args: fmt::Arguments) {
    free(|cs| {
        if let Some(serial) = crate::serial_log::SERIAL_OUT.borrow(cs).borrow_mut().deref_mut() {
            serial.write_fmt(args).unwrap();
        }
    });
}

macro_rules! uprint {
    ($($arg:tt)*) => {
        crate::serial_log::serialout_fmt(format_args!($($arg)*))
    };
}

macro_rules! uprintln {
    ($fmt:expr) => {
        uprint!(concat!($fmt, "\r\n"))
    };
    ($fmt:expr, $($arg:tt)*) => {
        uprint!(concat!($fmt, "\r\n"), $($arg)*)
    };
}
