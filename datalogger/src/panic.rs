use core::{panic::PanicInfo, mem::transmute, fmt, fmt::Write, cell::RefCell, ops::DerefMut};
use ssd1306::{mode::BufferedGraphicsMode, prelude::*, I2CDisplayInterface, Ssd1306};
use crate::menu::draw_error;
use heapless::String;
use cortex_m::interrupt::{free, Mutex};

use stm32f4xx_hal::{
    prelude::*,
    gpio::{Alternate, OpenDrain, gpiob::{PB6, PB7}},
    i2c, i2c::I2c,
    rcc::Clocks,
    pac, pac::I2C1,
};

// core::PanicInfo notes:
// .message() is not stable
// .payload() doesn't seem to work without libstd
// -> alternative: global panic message set within the dl_panic macro/function
static PANIC_MSG: Mutex<RefCell<String<100>>> = Mutex::new(RefCell::new(String::new()));

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    // Check for dl_panic message
    let mut panic_msg: String<100> = String::new();
    free(|cs| {
        let mut _msg = crate::panic::PANIC_MSG.borrow(cs).borrow_mut();
        if _msg.len() > 0 {
            write!(&mut panic_msg, "{}", _msg.as_str()).ok();
        }
    });

    // Print panic message to UART
    uprintln!("********** Panic! *********");
    if let Some(location) = info.location() {
        uprintln!("file: {} @ {} / {}", location.file(), location.line(), location.column());
    }
    if panic_msg.len() > 0 {
        uprintln!("message: {}", panic_msg.as_str());
    }
    uprintln!("\r\n");

    // Steal display peripheral & display the error
    // on screen indefinitely
    let mut out_msg: String<100> = String::new();

    let msg =
        if panic_msg.len() > 0 {
            panic_msg.as_str()
        } else if let Some(location) = info.location() {
            write!(&mut out_msg, "{}\nline: {} col: {}", location.file(), location.line(), location.column()).ok();
            out_msg.as_str()
        } else {
            &"N/A"
        };

    let mut stolen_display = steal_display();
    draw_error(&mut stolen_display, &"Panic", msg);
    stolen_display.flush().unwrap();

    loop {}
}

// Reinitializes clocks, I2C1 and the display
// with a stolen Peripherals struct
fn steal_display()
-> Ssd1306<I2CInterface<I2c<I2C1, (PB6<Alternate<4, OpenDrain>>, PB7<Alternate<4, OpenDrain>>)>>, DisplaySize128x32, BufferedGraphicsMode<DisplaySize128x32>> {
    let dev = unsafe { pac::Peripherals::steal() };
    let gpiob = dev.GPIOB.split();
    let rcc = dev.RCC.constrain();
    let clocks = rcc
        .cfgr
        .use_hse(12.MHz())
        .require_pll48clk()
        .sysclk(168.MHz())
        .hclk(168.MHz())
        .pclk1(42.MHz())
        .pclk2(84.MHz())
        .freeze();

    let scl = gpiob.pb6.into_alternate_open_drain();
    let sda = gpiob.pb7.into_alternate_open_drain();
    let i2c = I2c::new(
        dev.I2C1,
        (scl, sda),
        i2c::Mode::Standard {frequency: 400.kHz()},
        &clocks
    );

    let mut display = Ssd1306::new(
        I2CDisplayInterface::new(i2c),
        DisplaySize128x32,
        DisplayRotation::Rotate0,
    ).into_buffered_graphics_mode();

    display.init().ok();
    display
}

/* dl_panic! macro + shorthands */

pub fn dl_panic_fmt(args: fmt::Arguments) -> ! {
    // set panic message
    free(|cs| {
        let mut panic_msg = crate::panic::PANIC_MSG.borrow(cs).borrow_mut();
        panic_msg.write_fmt(args).ok();
    });

    // actually panic
    panic!();
}

macro_rules! dl_panic {
    ($($arg:tt)*) => {
        crate::panic::dl_panic_fmt(format_args!($($arg)*))
    };
}

macro_rules! dl_check {
    ($rt:ident, $s:expr) => {
        if let Err(err) = $rt { crate::panic::dl_panic!("{}: {:?}", $s, err) };
    };
}

macro_rules! dl_unwrap {
    ($rt:ident, $s:expr) => {
        let $rt =
            match $rt {
                Ok(val) => val,
                Err(err) => crate::panic::dl_panic!("{}: {:?}", $s, err)
            };
    };
}

pub(crate) use dl_panic;
pub(crate) use dl_check;
pub(crate) use dl_unwrap;
