#![no_std]
#![no_main]
#![allow(unused_imports)]

use rtic::app;

#[macro_use]
mod serial_log;
#[macro_use]
extern crate serde;
extern crate serde_big_array;

mod menu;
mod buttons;
mod storage;
mod sensor;
mod config;
mod config_writer;

#[macro_use]
mod panic;
use panic as _;

#[app(device = stm32f4xx_hal::pac, dispatchers = [DMA2_STREAM5, DMA2_STREAM6, DMA2_STREAM7, TIM7, TIM8_CC])]
mod app {
    use stm32f4xx_hal::{
        prelude::*,
        gpio::{Output, Alternate, OpenDrain, Edge, Input, PushPull, gpioa::PA15, gpiob::{PB3, PB4, PB5, PB6, PB7, PB10, PB11}, gpioc::{PC2, PC3, PC6, PC7}},
        i2c, i2c::I2c,
        spi, spi::{Spi, Polarity, Phase, TransferModeNormal},
        rcc::Clocks,
        serial, serial::{Serial, config::{Config, Parity, StopBits, WordLength}},
        time::{Bps, KiloHertz},
        pac, pac::{I2C1, EXTI, SYSCFG, USART3, TIM6, DMA2, SPI2, SPI3},
        timer::Delay,
        dma, dma::{Stream3, PeripheralToMemory, StreamsTuple, Transfer},
    };

    use shared_bus::{I2cProxy, NullMutex, AtomicCheckMutex};
    use core::{fmt, fmt::Write, str};
    use systick_monotonic::{Systick, fugit::Kilohertz};
    use ::adxl345::F32x3;
    use heapless::Vec;

    use crate::menu::{menu, MenuEvent, MenuResources, MenuResourcesError, MenuState, Ssd1306Cfg};
    use crate::buttons::{Buttons, button_up, button_down, button_select, button_timeout, ButtonEvent};
    use crate::storage::{Storage, storage, FileList, StorageEvent, StorageConfig};
    use crate::sensor::{SensorConfig, DatalogState, Sensors, Dht11Sensor, Mcp9808Sensor, Adxl345Sensor, Co2DummySensor,
                        sensor_dht11, sensor_mcp9808, sensor_co2dummy, sensor_adxl345_polling, sensor_adxl345_dma_rx};
    use crate::serial_log::serial_log_init;
    use crate::config::DatalogConfig;
    use crate::config_writer::{ConfigWriter, persist_config};

    pub type I2c1 = I2c<I2C1, (PB6<Alternate<4, OpenDrain>>, PB7<Alternate<4, OpenDrain>>)>;
    pub type I2c1Proxy = I2cProxy<'static, AtomicCheckMutex<I2c1>>;
    pub type SensorDelay = Delay<TIM6, 1_000_000>;
    pub const GRAN: u32 = 1000; // 1ms

    #[monotonic(binds = SysTick, default = true)]
    type Mono = Systick<GRAN>;

    #[shared]
    struct Shared {
        delay: SensorDelay,
        buttons: Buttons,
        menu_state: MenuState<'static>,
        sensor_config: SensorConfig,
        storage_config: StorageConfig,
        datalog_state: DatalogState,
        sensors: Sensors
    }

    #[local]
    struct Local {
        display: Ssd1306Cfg<I2c1Proxy>,
        storage: Storage,
        config_writer: ConfigWriter<Spi<SPI3, (PB3<Alternate<6>>, PB4<Alternate<6>>, PB5<Alternate<6>>), TransferModeNormal>, PA15<Output<OpenDrain>>>
    }

    #[init]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        let core: cortex_m::Peripherals = cx.core;
        let dev: pac::Peripherals = cx.device;

        let gpioa = dev.GPIOA.split();
        let gpiob = dev.GPIOB.split();
        let gpioc = dev.GPIOC.split();
        let gpiod = dev.GPIOD.split();

        // clock config
        let rcc = dev.RCC.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(12.MHz()) // external high speed oscillator (OSC_IN, OSC_OUT, 12Mhz)
            .require_pll48clk() // PLL (allows freq mult up to 168Mhz)
            .sysclk(168.MHz()) // system core frequency
            .hclk(168.MHz()) // AHB1..3 buses
            .pclk1(42.MHz()) // low-speed APB domains (APB1)
            .pclk2(84.MHz()) // high-speed APB domains (APB2)
            .freeze();

        // Systick counts at the bus clock frequency, in our case 168MHz
        let mono = Systick::new(core.SYST, clocks.hclk().raw());

        // init uprintln! macro
        serial_log_init(dev.USART3, gpiob.pb10.into_alternate(), &clocks).ok();

        /* init external flash (GD25Q16C) */
        let spi_mode = spi::Mode {
            polarity: Polarity::IdleLow, // CPOL = 0
            phase: Phase::CaptureOnFirstTransition // CPHA = 0
        };
        let ext_flash_sck = gpiob.pb3.into_alternate();
        let ext_flash_miso = gpiob.pb4.into_alternate();
        let ext_flash_mosi = gpiob.pb5.into_alternate();
        let ext_flash_cs = gpioa.pa15.into_open_drain_output();
        let ext_flash_spi = Spi::new(dev.SPI3, (ext_flash_sck, ext_flash_miso, ext_flash_mosi), spi_mode, 500.kHz(), &clocks);

        /* init sensor buses */

        // Display, MCP9808: I2C
        let shared_i2c_bus: &'static _ = {
            let scl = gpiob.pb6.into_alternate_open_drain();
            let sda = gpiob.pb7.into_alternate_open_drain();

            let shared_i2c = I2c::new(
                dev.I2C1,
                (scl, sda),
                i2c::Mode::Standard {frequency: 400.kHz()},
                &clocks
            );

            match shared_bus::new_atomic_check!(I2c1 = shared_i2c) {
                Some(sh_bus) => sh_bus,
                None => dl_panic!("Failed to init shared I2C bus")
            }
        };

        // DHT11: custom ow prot
        let dht11_gpio = gpiob.pb8.into_open_drain_output();

        // Adxl345: SPI init
        let adxl345_int0 = gpioc.pc4.into_floating_input();
        let adxl345_cs = gpioc.pc5.into_open_drain_output();
        let sck = gpiob.pb13.into_alternate();
        let mosi = gpiob.pb14.into_alternate();
        let miso = gpiob.pb15.into_alternate();
        let spi_mode = spi::Mode {
            polarity: Polarity::IdleHigh, // CPOL = 1
            phase: Phase::CaptureOnSecondTransition // CPHA = 1 (trailing edge)
        };
        let stream_tuple = StreamsTuple::new(dev.DMA1);
        let adxl345_stream_tx = stream_tuple.4;
        let adxl345_stream_rx = stream_tuple.3;
        let adxl345_spi = Spi::new(dev.SPI2, (sck, mosi, miso), spi_mode, 500.kHz(), &clocks);

        // CO2 mock sensor: USART6 init
        // (PC6, PC7) 8-N-1
        let serial_config = Config::default()
            .baudrate(Bps(19200))
            .wordlength_8()
            .parity_none()
            .stopbits(StopBits::STOP1);

        let co2dummy_serial =
            match Serial::new(
                dev.USART6,
                (gpioc.pc6.into_alternate(), gpioc.pc7.into_alternate()),
                serial_config,
                &clocks
            ) {
                Ok(co2dummy_serial) => co2dummy_serial,
                Err(err) => dl_panic!("USART6 init failed: {:?}", err)
            };

        // config setup
        let mut config_writer =
            match ConfigWriter::new(ext_flash_spi, ext_flash_cs) {
                Ok(config_writer) => config_writer,
                Err(err) => dl_panic!("Failed to init external flash: {:?}", err)
            };
        let datalog_config = config_writer.config().unwrap_or(DatalogConfig::new());
        let DatalogConfig { sensor_config, storage_config } = datalog_config;

        // storage setup
        let storage =
            match Storage::new(
                dev.SDIO,
                &clocks,
                gpioc.pc12,
                gpiod.pd2,
                gpioc.pc8,
                gpioc.pc9,
                gpioc.pc10,
                gpioc.pc11,
                &storage_config
            ) {
                Ok(storage) => storage,
                Err(error) => dl_panic!("Failed to init storage: {:?}", error)
            };

        let datalog_state = DatalogState::new(&sensor_config);

        // menu/display setup
        let (mbr_table, part_entry_idx) = storage.mbr_table();
        let MenuResources(display, menu_state) =
            match MenuResources::new(shared_i2c_bus.acquire_i2c(), mbr_table, part_entry_idx) {
                Ok(menu_resources) => menu_resources,
                Err(error) => dl_panic!("Failed to init menu: {:?}", error)
            };

        // delay abstraction (TIM6)
        let mut delay = dev.TIM6.delay_us(&clocks);

        // sensor setup
        let dht11 = Dht11Sensor::new(dht11_gpio, 0);
        let mcp9808 = Mcp9808Sensor::new(shared_i2c_bus.acquire_i2c(), 1);
        let adxl345 = Adxl345Sensor::new(adxl345_spi, adxl345_cs, adxl345_int0, adxl345_stream_tx, adxl345_stream_rx, &sensor_config, 2);
        let co2dummy = Co2DummySensor::new(co2dummy_serial, &mut delay, 3);

        dl_unwrap!(mcp9808, "Failed to init MCP9808");
        dl_unwrap!(adxl345, "Failed to init ADXL345");
        dl_unwrap!(co2dummy, "Failed to init CO2Dummy");

        let sensors = Sensors { dht11, mcp9808, adxl345, co2dummy };

        // enable button interrupts
        let buttons = Buttons::new(gpioc.pc3, gpioc.pc2, gpioc.pc1, dev.EXTI, dev.SYSCFG);

        (
            Shared { delay, buttons, menu_state, sensor_config, storage_config, datalog_state, sensors },
            Local { display, storage, config_writer },
            init::Monotonics(mono)
        )
    }

    #[idle]
    fn idle(_cx: idle::Context) -> ! {
        uprintln!("idle()");
        loop {}
    }

    /* delegate tasks into separate modules */
    extern "Rust" {
        /* menu/display task */
        #[task(local = [display], shared = [menu_state, datalog_state, sensor_config, storage_config], capacity = 3, priority = 3)]
        fn menu(cx: menu::Context, menu_event: MenuEvent);

        /* storage/sdcard task */
        #[task(local = [storage], shared = [menu_state, datalog_state, sensors, sensor_config, storage_config], capacity = 200)]
        fn storage(cx: storage::Context, storage_event: StorageEvent);

        /* config persist/ext.flash task */
        #[task(local = [config_writer], shared = [sensor_config, storage_config])]
        fn persist_config(cx: persist_config::Context);

        /* button interrupts */
        #[task(binds = EXTI3, shared = [buttons], priority = 2)]
        fn button_up(cx: button_up::Context);
        #[task(binds = EXTI2, shared = [buttons], priority = 2)]
        fn button_down(cx: button_down::Context);
        #[task(binds = EXTI1, shared = [buttons], priority = 2)]
        fn button_select(cx: button_select::Context);
        #[task(shared = [buttons], capacity = 3, priority = 4)]
        fn button_timeout(cx: button_timeout::Context, button_event: ButtonEvent);

        /* sensor tasks */

        // periodic requests
        #[task(shared = [delay, sensor_config, datalog_state, sensors])]
        fn sensor_dht11(cx: sensor_dht11::Context);
        #[task(shared = [delay, sensor_config, datalog_state, sensors])]
        fn sensor_mcp9808(cx: sensor_mcp9808::Context);
        #[task(shared = [delay, sensor_config, datalog_state, sensors])]
        fn sensor_co2dummy(cx: sensor_co2dummy::Context);

        // dma fifo interrupt polling
        #[task(shared = [delay, sensor_config, datalog_state, sensors])]
        fn sensor_adxl345_polling(cx: sensor_adxl345_polling::Context);
        #[task(binds = DMA1_STREAM3, shared = [delay, sensor_config, datalog_state, sensors])]
        fn sensor_adxl345_dma_rx(cx: sensor_adxl345_dma_rx::Context);
    }
}
