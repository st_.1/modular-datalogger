# Cargo workspaces with multiple targets are not in stable, using a Makefile instead.
# https://github.com/rust-lang/cargo/issues/9406
# https://github.com/rust-lang/cargo/pull/7022

.PHONY: test build run
all: test build

test:
	@echo "TEST: fsutils"
	@cd fsutils; cargo test

build:
	@echo "BUILD: datalogger"
	@cd datalogger; cargo build

run:
	@echo "RUN: datalogger"
	@cd datalogger; cargo run
