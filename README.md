# `ModularDatalogger`

> A modular datalogger written in Rust, as part of a bacelor thesis at VSB-TUO Ostrava "Embedded datalogger in Rust".

## UI

![](datalogger/assets/mockup.png)

## License

- Apache License, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
