use core::cmp::min;
use fatfs::{Read, Write, Seek, IoBase, SeekFrom};

const BUF_SIZE: usize = 512;

#[derive(PartialEq, Eq)]
enum Mode {
    Read,
    Write
}

/// `FsCache` is a generic IO cache used to improve performance of SDIO communication
/// Internally `FsCache` is a 512 byte wide buffer which is used to store consecutive reads/writes
/// and flushed whenever different part of the underlying IO is accessed, the length of the buffer
/// is exceeded, or when the object is dropped.
/// Adapted from `fscommon::BufStream` by Rafał Harabień.
pub struct FsCache<IO: Read + Write + Seek> {
    inner: IO,
    buf: [u8; BUF_SIZE],
    len: usize,
    pos: usize,
    mode: Mode,
}

impl<IO: Read + Write + Seek> FsCache<IO> {
    pub fn new(inner: IO) -> Self {
        FsCache {
            inner,
            buf: [0; BUF_SIZE], pos: 0, len: 0,
            mode: Mode::Read
        }
    }

    fn flush_buf(&mut self) -> Result<(), IO::Error> {
        if self.mode == Mode::Write {
            self.inner.write_all(&self.buf[..self.pos])?;
        }

        if self.mode == Mode::Read {
            self.inner.seek(SeekFrom::Current(- (self.len as i64 - self.pos as i64) ))?; // rewind
        }

        self.pos = 0;
        self.len = 0;

        Ok(())
    }

    fn read_mode(&mut self) -> Result<(), IO::Error> {
        if self.mode != Mode::Read {
            self.flush_buf()?;
            self.mode = Mode::Read;
        }
        Ok(())
    }

    fn write_mode(&mut self) -> Result<(), IO::Error> {
        if self.mode != Mode::Write {
            self.flush_buf()?;
            self.mode = Mode::Write;
        }
        Ok(())
    }
}

impl<IO: Read + Write + Seek> Read for FsCache<IO> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, IO::Error> {
        // switch to Read mode if needed
        self.read_mode()?;

        // if the buffer is empty and buf.len() > BUF_SIZE
        // read directly
        if self.pos == self.len && buf.len() >= BUF_SIZE {
            return self.inner.read(buf);
        }

        // fill self.buf as needed (up to BUF_SIZE)
        // (this only happens when we run out of buffered data)
        if self.pos == self.len {
            self.len = self.inner.read(&mut self.buf)?;
            self.pos = 0;
        }

        assert!(self.pos < self.len);

        let remaining = self.len - self.pos;

        // read data from self.buf
        let bytes_read = min(remaining, buf.len());
        assert!(self.pos + bytes_read <= self.len);
        buf[..bytes_read].clone_from_slice(&self.buf[self.pos..self.pos + bytes_read]);

        // seek self.pos
        self.pos += bytes_read;

        Ok(bytes_read)
    }
}

impl<IO: Read + Write + Seek> Write for FsCache<IO> {
    fn write(&mut self, buf: &[u8]) -> Result<usize, IO::Error> {
        // switch to Write mode if needed
        self.write_mode()?;

        // data > remaining space
        if buf.len() > (BUF_SIZE - self.pos) {
            self.flush_buf()?;
            // data equal or larger to buffer size, write directly
            if buf.len() >= BUF_SIZE {
                return self.inner.write(buf);
            }
        }

        // data < remaining space, append
        self.buf[self.pos..self.pos + buf.len()].clone_from_slice(buf);
        self.pos += buf.len();
        Ok(buf.len())
    }

    fn flush(&mut self) -> Result<(), IO::Error> {
        self.flush_buf()?;
        self.inner.flush()
    }
}

impl<IO: Read + Write + Seek> Seek for FsCache<IO> {
    fn seek(&mut self, seek_from: SeekFrom) -> Result<u64, IO::Error> {
        if self.pos > 0 || self.len > 0 {
            self.flush_buf()?;
        }

        self.inner.seek(seek_from)
    }
}

impl<IO: Read + Write + Seek> IoBase for FsCache<IO> {
    type Error = IO::Error;
}

impl<IO: Read + Write + Seek> Drop for FsCache<IO> {
    fn drop(&mut self) {
       self.flush_buf().unwrap();
    }
}

#[cfg(test)]
mod tests {
    use std::vec;

    use crate::storagemock::*;
    use super::*;
    use rand::RngCore;

    const MOCK_SIZE: usize = 40000;
    const MAX_LEN: usize = MOCK_SIZE / 2;

    #[test]
    fn read() {
        let mut prng = rand::thread_rng();

        // read raw data & via fscache, compare results
        for read_len in (1..MAX_LEN).step_by((prng.next_u64() % 100) as usize) {
            let mut read_raw = vec![0u8; read_len];
            let mut read_cached = vec![0u8; read_len];
            let offset = prng.next_u64() % (MOCK_SIZE - read_len) as u64;

            let mut storage = StorageMock::new(MOCK_SIZE);

            storage.seek(SeekFrom::Start(offset)).unwrap();
            storage.read_exact(&mut read_raw).unwrap();

            let mut fscache = FsCache::<StorageMock>::new(storage);

            fscache.seek(SeekFrom::Current(- (read_len as i64))).unwrap();
            fscache.read_exact(&mut read_cached).unwrap();

            println!("len: {} offset: {}", read_len, offset);

            assert_eq!(read_raw, read_cached);
        }
    }

    #[test]
    fn write() {
        let mut prng = rand::thread_rng();
        let storage = StorageMock::new(MOCK_SIZE);
        let mut fscache = FsCache::<StorageMock>::new(storage);

        // write data at a random position, read them back and compare
        for write_len in (1..MAX_LEN).step_by((prng.next_u64() % 100 + 1) as usize) {
            let mut write_orig = vec![0u8; write_len];
            let mut write_copy = vec![0u8; write_len];
            let offset = prng.next_u64() % (MOCK_SIZE - write_len * 2) as u64;

            println!("len: {} offset: {}", write_len, offset);

            prng.fill_bytes(&mut write_orig);
            // pos = offset
            fscache.seek(SeekFrom::Start(offset)).unwrap();
            // pos += write_len
            fscache.write_all(&mut write_orig).unwrap();
            // pos += write_len
            fscache.read_exact(&mut write_copy).unwrap();

            assert_ne!(write_orig, write_copy);

            // pos -= write_len * 2 (rewind to offset)
            fscache.seek(SeekFrom::Current(- ((write_len * 2) as i64))).unwrap();
            fscache.seek(SeekFrom::Start(offset)).unwrap();
            fscache.seek(SeekFrom::Start(offset)).unwrap();
            // pos += write_len
            fscache.read_exact(&mut write_copy).unwrap();

            assert_eq!(write_orig, write_copy);
        }
    }
}
