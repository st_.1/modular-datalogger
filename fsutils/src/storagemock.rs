#![allow(dead_code)]

use fatfs::{Read, Write, Seek, IoBase, SeekFrom};
use std::vec::*;
use std::cmp::min;
use rand::{RngCore, SeedableRng};
use rand_pcg::Pcg64;

pub struct StorageMock {
    buf: Vec<u8>,
    len: usize,
    pos: usize
}

impl StorageMock {
    pub fn new(len: usize) -> Self {
        // generate seed value
        let seed = rand::thread_rng().next_u64();

        Self::from_seed(len, seed)
    }

    pub fn from_seed(len: usize, seed: u64) -> Self {
        let mut buf = vec![0; len];

        // seed a PRNG
        let mut prng = Pcg64::seed_from_u64(seed);

        // fill buf with random data
        prng.fill_bytes(&mut buf);

        Self { buf, len, pos: 0 }
    }
}

impl Read for StorageMock {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, ()> {

        assert!(self.pos < self.len);
        let remaining = self.len - self.pos;
        let bytes_read = min(remaining, buf.len());
        assert!(self.pos + bytes_read <= self.len);

        // read data from self.buf
        buf[..bytes_read].copy_from_slice(&self.buf[self.pos..self.pos + bytes_read]);

        // seek self.pos
        self.pos += bytes_read;

        Ok(bytes_read)
    }
}

impl Write for StorageMock {
    fn write(&mut self, buf: &[u8]) -> Result<usize, ()> {
        assert!(self.pos < self.len);
        let remaining = self.len - self.pos;
        let bytes_written = min(remaining, buf.len());
        assert!(self.pos + bytes_written <= self.len);

        // write data to self.buf
        self.buf[self.pos..self.pos + bytes_written].copy_from_slice(&buf[..bytes_written]);

        // seek self.pos
        self.pos += bytes_written;

        Ok(bytes_written)
    }

    fn flush(&mut self) -> Result<(), ()> {
        Ok(())
    }
}

impl Seek for StorageMock {
    fn seek(&mut self, seek_from: SeekFrom) -> Result<u64, ()> {
        let new_pos = match seek_from {
            SeekFrom::Start(offset) => offset as i64,
            SeekFrom::End(offset) => self.len as i64 + offset,
            SeekFrom::Current(offset) => self.pos as i64 + offset
        };

        assert!(new_pos >= 0 && new_pos <= self.len as i64);

        println!("self.pos: {} new_pos: {}", self.pos, new_pos);

        self.pos = new_pos as usize;

        Ok(new_pos as u64)
    }
}

impl IoBase for StorageMock {
    type Error = ();
}

impl Drop for StorageMock {
    fn drop(&mut self) {}
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read() {
        let mut storage = StorageMock::from_seed(100, 0);

        let mut buf = [0u8; 20];
        storage.read_exact(&mut buf).unwrap();

        assert_eq!(buf, [83, 188, 226, 212, 218, 37, 174, 32, 251, 105, 191, 43, 225, 56, 249, 88, 49, 226, 248, 12]);
    }

    #[test]
    fn write() {
        let mut storage = StorageMock::new(300);
        let mut prng = rand::thread_rng();

        let mut buf_orig = [0u8; 40];
        let mut buf_copy = [0u8; 40];
        prng.fill_bytes(&mut buf_orig);

        storage.seek(SeekFrom::Start(20)).unwrap();
        storage.write_all(&buf_orig).unwrap();

        storage.read_exact(&mut buf_copy).unwrap();

        assert_ne!(buf_orig, buf_copy);

        storage.seek(SeekFrom::Start(20)).unwrap();
        storage.read_exact(&mut buf_copy).unwrap();

        assert_eq!(buf_orig, buf_copy);
    }
}
