#![cfg_attr(not(feature = "std"), no_std)]

mod fscache;
pub use fscache::*;

#[cfg(feature = "std")]
mod storagemock;
